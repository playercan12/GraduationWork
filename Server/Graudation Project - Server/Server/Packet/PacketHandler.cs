﻿using Google.Protobuf;
using Google.Protobuf.Protocol;
using Server;
using Server.Game;
using ServerCore;
using System;
using System.Collections.Generic;
using System.Text;

class PacketHandler
{
	public static void C_MoveHandler(PacketSession session, IMessage packet)
	{
		C_Move movePacket = packet as C_Move;
		ClientSession clientSession = session as ClientSession;

		Player player = clientSession.MyPlayer;
		if (player == null)
			return;

		GameRoom room = player.Room;
		if (room == null)
			return;

		//room.HandleMove(player, movePacket);
		room.Push(room.HandleMove, player, movePacket);
	}

	public static void C_AttackStateHandler(PacketSession session, IMessage packet)
	{
		C_AttackState attackPacket = packet as C_AttackState;
		ClientSession clientSession = session as ClientSession;

		Player player = clientSession.MyPlayer;
		if (player == null)
			return;

		GameRoom room = player.Room;
		if (room == null)
			return;

		room.Push(room.HandleAttack, player, attackPacket);
	}

	public static void C_ChangeStatHandler(PacketSession session, IMessage packet)
	{
		C_ChangeStat changePacket = packet as C_ChangeStat;

		ClientSession clientSession = session as ClientSession;

		Player player = clientSession.MyPlayer;
		if (player == null)
			return;

		GameRoom room = player.Room;
		if (room == null)
			return;

		room.Push(room.HandleStat, player, changePacket);
	}

	public static void C_CarHandler(PacketSession session, IMessage packet)
	{
		C_Car carPacket = packet as C_Car;
		ClientSession clientSession = session as ClientSession;

		Player player = clientSession.MyPlayer;
		if (player == null)
			return;

		GameRoom room = player.Room;
		if (room == null)
			return;

		room.Push(room.HandleCar, player, carPacket);
	}

	public static void C_ButtonHandler(PacketSession session, IMessage packet)
	{
		C_Button buttonPacket = packet as C_Button;
		ClientSession clientSession = session as ClientSession;

		Player player = clientSession.MyPlayer;
		if (player == null)
			return;

		GameRoom room = player.Room;
		if (room == null)
			return;

		room.Push(room.HandleButton, player, buttonPacket);
	}

	public static void C_LoginHandler(PacketSession session, IMessage packet)
	{
		C_Login loginPacket = packet as C_Login;
		ClientSession clientSession = session as ClientSession;
		clientSession.HandleLogin(loginPacket);

	}

	public static void C_EnterGameHandler(PacketSession session, IMessage packet)
	{
		C_EnterGame enterGamePacket = (C_EnterGame)packet;
		ClientSession clientSession = (ClientSession)session;
		clientSession.HandleEnterGame(enterGamePacket);
	}

	public static void C_EnterRobbyHandler(PacketSession session, IMessage packet)
	{
		C_EnterRobby RobbyPacket = (C_EnterRobby)packet;
		ClientSession clientSession = (ClientSession)session;
		clientSession.HandleEnterRobby(RobbyPacket);
	}
}
