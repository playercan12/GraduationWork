﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using ServerCore;
using System.Net;
using Google.Protobuf.Protocol;
using Google.Protobuf;
using Server.Game;
using System.Numerics;

namespace Server
{
    public partial class ClientSession : PacketSession
    {
        private static int player_count = 0;
        private static string p1_id = " ";
        private static string p2_id = " ";
        private static string p3_id = " ";
        private static string p4_id = " ";
        private static int isStart = 0;

        public void HandleLogin(C_Login loginPacket)
        {
            if (loginPacket.UniqueId != null)
            {
                switch (player_count)
                {
                    case 0:
                        p1_id = loginPacket.UniqueId;
                        break;

                    case 1:
                        p2_id = loginPacket.UniqueId;
                        break;

                    case 2:
                        p3_id = loginPacket.UniqueId;
                        break;

                    case 3:
                        p4_id = loginPacket.UniqueId;
                        break;

                    default:
                        return;
                }
                player_count++;
            }
            else
            {
                return;
            }
        }

        public void HandleEnterRobby(C_EnterRobby RobbyPacket)
        {
            if (RobbyPacket.EnterOK == 1)
                isStart = 1;

            S_Login loginOK = new S_Login();
            loginOK.LoginOK = isStart;
            loginOK.One = p1_id;
            loginOK.Two = p2_id;
            loginOK.Three = p3_id;
            loginOK.Four = p4_id;
            Send(loginOK);
        }

        public void HandleEnterGame(C_EnterGame enterGamePacket)
        {
            if (enterGamePacket.IsStarted == true)
            {
                MyPlayer = ObjectManager.Instance.Add<Player>();
                {
                    //MyPlayer.Info.Name = $"P_{MyPlayer.Info.ObjectId}";
                    MyPlayer.Info.Name = enterGamePacket.MyId;

                    Random rand = new Random();
                    int num = rand.Next(0, 5);

                    // 던전1 광장 앞
                    MyPlayer.Info.PosInfo.PosX = 1370 + num;
                    MyPlayer.Info.PosInfo.PosY = 226;
                    MyPlayer.Info.PosInfo.PosZ = 4930 + num;

                    // 던전1 입구 앞
                    //MyPlayer.Info.PosInfo.PosX = 1436 + num;
                    //MyPlayer.Info.PosInfo.PosY = 263;
                    //MyPlayer.Info.PosInfo.PosZ = 4935 + num;

                    // 던전1 보스 몬스터
                    //MyPlayer.Info.PosInfo.PosX = 1394 + num;
                    //MyPlayer.Info.PosInfo.PosY = 226;
                    //MyPlayer.Info.PosInfo.PosZ = 4903 + num;

                    // 던전2 입구
                    //MyPlayer.Info.PosInfo.PosX = 1473.947f;
                    //MyPlayer.Info.PosInfo.PosY = 285.9502f;
                    //MyPlayer.Info.PosInfo.PosZ = 1997.672f;

                    // 마을 앞
                    //MyPlayer.Info.PosInfo.PosX = 2235 + num;
                    //MyPlayer.Info.PosInfo.PosY = 120;
                    //MyPlayer.Info.PosInfo.PosZ = 3455 + num;

                    // 벽 클라이밍 
                    //MyPlayer.Info.PosInfo.PosX = 1443;
                    //MyPlayer.Info.PosInfo.PosY = 296;
                    //MyPlayer.Info.PosInfo.PosZ = 2286;

                    // 사다리 클라이밍
                    //MyPlayer.Info.PosInfo.PosX = 1507;
                    //MyPlayer.Info.PosInfo.PosY = 368;
                    //MyPlayer.Info.PosInfo.PosZ = 2303;

                    // 던전2 발판 밟기
                    //MyPlayer.Info.PosInfo.PosX = 1447;
                    //MyPlayer.Info.PosInfo.PosY = 285;
                    //MyPlayer.Info.PosInfo.PosZ = 2305;

                    // 던전2 전구 키기
                    //MyPlayer.Info.PosInfo.PosX = 1472;
                    //MyPlayer.Info.PosInfo.PosY = 241;
                    //MyPlayer.Info.PosInfo.PosZ = 2272;

                    // 던전2 점프 맵
                    //MyPlayer.Info.PosInfo.PosX = 1490;
                    //MyPlayer.Info.PosInfo.PosY = 246;
                    //MyPlayer.Info.PosInfo.PosZ = 2327;

                    // 던전2 보스
                    //MyPlayer.Info.PosInfo.PosX = 1495;
                    //MyPlayer.Info.PosInfo.PosY = 330;
                    //MyPlayer.Info.PosInfo.PosZ = 2293;

                    // 던전3 보스
                    //MyPlayer.Info.PosInfo.PosX = 5324;
                    //MyPlayer.Info.PosInfo.PosY = 60;
                    //MyPlayer.Info.PosInfo.PosZ = 5090;
                }

                MyPlayer.Session = this;

                GameRoom room = RoomManager.Instance.Find(1);
                room.Push(room.EnterGame, MyPlayer);
            }
        }


    }
}
