﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{
    public class Player : GameObject
    {
        public ClientSession Session { get; set; }

        public Player()
        {
            ObjectType = GameObjectType.Player;

            StatInfo.MaxHp = 100;
            StatInfo.Hp = 100;
        }

        public override void Update()
        {
            SelfRecovery();
        }

        int _recoveryTick = 0;
        private void SelfRecovery()
        {
            if (_recoveryTick > Environment.TickCount64)
                return;
            _recoveryTick = Environment.TickCount + 3000;

            if (StatInfo.Hp < 100 && StatInfo.Hp > 0)
            {
                StatInfo.Hp += 5;
                BroadcastStat();
            }
        }

        void BroadcastStat()
        {
            S_ChangeStat changePacket = new S_ChangeStat();
            changePacket.ObjectId = Id;
            changePacket.StatInfo = StatInfo;
            Room.Broadcast(changePacket);
        }
    }
}
