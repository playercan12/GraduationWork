﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{
    class Boss1 : GameObject
    {
        private bool IsDie = false;

        public Boss1()
        {
            ObjectType = GameObjectType.Bossone;

            State = State.Idle;
            Pattern = Pattern.No;

            StatInfo.MaxHp = 1000;
            StatInfo.Hp = 1000;
        }

        public override void Update()
        {
            if (!IsDie)
                CalcPattern();

            if (IsDie)
                return;
        }

        int _patternOneTick = 0;
        int _patternTwoTick = 0;
        int _patternThreeTick = 0;
        int _patternFourTick = 0;
        int _randomPatternTick = 0;

        private void CalcPattern()
        {
            float hpPer = ((float)(float)StatInfo.Hp / (float)StatInfo.MaxHp) * 100;

            if (hpPer <= 99.0f && hpPer >= 80.0f)
            {
                if (_patternOneTick > Environment.TickCount64)
                    return;
                _patternOneTick = Environment.TickCount + 10000;

                Pattern = Pattern.One;
                BroadcastBoss();
                SpawnMonster();
            }

            if (hpPer < 80.0f && hpPer >= 50.0f)
            {
                Random rand = new Random();
                int randPattern = rand.Next(1, 3);

                if (randPattern == 1)
                {
                    if (_patternOneTick > Environment.TickCount64)
                        return;
                    _patternOneTick = Environment.TickCount + 10000;

                    Pattern = Pattern.One;
                    BroadcastBoss();
                    SpawnMonster();
                }

                else if (randPattern == 2)
                {
                    if (_patternTwoTick > Environment.TickCount64)
                        return;
                    _patternTwoTick = Environment.TickCount + 8000;

                    Pattern = Pattern.Two;
                    BroadcastBoss();
                }
            }

            if (hpPer < 50.0f && hpPer >= 30.0f)
            {
                Random rand = new Random();
                int randPattern = rand.Next(1, 4);

                if (randPattern == 1)
                {
                    if (_patternOneTick > Environment.TickCount64)
                        return;
                    _patternOneTick = Environment.TickCount + 12000;

                    Pattern = Pattern.One;
                    BroadcastBoss();
                    SpawnMonster();
                }

                else if (randPattern == 2)
                {
                    if (_patternTwoTick > Environment.TickCount64)
                        return;
                    _patternTwoTick = Environment.TickCount + 10000;

                    Pattern = Pattern.Two;
                    BroadcastBoss();
                }

                else if (randPattern == 3)
                {
                    if (_patternThreeTick > Environment.TickCount64)
                        return;
                    _patternThreeTick = Environment.TickCount + 8000;

                    Pattern = Pattern.Three;
                    BroadcastBoss();
                }

            }

            if (hpPer < 30.0f && hpPer > 0.0f)
            {

                Random rand = new Random();
                int randPattern = 0;
                {
                    if (_randomPatternTick > Environment.TickCount)
                        return;
                    _randomPatternTick = Environment.TickCount + 2000;

                    randPattern = rand.Next(1, 5);
                }

                if (randPattern == 1)
                {
                    if (_patternOneTick > Environment.TickCount64)
                        return;
                    _patternOneTick = Environment.TickCount + 12000;

                    Pattern = Pattern.One;
                    BroadcastBoss();
                    SpawnMonster();
                }

                else if (randPattern == 2)
                {
                    if (_patternTwoTick > Environment.TickCount64)
                        return;
                    _patternTwoTick = Environment.TickCount + 10000;

                    Pattern = Pattern.Two;
                    BroadcastBoss();
                }

                else if (randPattern == 3)
                {
                    if (_patternThreeTick > Environment.TickCount64)
                        return;
                    _patternThreeTick = Environment.TickCount + 8000;

                    Pattern = Pattern.Three;
                    BroadcastBoss();
                }

                else if (randPattern == 4)
                {
                    if (_patternFourTick > Environment.TickCount64)
                        return;
                    _patternFourTick = Environment.TickCount + 6000;

                    StatInfo.Speed = 1;
                    int r = rand.Next(1, 12);
                    RandWall = r;
                    Pattern = Pattern.Four;

                    BroadcastBoss();
                }
            }

            if (hpPer <= 0.0f)
            {
                IsDie = true;
            }
        }

        void BroadcastBoss()
        {
            S_Move bossPatternPacket = new S_Move();
            bossPatternPacket.ObjectId = Id;
            bossPatternPacket.PosInfo = PosInfo;
            Room.Broadcast(bossPatternPacket);
        }

        void SpawnMonster()
        {
            GameRoom room = Room;

            Random rand = new Random();
            int Left_randX = rand.Next(0, 4);
            int Left_randZ = rand.Next(0, 4);
            int Right_randX = rand.Next(0, 4);
            int Right_randZ = rand.Next(0, 4);

            NavRMonster n_rmonster = ObjectManager.Instance.Add<NavRMonster>();
            n_rmonster.HP = 200;
            n_rmonster.CellPos = new Vector3(1406f - Left_randX, 226f, 4911f);
            room.EnterGame(n_rmonster);

            NavCMonster p_cmonster = ObjectManager.Instance.Add<NavCMonster>();
            p_cmonster.HP = 200;
            p_cmonster.CellPos = new Vector3(1409f - Right_randX, 226f, 4904f);
            room.EnterGame(p_cmonster);
        }
    }
}


































//using Google.Protobuf.Protocol;
//using System;
//using System.Collections.Generic;
//using System.Numerics;
//using System.Text;

//namespace Server.Game
//{
//    class Boss1 : GameObject
//    {
//        Player _target;

//        private bool IsPatternStart = false;
//        private bool IsDie = false;

//        public Boss1()
//        {
//            ObjectType = GameObjectType.Bossone;

//            State = State.Idle;
//            Pattern = Pattern.No;

//            StatInfo.MaxHp = 1000;
//            StatInfo.Hp = 1000;
//        }

//        public override void Update()
//        {
//            if (StatInfo.Hp <= 0)
//            {
//                IsDie = true;
//                return;
//            }

//            if (!IsDie)
//            {
//                // 처음에 플레이어를 찾다가 
//                if (IsPatternStart == false)
//                    FindClosedPlayer();

//                // 플레이어가 찾아지면 Pattern을 랜덤하게 주기 시작한다
//                if (IsPatternStart == true)
//                    RandPattern();

//                // 랜덤하게 주어진 Pattern에 따라 동작해야할 행동을 준다
//                switch (Pattern)
//                {
//                    case Pattern.No:
//                        FindClosedPlayer();
//                        break;
//                    case Pattern.One:
//                        RandNum();
//                        break;
//                }
//            }
//        }

//        int _checkPlayerTick = 0;
//        private void FindClosedPlayer()
//        {
//            if (_checkPlayerTick > Environment.TickCount64)
//                return;
//            _checkPlayerTick = Environment.TickCount + 10000;

//            Player target = Room.FindPlayer(p =>
//            {
//                float dist = DistanceToPoint(p.CellPos, CellPos);
//                //Console.WriteLine("거리" + dist);
//                return dist <= 10f;
//            });

//            if (target == null)
//                return;

//            _target = target;

//            //Pattern = Pattern.One;
//            //BroadcastBoss();

//            IsPatternStart = true;
//        }

//        int _RandWallTick = 0;
//        private void RandNum()
//        {
//            if (_RandWallTick > Environment.TickCount64)
//                return;
//            _RandWallTick = Environment.TickCount + 3000;

//            Random rand = new Random();
//            StatInfo.Speed = 1;
//            int r = rand.Next(1, 12);
//            RandWall = r;

//            // 이 시기에 생성할 수 있도록 하자..
//            BroadcastBoss();
//        }

//        // 15초 마다 4개의 패턴을 랜덤하게 주자.
//        int _RandPatternTick = 0;
//        private void RandPattern()
//        {
//            if (_RandPatternTick > Environment.TickCount64)
//                return;
//            _RandPatternTick = Environment.TickCount + 5000;

//            Random rand = new Random();

//            int randPattern = rand.Next(1, 5);

//            randPattern = 2;

//            if (randPattern == 1)
//            {
//                Pattern = Pattern.One;                
//                BroadcastBoss();
//            }

//            if (randPattern == 2)
//            {
//                Pattern = Pattern.Two;
//                BroadcastBoss();
//            }

//            if (randPattern == 3)
//            {
//                Pattern = Pattern.Three;
//                BroadcastBoss();
//            }

//            if (randPattern == 4)
//            {
//                Pattern = Pattern.Four;

//                StatInfo.Speed = 1;
//                int r = rand.Next(1, 12);
//                RandWall = r;
//                Console.WriteLine(RandWall);
//                BroadcastBoss();
//            }
//        }

//        private float DistanceToPoint(Vector3 a, Vector3 b)
//        {
//            return (float)Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2) + Math.Pow(a.Z - b.Z, 2));
//        }

//        void BroadcastBoss()
//        {
//            S_Move bossPatternPacket = new S_Move();
//            bossPatternPacket.ObjectId = Id;
//            bossPatternPacket.PosInfo = PosInfo;
//            Room.Broadcast(bossPatternPacket);
//        }
//    }
//}

