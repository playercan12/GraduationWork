﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{
    public class PatrolCMonster : MonsterSpawner
    {
        private bool checkHitted = false;

        public void SetHitted(bool check)
        {
            checkHitted = check;
        }

        private float rand_x;
        private float rand_z;

        public PatrolCMonster()
        {
            ObjectType = GameObjectType.Cmonster;

            State = State.Idle;

            StatInfo.MaxHp = 400;
            StatInfo.Hp = 400;
        }

        public override void Update()
        {
            DieAndSpawn();
            RandomPos();
        }

        int _randTick = 0;
        private void RandomPos()
        {            
            Random rand = new Random();

            if (_randTick > Environment.TickCount64)
                return;
            _randTick = Environment.TickCount + 5000;
            
            float minX, maxX, minZ, maxZ;
            float range = 10f;

            minX = CellPos.X - range;
            maxX = CellPos.X + range;
            minZ = CellPos.Z - range;
            maxZ = CellPos.Z + range;

            float f = (float)rand.NextDouble();

            rand_x = (f * 20f) + minX;
            rand_z = (f * 20f) + minZ;

            this.PosInfo.SpineX = rand_x;
            this.PosInfo.SpineZ = rand_z;

            BroadcastMove();            
        }

        void DieAndSpawn()
        {
            if (checkHitted)
            {
                GameRoom room = Room;
                Random rand = new Random();

                if (room == null)
                    return;

                float randX = ((float)rand.NextDouble()) * 5000.0f + 500.0f;
                float randZ = ((float)rand.NextDouble()) * 5000.0f + 500.0f;

                if (StatInfo.Hp <= 0)
                {
                    PatrolCMonster cmonster = ObjectManager.Instance.Add<PatrolCMonster>();

                    cmonster.StatInfo.Hp = this.StatInfo.MaxHp;
                    cmonster.CellPos = new Vector3(randX, 0f, randZ);
                    room.EnterGame(cmonster);
                }

                checkHitted = false;
            }
        }

        void BroadcastMove()
        {
            S_Move movePacket = new S_Move();
            movePacket.ObjectId = Id;
            movePacket.PosInfo = PosInfo;
            Room.Broadcast(movePacket);
        }

    }
}