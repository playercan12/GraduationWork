﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{
    public class PatrolRMonster : MonsterSpawner
    {
        private bool checkHitted = false;

        public void SetHitted(bool check)
        {
            checkHitted = check;
        }

        private float rand_x;
        private float rand_z;

        public PatrolRMonster()
        {
            ObjectType = GameObjectType.Rmonster;

            State = State.Idle;

            StatInfo.MaxHp = 300;
            StatInfo.Hp = 300;

            //Random rand = new Random();
            //float r = ( (float)rand.NextDouble() + 0.01f ) * 3.0f; // 0~1사이 소수값            
            //PosInfo.SpineY = r;
        }

        public override void Update()
        {
            DieAndSpawn();
            RandomPos();
            coolAttack();
        }

        int _randTick = 0;
        private void RandomPos()
        {
            Random rand = new Random();

            if (_randTick > Environment.TickCount64)
                return;
            _randTick = Environment.TickCount + 3000;

            float minX, maxX, minZ, maxZ;
            float range = 10f;

            minX = CellPos.X - range;
            maxX = CellPos.X + range;
            minZ = CellPos.Z - range;
            maxZ = CellPos.Z - range;

            float f = (float)rand.NextDouble();

            rand_x = (f * 20f) + minX;
            rand_z = (f * 20f) + minZ;

            this.PosInfo.SpineX = rand_x;
            this.PosInfo.SpineZ = rand_z;

            BroadcastMove();
        }

        int _attackTick = 0;
        private void coolAttack()
        {
            if (_attackTick > Environment.TickCount64)
                return;
            _attackTick = Environment.TickCount + 4000;

            Random rand = new Random();
            float coolTime = ((float)rand.NextDouble()) * 2.0f;            

            PosInfo.SpineY = 1.0f + coolTime;
            BroadcastMove();
        }

        void DieAndSpawn()
        {
            if (checkHitted)
            {
                GameRoom room = Room;
                Random rand = new Random();

                if (room == null)
                    return;

                float randX = ((float)rand.NextDouble()) * 5000.0f;
                float randZ = ((float)rand.NextDouble()) * 5000.0f;

                if (StatInfo.Hp <= 0)
                {
                    PatrolRMonster rmonster = ObjectManager.Instance.Add<PatrolRMonster>();

                    rmonster.StatInfo.Hp = this.StatInfo.MaxHp;
                    rmonster.CellPos = new Vector3(randX, 0f, randZ);
                    room.EnterGame(rmonster);
                }

                checkHitted = false;
            }
        }

        void BroadcastMove()
        {
            S_Move movePacket = new S_Move();
            movePacket.ObjectId = Id;
            movePacket.PosInfo = PosInfo;
            Room.Broadcast(movePacket);
        }
    }
}