﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{
    public class NavRMonster : MonsterSpawner
    {
        private bool checkHitted = false;

        public void SetHitted(bool check)
        {
            checkHitted = check;
        }

        public NavRMonster()
        {
            ObjectType = GameObjectType.Navrmonster;

            StatInfo.MaxHp = 300;
            StatInfo.Hp = 300;

            //Random rand = new Random();
            //float r = ((float)rand.NextDouble() + 0.01f) * 3.0f; // 0~2사이 소수값            
            //PosInfo.SpineY = r;
        }

        public override void Update()
        {
            DieAndSpawn();
            coolAttack();
        }

        int _attackTick = 0;
        private void coolAttack()
        {
            if (_attackTick > Environment.TickCount64)
                return;
            _attackTick = Environment.TickCount + 5000;

            Random rand = new Random();
            float coolTime = ((float)rand.NextDouble()) * 4.0f;

            PosInfo.SpineY = 1.0f + coolTime;
            BroadcastMove();
        }

        void DieAndSpawn()
        {
            if (checkHitted)
            {
                GameRoom room = Room;
                Random rand = new Random();

                if (room == null)
                    return;

                float randX = ((float)rand.NextDouble()) * 5000.0f + 500.0f;
                float randZ = ((float)rand.NextDouble()) * 5000.0f + 500.0f;

                if (StatInfo.Hp <= 0)
                {
                    NavRMonster rmonster = ObjectManager.Instance.Add<NavRMonster>();

                    rmonster.StatInfo.Hp = this.StatInfo.MaxHp;
                    rmonster.CellPos = new Vector3(randX, 0f, randZ);
                    room.EnterGame(rmonster);
                }

                checkHitted = false;
            }
        }

        void BroadcastMove()
        {
            S_Move movePacket = new S_Move();
            movePacket.ObjectId = Id;
            movePacket.PosInfo = PosInfo;
            Room.Broadcast(movePacket);
        }
    }
}

