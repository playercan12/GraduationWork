﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{
    public class PatrolDMonster : MonsterSpawner
    {
        private bool checkHitted = false;

        public void SetHitted(bool check)
        {
            checkHitted = check;
        }

        private float rand_x;
        private float rand_z;

        public PatrolDMonster()
        {
            ObjectType = GameObjectType.Dmonster;

            StatInfo.MaxHp = 250;
            StatInfo.Hp = 250;

            Random rand = new Random();
            int r = rand.Next(5, 20);
            PosInfo.RotY = r;
        }

        public override void Update()
        {
            DieAndSpawn();
            RandomPos();
            coolAttack();
        }

        int _randTick = 0;
        private void RandomPos()
        {
            Random rand = new Random();

            if (_randTick > Environment.TickCount64)
                return;
            _randTick = Environment.TickCount + 5000;

            float minX, maxX, minZ, maxZ;
            float range = 50f;

            minX = CellPos.X - range;
            maxX = CellPos.X + range;
            minZ = CellPos.Z - range;
            maxZ = CellPos.Z + range;

            float f = (float)rand.NextDouble();

            rand_x = (f * 100f) + minX;
            rand_z = (f * 100f) + minZ;

            this.PosInfo.SpineX = rand_x;
            this.PosInfo.SpineZ = rand_z;

            BroadcastMove();
        }

        int _attackTick = 0;
        private void coolAttack()
        {
            if (_attackTick > Environment.TickCount64)
                return;
            _attackTick = Environment.TickCount + 7000;

            Random rand = new Random();
            float coolTime = ((float)rand.NextDouble()) * 3.0f;

            PosInfo.SpineY = 3.0f + coolTime;
            BroadcastMove();
        }

        void DieAndSpawn()
        {
            if (checkHitted)
            {
                GameRoom room = Room;
                Random rand = new Random();

                if (room == null)
                    return;

                int randX = rand.Next(500, 5500);
                int randZ = rand.Next(500, 5500);

                if (StatInfo.Hp <= 0)
                {
                    PatrolDMonster dmonster = ObjectManager.Instance.Add<PatrolDMonster>();

                    dmonster.StatInfo.Hp = this.StatInfo.MaxHp;
                    dmonster.CellPos = new Vector3(randX, 0f, randZ);
                    room.EnterGame(dmonster);
                }

                checkHitted = false;
            }
        }

        void BroadcastMove()
        {
            S_Move movePacket = new S_Move();
            movePacket.ObjectId = Id;
            movePacket.PosInfo = PosInfo;
            Room.Broadcast(movePacket);
        }
    }
}

