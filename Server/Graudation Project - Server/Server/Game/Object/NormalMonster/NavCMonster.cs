﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;


namespace Server.Game
{
    public class NavCMonster : MonsterSpawner
    {
        private bool checkHitted = false;

        public void SetHitted(bool check)
        {
            checkHitted = check;
        }

        public NavCMonster()
        {
            ObjectType = GameObjectType.Navcmonster;

            StatInfo.MaxHp = 400;
            StatInfo.Hp = 400;
        }

        public override void Update()
        {
            DieAndSpawn();
            coolAttack();
        }

        int _attackTick = 0;
        private void coolAttack()
        {
            if (_attackTick > Environment.TickCount64)
                return;
            _attackTick = Environment.TickCount + 6000;

            Random rand = new Random();
            float coolTime = ((float)rand.NextDouble()) * 3.5f;

            PosInfo.SpineY = 2.5f + coolTime;
            BroadcastMove();
        }

        void DieAndSpawn()
        {
            if (checkHitted)
            {
                GameRoom room = Room;
                Random rand = new Random();

                if (room == null)
                    return;

                float randX = ((float)rand.NextDouble()) * 5000.0f + 500.0f;
                float randZ = ((float)rand.NextDouble()) * 5000.0f + 500.0f;

                if (StatInfo.Hp <= 0)
                {                    
                    NavCMonster cmonster = ObjectManager.Instance.Add<NavCMonster>();

                    cmonster.StatInfo.Hp = this.StatInfo.MaxHp;
                    cmonster.CellPos = new Vector3(randX, 0f, randZ);
                    room.EnterGame(cmonster);                    
                }

                checkHitted = false;
            }
        }

        void BroadcastMove()
        {
            S_Move movePacket = new S_Move();
            movePacket.ObjectId = Id;
            movePacket.PosInfo = PosInfo;
            Room.Broadcast(movePacket);
        }
    }
}

