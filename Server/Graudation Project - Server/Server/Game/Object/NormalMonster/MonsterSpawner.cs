﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{

    public class MonsterSpawner : GameObject
    {
        public static int CM_count;
        public int RM_count;
        public int DM_count;

        public MonsterSpawner()
        {
            ObjectType = GameObjectType.Monsterspawner;
        }

        public override void Update()
        {
            if (CM_count < 2000)
                SpawnClosedMonster();

            if (RM_count < 2000)
                SpawnRangedMonster();

            if (DM_count < 1000)
                SpawnDroneMonster();
        }

        int _spawnCMTick = 0;
        int _spawnRMTick = 0;
        int _spawnDMTick = 0;

        void SpawnClosedMonster()
        {
            if (_spawnCMTick > Environment.TickCount64)
                return;
            _spawnCMTick = Environment.TickCount + 10000;

            GameRoom room = Room;

            Random rand = new Random();

            float nc_randX = ((float)rand.NextDouble()) * 6000.0f;
            float nc_randZ = ((float)rand.NextDouble()) * 6000.0f;
            float pc_randX = ((float)rand.NextDouble()) * 6000.0f;
            float pc_randZ = ((float)rand.NextDouble()) * 6000.0f;         

            NavCMonster n_cmonster = ObjectManager.Instance.Add<NavCMonster>();
            n_cmonster.CellPos = new Vector3(nc_randX, 0f, nc_randZ);
            room.EnterGame(n_cmonster);

            PatrolCMonster p_cmonster = ObjectManager.Instance.Add<PatrolCMonster>();
            p_cmonster.CellPos = new Vector3(pc_randX, 0f, pc_randZ);
            room.EnterGame(p_cmonster);

            CM_count++;
        }

        void SpawnRangedMonster()
        {
            if (_spawnRMTick > Environment.TickCount64)
                return;
            _spawnRMTick = Environment.TickCount + 350;

            GameRoom room = Room;

            Random rand = new Random();

            float nr_randX = ((float)rand.NextDouble()) * 6000.0f;
            float nr_randZ = ((float)rand.NextDouble()) * 6000.0f;
            float pr_randX = ((float)rand.NextDouble()) * 6000.0f;
            float pr_randZ = ((float)rand.NextDouble()) * 6000.0f;
            
            NavRMonster n_rmonster = ObjectManager.Instance.Add<NavRMonster>();
            n_rmonster.CellPos = new Vector3(nr_randX, 0f, nr_randZ);
            room.EnterGame(n_rmonster);

            PatrolRMonster p_rmonster = ObjectManager.Instance.Add<PatrolRMonster>();
            p_rmonster.CellPos = new Vector3(pr_randX, 0f, pr_randZ);
            room.EnterGame(p_rmonster);

            RM_count++;
        }

        void SpawnDroneMonster()
        {
            if (_spawnDMTick > Environment.TickCount64)
                return;
            _spawnDMTick = Environment.TickCount + 520;

            GameRoom room = Room;

            Random rand = new Random();
            int nc_randX = rand.Next(0, 6000);
            int nc_randZ = rand.Next(0, 6000);
            int pc_randX = rand.Next(0, 6000);
            int pc_randZ = rand.Next(0, 6000);

            NavDMonster n_dmonster = ObjectManager.Instance.Add<NavDMonster>();
            n_dmonster.CellPos = new Vector3(nc_randX, 0f, nc_randZ);
            room.EnterGame(n_dmonster);

            PatrolDMonster p_dmonster = ObjectManager.Instance.Add<PatrolDMonster>();
            p_dmonster.CellPos = new Vector3(pc_randX, 0f, pc_randZ);
            room.EnterGame(p_dmonster);

            DM_count++;
        }

    }
}
