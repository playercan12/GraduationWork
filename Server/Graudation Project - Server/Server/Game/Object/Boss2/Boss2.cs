﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{
    class Boss2 : GameObject
    {
        private bool boss2_PatternStart = false;

        public void setIsStart(bool isStart)
        {
            boss2_PatternStart = isStart;
        }

        public Boss2()
        {
            ObjectType = GameObjectType.Bosstwo;

            State = State.Idle;
            Pattern = Pattern.No;

            StatInfo.MaxHp = 1000;
            StatInfo.Hp = 1000;
        }

        public override void Update()
        {
            if (boss2_PatternStart)            
                CalcPattern();           
        }

        int _patternOneTick = 0;
        int _patternTwoTick = 0;
        int _patternThreeTick = 0;
        int _patternFourTick = 0;
        int _randomPatternTick = 0;

        private void CalcPattern()
        {
            float hpPer = ((float)(float)StatInfo.Hp / (float)StatInfo.MaxHp) * 100;

            if (hpPer <= 100.0f && hpPer >= 80.0f)
            {
                if (_patternOneTick > Environment.TickCount64)
                    return;
                _patternOneTick = Environment.TickCount + 12000;

                Pattern = Pattern.One;
                BroadcastBoss();
                SpawnMonster();
            }

            if (hpPer < 80.0f && hpPer >= 50.0f)
            {
                Random rand = new Random();
                int randPattern = rand.Next(1, 3);

                if (randPattern == 1) 
                {
                    if (_patternOneTick > Environment.TickCount64)
                        return;
                    _patternOneTick = Environment.TickCount + 12000;

                    Pattern = Pattern.One;
                    BroadcastBoss();
                    SpawnMonster();
                }

                else if (randPattern == 2)
                {
                    if (_patternTwoTick > Environment.TickCount64)
                        return;
                    _patternTwoTick = Environment.TickCount + 8000;

                    Pattern = Pattern.Two;
                    BroadcastBoss();
                }
            }

            if (hpPer < 50.0f && hpPer >= 30.0f)
            {
                Random rand = new Random();
                int randPattern = rand.Next(1, 4);

                if (randPattern == 1)
                {
                    if (_patternOneTick > Environment.TickCount64)
                        return;
                    _patternOneTick = Environment.TickCount + 12000;

                    Pattern = Pattern.One;
                    BroadcastBoss();
                    SpawnMonster();
                }

                else if (randPattern == 2)
                {
                    if (_patternTwoTick > Environment.TickCount64)
                        return;
                    _patternTwoTick = Environment.TickCount + 8000;

                    Pattern = Pattern.Two;
                    BroadcastBoss();
                }

                else if (randPattern == 3)
                {
                    if (_patternThreeTick > Environment.TickCount64)
                        return;
                    _patternThreeTick = Environment.TickCount + 6000;

                    Pattern = Pattern.Three;
                    BroadcastBoss();
                }
            }

            if (hpPer < 30.0f && hpPer > 0.0f)
            {

                Random rand = new Random();
                int randPattern = 0;
                { 
                    if (_randomPatternTick > Environment.TickCount)
                        return;
                    _randomPatternTick = Environment.TickCount + 2000;

                    randPattern = rand.Next(1, 5);
                }

                if (randPattern == 1)
                {
                    if (_patternOneTick > Environment.TickCount64)
                        return;
                    _patternOneTick = Environment.TickCount + 12000;

                    Pattern = Pattern.One;
                    BroadcastBoss();
                    SpawnMonster();
                }

                else if (randPattern == 2)
                {
                    if (_patternTwoTick > Environment.TickCount64)
                        return;
                    _patternTwoTick = Environment.TickCount + 4000;

                    Pattern = Pattern.Two;
                    BroadcastBoss();
                }

                else if (randPattern == 3)
                {
                    if (_patternThreeTick > Environment.TickCount64)
                        return;
                    _patternThreeTick = Environment.TickCount + 6000;

                    Pattern = Pattern.Three;
                    BroadcastBoss();
                }

                else if (randPattern == 4)
                {
                    if (_patternFourTick > Environment.TickCount64)
                        return;
                    _patternFourTick = Environment.TickCount + 8000;

                    Pattern = Pattern.Four;
                    BroadcastBoss();
                }
            }

            if (hpPer <= 0.0f)
            {
                boss2_PatternStart = false;
            }
        }

        void SpawnMonster()
        {
            GameRoom room = Room;

            NavDMonster n_dmonster = ObjectManager.Instance.Add<NavDMonster>();
            n_dmonster.HP = 200;
            n_dmonster.CellPos = new Vector3(1472f , 350f , 2300f );
            room.EnterGame(n_dmonster);

            PatrolDMonster p_dmonster = ObjectManager.Instance.Add<PatrolDMonster>();
            p_dmonster.HP = 200;
            p_dmonster.CellPos = new Vector3(1472f, 350f, 2300f);
            room.EnterGame(p_dmonster);
        }

        void BroadcastBoss()
        {
            S_Move bossPatternPacket = new S_Move();
            bossPatternPacket.ObjectId = Id;
            bossPatternPacket.PosInfo = PosInfo;
            Room.Broadcast(bossPatternPacket);
        }
    }
}

