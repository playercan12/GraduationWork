﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{
    class Boss3 : GameObject
    {
        private bool IsDie = false;

        public Boss3()
        {
            ObjectType = GameObjectType.Bossthree;

            State = State.Idle;
            Pattern = Pattern.No;

            StatInfo.MaxHp = 1000;
            StatInfo.Hp = 1000;
        }

        public override void Update()
        {
            if (((float)(float)StatInfo.Hp / (float)StatInfo.MaxHp) * 100 <= 99.0f && !IsDie)
            {
                CalcPattern();
            }
        }

        int _patternOneTick = 0;
        int _patternTwoTick = 0;
        int _patternThreeTick = 0;
        int _spawnTick = 0;

        private void CalcPattern()
        {
            float hpPer = ((float)(float)StatInfo.Hp / (float)StatInfo.MaxHp) * 100;

            if (hpPer <= 100.0f && hpPer >= 80.0f)
            {
                if (_patternOneTick > Environment.TickCount64)
                    return;
                _patternOneTick = Environment.TickCount + 3000;

                Random rand = new Random();
                int rand_pattern_type = rand.Next(1, 3);

                Pattern = Pattern.One;
                RandWall = rand_pattern_type; // 패킷에 RandWall을 이용

                BroadcastBoss();
            }

            if (hpPer < 80.0f && hpPer > 50.0f)
            {
                if (_patternTwoTick > Environment.TickCount64)
                    return;
                _patternTwoTick = Environment.TickCount + 3000;

                Random rand = new Random();
                int rand_pattern_type = rand.Next(1, 3);

                Pattern = Pattern.Two;
                RandWall = rand_pattern_type; // 패킷에 RandWall을 이용

                BroadcastBoss();
            }

            if (hpPer < 50.0f)
            {
                // 패턴3은 3초마다 하되 몬스터 스폰은 5초마다 하자

                if (_patternThreeTick > Environment.TickCount64)
                    return;
                _patternThreeTick = Environment.TickCount + 3000;

                Pattern = Pattern.Three;
                BroadcastBoss();

                if (_spawnTick > Environment.TickCount64)
                    return;
                _spawnTick = Environment.TickCount + 5000;
                SpawnMonster();
            }

            if (hpPer <= 0.0f)
            {
                IsDie = true;
            }
        }

        void BroadcastBoss()
        {
            S_Move bossPatternPacket = new S_Move();
            bossPatternPacket.ObjectId = Id;
            bossPatternPacket.PosInfo = PosInfo;
            Room.Broadcast(bossPatternPacket);
        }


        void SpawnMonster()
        {
            Random rand = new Random();
            int c_randX = rand.Next(5249, 5472);
            int c_randZ = rand.Next(4601, 4772);
            int r_randX = rand.Next(5249, 5472);
            int r_randZ = rand.Next(4601, 4772);
            int d_randX = rand.Next(5249, 5472);
            int d_randZ = rand.Next(4601, 4772);

            GameRoom room = Room;

            NavCMonster n_cmonster = ObjectManager.Instance.Add<NavCMonster>();
            n_cmonster.HP = 200;
            n_cmonster.CellPos = new Vector3(c_randX, 0f, c_randZ);
            room.EnterGame(n_cmonster);

            NavRMonster n_rmonster = ObjectManager.Instance.Add<NavRMonster>();
            n_rmonster.HP = 200;
            n_rmonster.CellPos = new Vector3(r_randX, 0f, r_randZ);
            room.EnterGame(n_rmonster);

            PatrolDMonster p_dmonster = ObjectManager.Instance.Add<PatrolDMonster>();
            p_dmonster.HP = 200;
            p_dmonster.CellPos = new Vector3(d_randX, 70f, d_randZ);
            room.EnterGame(p_dmonster);
        }
    }
}

