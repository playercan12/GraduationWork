﻿using Google.Protobuf;
using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Server.Game
{
    public class GameRoom : JobSerializer
    {
        //object _lock = new object();
        public int RoomId { get; set; }

        public int attacker { get; set; }
        public bool m_damaged { get; set; }


        Dictionary<int, Player> _players = new Dictionary<int, Player>();
        Dictionary<int, MonsterSpawner> _monsterspawner = new Dictionary<int, MonsterSpawner>();
        Dictionary<int, PatrolCMonster> _Pcmonsters = new Dictionary<int, PatrolCMonster>();
        Dictionary<int, PatrolRMonster> _Prmonsters = new Dictionary<int, PatrolRMonster>();
        Dictionary<int, PatrolDMonster> _Pdmonsters = new Dictionary<int, PatrolDMonster>();
        Dictionary<int, NavCMonster> _Ncmonsters = new Dictionary<int, NavCMonster>();
        Dictionary<int, NavRMonster> _Nrmonsters = new Dictionary<int, NavRMonster>();
        Dictionary<int, NavDMonster> _Ndmonsters = new Dictionary<int, NavDMonster>();
        Dictionary<int, Boss1> _boss1 = new Dictionary<int, Boss1>();
        Dictionary<int, Boss2> _boss2 = new Dictionary<int, Boss2>();
        Dictionary<int, Boss3> _boss3 = new Dictionary<int, Boss3>();

        Dictionary<int, Buggy> _buggy = new Dictionary<int, Buggy>();
        Dictionary<int, Button> _button = new Dictionary<int, Button>();
        Dictionary<int, Door> _door = new Dictionary<int, Door>();
        Dictionary<int, DoorManager> _doorManager = new Dictionary<int, DoorManager>();

        Dictionary<int, ObsCore> _core = new Dictionary<int, ObsCore>();

        public List<Vector3> CMonster_pos = new List<Vector3>();
        public List<Vector3> RMonster_pos = new List<Vector3>();
        public List<Vector3> DMonster_pos = new List<Vector3>();
        public List<Vector3> Wall_pos = new List<Vector3>();
        public List<Vector3> Buggy_pos = new List<Vector3>();


        public void Init(int mapId)
        {
            InitWallPos();
            InitCMosPos();
            InitRMosPos();
            InitDMonsPos();
            InitBuggyPos();

            Button button = ObjectManager.Instance.Add<Button>();
            button.CellPos = new Vector3(1f, 1f, 1f);
            button.Info.Name = $"InteractManager";
            //EnterGame(button);
            Push(EnterGame, button);

            DoorManager doorManager = ObjectManager.Instance.Add<DoorManager>();
            doorManager.Info.Name = $"SliceManager";
            Push(EnterGame, doorManager);

            Boss1 boss1 = ObjectManager.Instance.Add<Boss1>();
            boss1.CellPos = new Vector3(1412.6f, 225.914f, 4909.44f);
            boss1.Info.Name = $"BossOne";
            Push(EnterGame, boss1);

            Boss2 boss2 = ObjectManager.Instance.Add<Boss2>();
            boss2.CellPos = new Vector3(1460.071f, 377.272f, 2283.344f);
            boss2.Info.Name = $"BossTwo";
            Push(EnterGame, boss2);

            Boss3 boss3 = ObjectManager.Instance.Add<Boss3>();
            boss3.CellPos = new Vector3(5283.68f, 50.41f, 5091.73f);
            boss3.Info.Name = $"BossThree";
            Push(EnterGame, boss3);

            MonsterSpawner spawner = ObjectManager.Instance.Add<MonsterSpawner>();
            spawner.Info.Name = $"Monster Spawner";
            Push(EnterGame, spawner);

            for (int i = 0; i < 35; ++i)
            {
                Door boss3Wall1 = ObjectManager.Instance.Add<Door>();
                boss3Wall1.CellPos = Wall_pos[i];
                boss3Wall1.Info.Name = $"FirstWalls";
                Push(EnterGame, boss3Wall1);
            }

            for (int i = 35; i < Wall_pos.Count; ++i)
            {
                Door boss3Wall2 = ObjectManager.Instance.Add<Door>();
                boss3Wall2.CellPos = Wall_pos[i];
                boss3Wall2.Info.Name = $"SecondWalls";
                Push(EnterGame, boss3Wall2);
            }

            Door D1_door1 = ObjectManager.Instance.Add<Door>();
            Push(EnterGame, D1_door1);

            Door D1_door2 = ObjectManager.Instance.Add<Door>();
            Push(EnterGame, D1_door2);

            Door D1_door3 = ObjectManager.Instance.Add<Door>();
            Push(EnterGame, D1_door3);

            Door D1_door4 = ObjectManager.Instance.Add<Door>();
            Push(EnterGame, D1_door4);

            Door D2_floor3_1 = ObjectManager.Instance.Add<Door>();
            Push(EnterGame, D2_floor3_1);

            Door D2_floor3_2 = ObjectManager.Instance.Add<Door>();
            Push(EnterGame, D2_floor3_2);

            for (int i = 0; i < Buggy_pos.Count; ++i)
            {
                Buggy buggy = ObjectManager.Instance.Add<Buggy>();
                buggy.CellPos = Buggy_pos[i];
                EnterGame(buggy);
            }

            for (int i = 0; i < CMonster_pos.Count; ++i)
            {
                PatrolCMonster CMonster = ObjectManager.Instance.Add<PatrolCMonster>();
                CMonster.CellPos = CMonster_pos[i];
                EnterGame(CMonster);
            }

            for (int i = 0; i < RMonster_pos.Count; ++i)
            {
                PatrolRMonster RMonster = ObjectManager.Instance.Add<PatrolRMonster>();
                RMonster.CellPos = RMonster_pos[i];
                EnterGame(RMonster);
            }

            for (int i = 0; i < DMonster_pos.Count; ++i)
            {
                PatrolDMonster DMonster = ObjectManager.Instance.Add<PatrolDMonster>();
                DMonster.CellPos = DMonster_pos[i];
                EnterGame(DMonster);
            }

        }

        public void Update()
        {
            
            foreach (Player player in _players.Values)
            {
                player.Update();
            }

            foreach (MonsterSpawner Monseter_spawner in _monsterspawner.Values)
            {
                Monseter_spawner.Update();
            }

            foreach (PatrolCMonster CMonster in _Pcmonsters.Values)
            {
                CMonster.Update();
            }

            foreach (PatrolRMonster RMonster in _Prmonsters.Values)
            {
                RMonster.Update();
            }

            foreach (PatrolDMonster DMonster in _Pdmonsters.Values)
            {
                DMonster.Update();
            }

            foreach (NavCMonster NCMonster in _Ncmonsters.Values)
            {
                NCMonster.Update();
            }

            foreach (NavRMonster NRMonster in _Nrmonsters.Values)
            {
                NRMonster.Update();
            }

            foreach (NavDMonster DMonster in _Ndmonsters.Values)
            {
                DMonster.Update();
            }

            foreach (Boss1 boss1 in _boss1.Values)
            {
                boss1.Update();
            }

            foreach (Boss2 boss2 in _boss2.Values)
            {
                boss2.Update();
            }

            foreach (Boss3 boss3 in _boss3.Values)
            {
                boss3.Update();
            }

            Flush();
        }

        public void EnterGame(GameObject gameObject)
        {
            if (gameObject == null)
                return;

            GameObjectType type = ObjectManager.GetObjectTypeById(gameObject.Id);


            if (type == GameObjectType.Player)
            {
                Player player = gameObject as Player;
                _players.Add(gameObject.Id, player);
                player.Room = this;

                // 본인한테 본인에 대한 정보 보내기
                {
                    S_EnterGame enterPacket = new S_EnterGame();
                    enterPacket.Player = player.Info;
                    player.Session.Send(enterPacket);

                    // 본인한테 타인에 대한 정보 보내기
                    S_Spawn spawnPacket = new S_Spawn();
                    foreach (Player p in _players.Values)
                    {
                        if (player != p)
                            spawnPacket.Objects.Add(p.Info);
                    }

                    foreach (MonsterSpawner ms in _monsterspawner.Values)
                        spawnPacket.Objects.Add(ms.Info);

                    foreach (PatrolCMonster cm in _Pcmonsters.Values)
                        spawnPacket.Objects.Add(cm.Info);

                    foreach (PatrolRMonster rm in _Prmonsters.Values)
                        spawnPacket.Objects.Add(rm.Info);

                    foreach (PatrolDMonster dm in _Pdmonsters.Values)
                        spawnPacket.Objects.Add(dm.Info);

                    foreach (NavCMonster ncm in _Ncmonsters.Values)
                        spawnPacket.Objects.Add(ncm.Info);

                    foreach (NavRMonster nrm in _Nrmonsters.Values)
                        spawnPacket.Objects.Add(nrm.Info);

                    foreach (NavDMonster ndm in _Ndmonsters.Values)
                        spawnPacket.Objects.Add(ndm.Info);

                    foreach (Buggy b in _buggy.Values)
                        spawnPacket.Objects.Add(b.Info);

                    foreach (Boss1 b1 in _boss1.Values)
                        spawnPacket.Objects.Add(b1.Info);

                    foreach (Boss2 b2 in _boss2.Values)
                        spawnPacket.Objects.Add(b2.Info);

                    foreach (Boss3 b3 in _boss3.Values)
                        spawnPacket.Objects.Add(b3.Info);

                    foreach (Button b in _button.Values)
                        spawnPacket.Objects.Add(b.Info);

                    foreach (Door d in _door.Values)
                        spawnPacket.Objects.Add(d.Info);

                    foreach (DoorManager dm in _doorManager.Values)
                        spawnPacket.Objects.Add(dm.Info);

                    foreach (ObsCore core in _core.Values)
                        spawnPacket.Objects.Add(core.Info);

                    player.Session.Send(spawnPacket);
                }
            }

            else if (type == GameObjectType.Monsterspawner)
            {
                MonsterSpawner Monsterspawn = gameObject as MonsterSpawner;
                _monsterspawner.Add(gameObject.Id, Monsterspawn);
                Monsterspawn.Room = this;
            }

            else if (type == GameObjectType.Cmonster)
            {
                PatrolCMonster CMonster = gameObject as PatrolCMonster;
                _Pcmonsters.Add(gameObject.Id, CMonster);
                CMonster.Room = this;
            }

            else if (type == GameObjectType.Rmonster)
            {
                PatrolRMonster RMonster = gameObject as PatrolRMonster;
                _Prmonsters.Add(gameObject.Id, RMonster);
                RMonster.Room = this;
            }

            else if (type == GameObjectType.Dmonster)
            {
                PatrolDMonster DMonster = gameObject as PatrolDMonster;
                _Pdmonsters.Add(gameObject.Id, DMonster);
                DMonster.Room = this;
            }

            else if (type == GameObjectType.Navcmonster)
            {
                NavCMonster NCMonster = gameObject as NavCMonster;
                _Ncmonsters.Add(gameObject.Id, NCMonster);
                NCMonster.Room = this;
            }

            else if (type == GameObjectType.Navrmonster)
            {
                NavRMonster NRMonster = gameObject as NavRMonster;
                _Nrmonsters.Add(gameObject.Id, NRMonster);
                NRMonster.Room = this;
            }

            else if (type == GameObjectType.Navdmonster)
            {
                NavDMonster NDMonster = gameObject as NavDMonster;
                _Ndmonsters.Add(gameObject.Id, NDMonster);
                NDMonster.Room = this;
            }

            else if (type == GameObjectType.Buggy)
            {
                Buggy buggy = gameObject as Buggy;
                _buggy.Add(gameObject.Id, buggy);
                buggy.Room = this;
            }

            else if (type == GameObjectType.Bossone)
            {
                Boss1 boss1 = gameObject as Boss1;
                _boss1.Add(gameObject.Id, boss1);
                boss1.Room = this;
            }

            else if (type == GameObjectType.Bosstwo)
            {
                Boss2 boss2 = gameObject as Boss2;
                _boss2.Add(gameObject.Id, boss2);
                boss2.Room = this;
            }

            else if (type == GameObjectType.Bossthree)
            {
                Boss3 boss3 = gameObject as Boss3;
                _boss3.Add(gameObject.Id, boss3);
                boss3.Room = this;
            }

            else if (type == GameObjectType.Button)
            {
                Button button = gameObject as Button;
                _button.Add(gameObject.Id, button);
                button.Room = this;
            }

            else if (type == GameObjectType.Door)
            {
                Door door = gameObject as Door;
                _door.Add(gameObject.Id, door);
                door.Room = this;
            }

            else if (type == GameObjectType.Doormanager)
            {
                DoorManager doorManager = gameObject as DoorManager;
                _doorManager.Add(gameObject.Id, doorManager);
                doorManager.Room = this;
            }

            else if (type == GameObjectType.Core)
            {
                ObsCore core = gameObject as ObsCore;
                _core.Add(gameObject.Id, core);
                core.Room = this;
            }

            // 타인한테 새로운 신입에 대한 정보 보내기
            {
                S_Spawn spawnPacket = new S_Spawn();
                spawnPacket.Objects.Add(gameObject.Info);
                foreach (Player p in _players.Values)
                {
                    if (p.Id != gameObject.Id)
                        p.Session.Send(spawnPacket);
                }
            }

        }

        public void LeaveGame(int objectId)
        {
            GameObjectType type = ObjectManager.GetObjectTypeById(objectId);


            if (type == GameObjectType.Player)
            {
                Player player = null;
                if (_players.Remove(objectId, out player) == false)
                    return;

                player.Room = null;

                // 본인한테 정보 전송
                {
                    S_LeaveGame leavePacket = new S_LeaveGame();
                    player.Session.Send(leavePacket);
                }
            }

            // 타인한테 정보 전송
            {
                S_Despawn despawnPacket = new S_Despawn();
                despawnPacket.ObjectIds.Add(objectId);
                foreach (Player p in _players.Values)
                {
                    if (p.Id != objectId)
                        p.Session.Send(despawnPacket);
                }
            }

        }

        public void HandleMove(Player player, C_Move movePacket)
        {
            if (player == null)
                return;

            // 일단 서버에서 좌표 이동
            PositionInfo movePosInofo = movePacket.PosInfo;
            ObjectInfo info = player.Info;

            // 실시간으로 플레이어 좌표를 CellPos에 받아와서 몬스터와의 거리를 Check할 예정
            // 이 값을 설정해주지 않으면 Player 스크립트의 CellPos 값은 유효하지 않음
            player.CellPos = new Vector3(movePacket.PosInfo.PosX, movePacket.PosInfo.PosY, movePacket.PosInfo.PosZ);

            // 다른 플레이어한테도 알려준다
            S_Move resMovePacket = new S_Move();
            resMovePacket.ObjectId = player.Info.ObjectId;
            resMovePacket.PosInfo = movePacket.PosInfo;

            Broadcast(resMovePacket);

        }

        public void HandleCar(Player player, C_Car carPacket)
        {
            if (player == null)
                return;


            Buggy buggy = new Buggy();

            if (_buggy.TryGetValue(carPacket.CarInfo.CarId, out buggy))
            {
                S_Car resCarPacket = new S_Car();
                buggy.carInfo.CarId = carPacket.CarInfo.CarId;
                buggy.carInfo.CarplayerId = carPacket.CarInfo.CarplayerId;
                buggy.carInfo.CarX = carPacket.CarInfo.CarX;
                buggy.carInfo.CarY = carPacket.CarInfo.CarY;
                buggy.carInfo.CarZ = carPacket.CarInfo.CarZ;
                buggy.carInfo.DirX = carPacket.CarInfo.DirX;
                buggy.carInfo.DirZ = carPacket.CarInfo.DirZ;
                buggy.carInfo.RotY = carPacket.CarInfo.RotY;
                buggy.carInfo.RotX = carPacket.CarInfo.RotX;
                buggy.carInfo.RotZ = carPacket.CarInfo.RotZ;

                buggy.carInfo.IsRide = carPacket.CarInfo.IsRide;

                resCarPacket.CarInfo = buggy.carInfo;

                //Console.WriteLine(resCarPacket);

                Broadcast(resCarPacket);
            }

        }

        public void HandleAttack(Player player, C_AttackState attackPacket)
        {
            if (player == null)
                return;


            AttackInfo attackInfo = attackPacket.AttackInfo;
            ObjectInfo info = player.Info;

            // 다른 플레이어한테도 알려준다
            S_AttackState resAttackPacket = new S_AttackState();
            resAttackPacket.ObjectId = player.Info.ObjectId;
            resAttackPacket.AttackInfo = attackPacket.AttackInfo;

            Broadcast(resAttackPacket);

        }

        public void HandleStat(Player player, C_ChangeStat changePacket)
        {
            if (player == null)
                return;


            PatrolCMonster cm = new PatrolCMonster();
            PatrolRMonster rm = new PatrolRMonster();
            PatrolDMonster dm = new PatrolDMonster();

            NavCMonster ncm = new NavCMonster();
            NavRMonster nrm = new NavRMonster();
            NavDMonster ndm = new NavDMonster();

            Boss1 boss1 = new Boss1();
            Boss2 boss2 = new Boss2();
            Boss3 boss3 = new Boss3();

            Door door = new Door();
            Player p = new Player();

            if (changePacket.ObjectId == 2)
            {
                boss2.setIsStart(true);
            }

            if (changePacket.ObjectId == 5097508)
            {

                S_ChangeStat resChangePacket = new S_ChangeStat();
                resChangePacket.ObjectId = changePacket.ObjectId;
                resChangePacket.StatInfo = changePacket.StatInfo;
                Broadcast(resChangePacket);

            }

            //if (changePacket.ObjectId == 4097508)
            //{
            //    m_boss1_SpawnCount++;

            //    if (m_boss1_SpawnCount < 2)
            //    {
            //        Random rand = new Random();
            //        int rand_type = rand.Next(1, 3);

            //        if (rand_type == 1)
            //        {
            //            NavCMonster n_cmonster1 = ObjectManager.Instance.Add<NavCMonster>();
            //            n_cmonster1.CellPos = new Vector3(1407f, 225.979f, 4914.01f);
            //            n_cmonster1.HP = 150;
            //            n_cmonster1.Info.Name = $"Nav Closed Monster";
            //            //EnterGame(n_cmonster1);
            //            Push(EnterGame, n_cmonster1);
            //        }

            //        else if (rand_type == 2)
            //        {
            //            NavRMonster n_rmonster1 = ObjectManager.Instance.Add<NavRMonster>();
            //            n_rmonster1.CellPos = new Vector3(1410f, 225.979f, 4904f);
            //            n_rmonster1.HP = 150;
            //            n_rmonster1.Info.Name = $"Nav Ranged Monster";
            //            //EnterGame(n_rmonster1);
            //            Push(EnterGame, n_rmonster1);
            //        }

            //        m_boss1_SpawnCount = 0;
            //        changePacket.ObjectId = 11111111;
            //    }
            //}

            //if (changePacket.ObjectId == 4045289)
            //{
            //    Console.WriteLine("몇번 들어오냐");

            //    m_boss2_SpawnCount++;

            //    if (m_boss2_SpawnCount < 2)
            //    {
            //        Random rand = new Random();
            //        int rand_x = rand.Next(1, 15);
            //        int rand_y = rand.Next(1, 15);
            //        int rand_z = rand.Next(1, 15);
            //        int rand_type = rand.Next(1, 3);

            //        Console.WriteLine("rand_type : " + rand_type);

            //        if (rand_type == 1)
            //        {
            //            NavDMonster n_dmonster1 = ObjectManager.Instance.Add<NavDMonster>();
            //            n_dmonster1.HP = 150;
            //            n_dmonster1.CellPos = new Vector3(1495f + rand_x, 330f + rand_y, 2293f + rand_z);
            //            n_dmonster1.Info.Name = $"Nav Drone Monster";
            //            //EnterGame(n_dmonster1);
            //            Push(EnterGame, n_dmonster1);
            //        }

            //        else if (rand_type == 2)
            //        {
            //            PatrolDMonster p_dmonster1 = ObjectManager.Instance.Add<PatrolDMonster>();
            //            p_dmonster1.HP = 150;
            //            p_dmonster1.CellPos = new Vector3(1495f + rand_z, 330f + rand_x, 2293f + rand_x);
            //            p_dmonster1.Info.Name = $"Patrol Drone Monster";
            //            //EnterGame(p_dmonster1);
            //            Push(EnterGame, p_dmonster1);
            //        }

            //        m_boss2_SpawnCount = 0;
            //        changePacket.ObjectId = 11111112;
            //    }
            //}

            if (_Pcmonsters.TryGetValue(changePacket.ObjectId, out cm))
            {
                S_ChangeStat resChangePacket = new S_ChangeStat();

                cm.StatInfo.Hp -= changePacket.StatInfo.Damage;
                cm.SetHitted(true);

                resChangePacket.StatInfo = cm.StatInfo;
                resChangePacket.ObjectId = changePacket.ObjectId;
                Broadcast(resChangePacket);
            }

            if (_Prmonsters.TryGetValue(changePacket.ObjectId, out rm))
            {
                S_ChangeStat resChangePacket = new S_ChangeStat();

                rm.StatInfo.Hp -= changePacket.StatInfo.Damage;
                rm.SetHitted(true);

                resChangePacket.StatInfo = rm.StatInfo;
                resChangePacket.ObjectId = changePacket.ObjectId;
                Broadcast(resChangePacket);
            }

            if (_Pdmonsters.TryGetValue(changePacket.ObjectId, out dm))
            {
                S_ChangeStat resChangePacket = new S_ChangeStat();

                dm.StatInfo.Hp -= changePacket.StatInfo.Damage;
                dm.SetHitted(true);

                resChangePacket.StatInfo = dm.StatInfo;
                resChangePacket.ObjectId = changePacket.ObjectId;
                Broadcast(resChangePacket);
            }

            if (_Ncmonsters.TryGetValue(changePacket.ObjectId, out ncm))
            {
                S_ChangeStat resChangePacket = new S_ChangeStat();

                ncm.StatInfo.Hp -= changePacket.StatInfo.Damage;
                ncm.SetHitted(true);

                resChangePacket.StatInfo = ncm.StatInfo;
                resChangePacket.ObjectId = changePacket.ObjectId;
                Broadcast(resChangePacket);
            }

            if (_Nrmonsters.TryGetValue(changePacket.ObjectId, out nrm))
            {
                S_ChangeStat resChangePacket = new S_ChangeStat();

                nrm.StatInfo.Hp -= changePacket.StatInfo.Damage;
                nrm.SetHitted(true);

                resChangePacket.StatInfo = nrm.StatInfo;
                resChangePacket.ObjectId = changePacket.ObjectId;
                Broadcast(resChangePacket);
            }

            if (_Ndmonsters.TryGetValue(changePacket.ObjectId, out ndm))
            {
                S_ChangeStat resChangePacket = new S_ChangeStat();

                ndm.StatInfo.Hp -= changePacket.StatInfo.Damage;
                ndm.SetHitted(true);

                resChangePacket.StatInfo = ndm.StatInfo;
                resChangePacket.ObjectId = changePacket.ObjectId;
                Broadcast(resChangePacket);
            }

            if (_boss1.TryGetValue(changePacket.ObjectId, out boss1))
            {
                S_ChangeStat resChangePacket = new S_ChangeStat();

                boss1.StatInfo.Hp -= changePacket.StatInfo.Damage;

                resChangePacket.StatInfo = boss1.StatInfo;
                resChangePacket.ObjectId = changePacket.ObjectId;
                Broadcast(resChangePacket);
            }

            if (_boss2.TryGetValue(changePacket.ObjectId, out boss2))
            {
                if (changePacket.StatInfo.Attack == 2)
                {
                    boss2.setIsStart(true);
                }

                {
                    S_ChangeStat resChangePacket = new S_ChangeStat();

                    boss2.StatInfo.Hp -= changePacket.StatInfo.Damage;

                    resChangePacket.StatInfo = boss2.StatInfo;
                    resChangePacket.ObjectId = changePacket.ObjectId;
                    Broadcast(resChangePacket);
                }
            }

            if (_boss3.TryGetValue(changePacket.ObjectId, out boss3))
            {

                S_ChangeStat resChangePacket = new S_ChangeStat();

                boss3.StatInfo.Hp -= changePacket.StatInfo.Damage;

                resChangePacket.StatInfo = boss3.StatInfo;
                resChangePacket.ObjectId = changePacket.ObjectId;
                Broadcast(resChangePacket);
            }


            if (_players.TryGetValue(changePacket.ObjectId, out p))
            {
                S_ChangeStat resChangePacket = new S_ChangeStat();

                p.StatInfo.Hp -= changePacket.StatInfo.Damage;

                if (p.StatInfo.Hp <= 0)
                {
                    p.StatInfo.Hp = 0;
                    p.StatInfo.Attack = 1000;   // 죽었을 때 보낸다
                }

                resChangePacket.StatInfo = p.StatInfo;
                resChangePacket.ObjectId = changePacket.ObjectId;
                Broadcast(resChangePacket);
            }

            if (_door.TryGetValue(changePacket.ObjectId, out door))
            {
                S_ChangeStat resChangePacket = new S_ChangeStat();

                resChangePacket.ObjectId = changePacket.ObjectId;
                resChangePacket.StatInfo = changePacket.StatInfo;

                Broadcast(resChangePacket);
            }

        }

        public void HandleButton(Player player, C_Button buttonPacket)
        {
            if (player == null)
                return;


            S_Button resButtonPacket = new S_Button();

            //resButtonPacket.ObjectId = player.Info.ObjectId;
            resButtonPacket.ObjectId = buttonPacket.ObjectId;
            resButtonPacket.ButtonInfo = buttonPacket.ButtonInfo;

            Broadcast(resButtonPacket);
        }

        public Player FindPlayer(Func<GameObject, bool> condition)
        {
            foreach (Player player in _players.Values)
            {
                if (condition.Invoke(player))
                    return player;
            }

            return null;
        }

        public PatrolCMonster FindCMonster(Func<GameObject, bool> condition)
        {
            foreach (PatrolCMonster cmonster in _Pcmonsters.Values)
            {
                if (condition.Invoke(cmonster))
                    return cmonster;
            }
            return null;
        }

        public void Broadcast(IMessage packet)
        {

            foreach (Player p in _players.Values)
            {
                p.Session.Send(packet);
            }

        }

        public void InitDMonsPos()
        {
            DMonster_pos.Add(new Vector3(1470.88f, 205f, 2293.33f));
            DMonster_pos.Add(new Vector3(1470.88f, 205f, 2303.17f));
            DMonster_pos.Add(new Vector3(1475.71f, 205f, 2298.882f));

            DMonster_pos.Add(new Vector3(1470.88f, 250f, 2293.33f));
            DMonster_pos.Add(new Vector3(1470.88f, 250f, 2303.17f));
            DMonster_pos.Add(new Vector3(1475.71f, 250f, 2298.882f));

            DMonster_pos.Add(new Vector3(1470.88f, 295f, 2293.33f));
            DMonster_pos.Add(new Vector3(1470.88f, 295f, 2303.17f));
            DMonster_pos.Add(new Vector3(1475.71f, 295f, 2298.882f));
        }

        public void InitCMosPos()
        {
            // 던전1 근접 몬스터 
            CMonster_pos.Add(new Vector3(1367.629f, 225.8263f, 4916.181f));
            CMonster_pos.Add(new Vector3(1375.874f, 225.9664f, 4936.51f));
            CMonster_pos.Add(new Vector3(1378.797f, 225.7046f, 4947.362f));
            CMonster_pos.Add(new Vector3(1379.603f, 225.955f, 4927.907f));
            CMonster_pos.Add(new Vector3(1373.603f, 225.955f, 4927.907f));
            CMonster_pos.Add(new Vector3(1381.721f, 226.0201f, 4927.731f));
            CMonster_pos.Add(new Vector3(1383.534f, 227.9f, 4922.81f));

            // 던전2 1층 근접 몬스터
            CMonster_pos.Add(new Vector3(1461.43f, 195.7737f, 2285.71f));
            CMonster_pos.Add(new Vector3(1472.03f, 195.7737f, 2285.71f));
            CMonster_pos.Add(new Vector3(1461.43f, 195.7737f, 2298.14f));
            CMonster_pos.Add(new Vector3(1471.87f, 195.7737f, 2311.093f));
            CMonster_pos.Add(new Vector3(1482.165f, 195.7737f, 2311.093f));


            // 던전2 계단

            CMonster_pos.Add(new Vector3(1442.31f, 199.1f, 2303.29f));
            CMonster_pos.Add(new Vector3(1452.38f, 201.01f, 2323.14f));
            CMonster_pos.Add(new Vector3(1472.78f, 203.78f, 2331.06f));
            CMonster_pos.Add(new Vector3(1443.14f, 216.239f, 2290.97f));
            CMonster_pos.Add(new Vector3(1484.29f, 222.95f, 2330.26f));
            CMonster_pos.Add(new Vector3(1504.22f, 228.02f, 2285.38f));
            CMonster_pos.Add(new Vector3(1470.18f, 231.85f, 2267.5f));
            CMonster_pos.Add(new Vector3(1471.87f, 240.574f, 2311.093f));
            CMonster_pos.Add(new Vector3(1471.87f, 285.18f, 2311.093f));
        }

        public void InitRMosPos()
        {
            // 던전1 원거리 몬스터 
            RMonster_pos.Add(new Vector3(1351.418f, 225.7309f, 4943.882f));
            RMonster_pos.Add(new Vector3(1351.052f, 225.8932f, 4927.855f));
            RMonster_pos.Add(new Vector3(1352.508f, 225.8775f, 4936.074f));
            RMonster_pos.Add(new Vector3(1354.841f, 225.7495f, 4940.246f));
            RMonster_pos.Add(new Vector3(1351.342f, 225.7424f, 4932.287f));
            RMonster_pos.Add(new Vector3(1355.91f, 227.43f, 4943.56f));

            // 던전2 1층 원거리 몬스터
            RMonster_pos.Add(new Vector3(1454.308f, 195.7737f, 2285.197f));
            RMonster_pos.Add(new Vector3(1491.523f, 195.7737f, 2307.989f));
            RMonster_pos.Add(new Vector3(1466.89f, 195.7737f, 2316.742f));
            RMonster_pos.Add(new Vector3(1454.381f, 195.7737f, 2294.554f));
            RMonster_pos.Add(new Vector3(1454.425f, 195.7737f, 2304.129f));            
        }

        public void InitBuggyPos()
        {
            Buggy_pos.Add(new Vector3(2240.4f, 110.8f, 3450.24f));
            Buggy_pos.Add(new Vector3(2230.4f, 110.8f, 3460.24f));
            Buggy_pos.Add(new Vector3(2240.4f, 110.8f, 3470.24f));
            Buggy_pos.Add(new Vector3(2230.4f, 110.8f, 3480.24f));

            //던전 1 버기
            Buggy_pos.Add(new Vector3(1442, 261, 4946));
            Buggy_pos.Add(new Vector3(1445, 261, 4946));
            Buggy_pos.Add(new Vector3(1446, 261, 4941));
            Buggy_pos.Add(new Vector3(1442, 261, 4941));
            //던전 2 탈출구 버기
            Buggy_pos.Add(new Vector3(1478, 424, 2288));
            Buggy_pos.Add(new Vector3(1482, 424, 2288));
            Buggy_pos.Add(new Vector3(1486, 424, 2288));
            Buggy_pos.Add(new Vector3(1490, 424, 2288));

            //강 앞 버기

            Buggy_pos.Add(new Vector3(2615, 89, 1066));
            Buggy_pos.Add(new Vector3(2619, 89, 1066));
            Buggy_pos.Add(new Vector3(2623, 89, 1066));
            Buggy_pos.Add(new Vector3(2627, 89, 1066));


            //던전 3 언덕 버기
            Buggy_pos.Add(new Vector3(5420, 71, 4531));
            Buggy_pos.Add(new Vector3(5424, 71, 4531));
            Buggy_pos.Add(new Vector3(5428, 71, 4531));
            Buggy_pos.Add(new Vector3(5432, 71, 4531));


        }

        // 보스3 Wall Position 
        public void InitWallPos()
        {
            // 던전3 첫번째 벽
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4933.008f));
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4943.736f));
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4954.473f));
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4965.126f));
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4975.891f));

            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4933.008f));
            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4943.736f));
            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4954.473f));
            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4965.126f));
            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4975.891f));

            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4933.008f));
            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4943.736f));
            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4954.473f));
            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4965.126f));
            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4975.891f));

            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4933.008f));
            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4943.736f));
            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4954.473f));
            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4965.126f));
            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4975.891f));

            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4933.008f));
            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4943.736f));
            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4954.473f));
            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4965.126f));
            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4975.891f));

            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4933.008f));
            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4943.736f));
            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4954.473f));
            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4965.126f));
            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4975.891f));

            Wall_pos.Add(new Vector3(5422.15f, 53.63651f, 4933.008f));
            Wall_pos.Add(new Vector3(5422.15f, 53.63651f, 4943.736f));
            Wall_pos.Add(new Vector3(5422.15f, 53.63651f, 4954.473f));
            Wall_pos.Add(new Vector3(5422.15f, 53.63651f, 4965.126f));
            Wall_pos.Add(new Vector3(5422.15f, 53.63651f, 4975.891f));

            // 던전3 두번째 벽
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4804.744f));
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4816.354f));
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4827.974f));
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4839.504f));
            Wall_pos.Add(new Vector3(5311.25f, 53.63651f, 4851.154f));

            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4804.744f));
            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4816.354f));
            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4827.974f));
            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4839.504f));
            Wall_pos.Add(new Vector3(5327.95f, 53.63651f, 4851.154f));

            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4804.744f));
            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4816.354f));
            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4827.974f));
            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4839.504f));
            Wall_pos.Add(new Vector3(5345.85f, 53.63651f, 4851.154f));

            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4804.744f));
            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4816.354f));
            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4827.974f));
            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4839.504f));
            Wall_pos.Add(new Vector3(5364.25f, 53.63651f, 4851.154f));

            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4804.744f));
            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4816.354f));
            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4827.974f));
            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4839.504f));
            Wall_pos.Add(new Vector3(5383.15f, 53.63651f, 4851.154f));

            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4804.744f));
            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4816.354f));
            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4827.974f));
            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4839.504f));
            Wall_pos.Add(new Vector3(5401.25f, 53.63651f, 4851.154f));

            Wall_pos.Add(new Vector3(5422.15f, 53.63651f, 4804.744f));
            Wall_pos.Add(new Vector3(5422.25f, 53.63651f, 4816.354f));
            Wall_pos.Add(new Vector3(5422.25f, 53.63651f, 4827.974f));
            Wall_pos.Add(new Vector3(5422.25f, 53.63651f, 4839.504f));
            Wall_pos.Add(new Vector3(5422.25f, 53.63651f, 4851.154f));
        }
    }
}
