﻿using System.Collections;
using System.Collections.Generic;
using Unity.IO.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.UIElements;

public class NoneControlObj : AnimationObj
{
    
    public Vector3 destPos; //도착지의 좌표 -CellPos? 내가 조작하는 플레이어는 이게 필요가 없어서 처리를 생각해보는것도 좋을듯

    public Vector3 moveDir;

    public Vector3 rot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Move()
    {
        //Debug.Log("destPos" + destPos);

        bool ischanged = true;

        if (destPos == CellPos)
        {
            ischanged = false;
        }

        if (ischanged)
        {
            // if 내가 무언가 움직이는 작업을 하고 있다면 OtherPlayer가 움직이도록 한다.
            destPos = CellPos;

            moveDir = destPos - transform.position;

            Vector3 moveNor = moveDir.normalized;
            float dist = moveDir.magnitude;

            if (dist < (speed * Time.deltaTime))
            {
                transform.position = destPos;
            }

            else
            {
                transform.position += (moveNor * speed * Time.deltaTime);
            }
        }

        // destPos == CellPos 일때, 즉 아무것도 누르지 않았지만 현재 나의 transformPosition과 CellPos가 다르다면 
        // 일정 거리 기준 이상으로 차이가 심하면 안전장치 느낌으로 기존 위치로 텔레포트 시켜준다.

        else if (!ischanged)
        {
            if ((CellPos - transform.position).magnitude >= 15)
            {
                //Debug.Log((CellPos - transform.position).magnitude);
                transform.position = CellPos;
            }
            else
            {
                //Debug.Log((CellPos - transform.position).magnitude);
                transform.position = Vector3.MoveTowards(transform.position, CellPos, Time.deltaTime * speed);
            }
        }
    }   
}
