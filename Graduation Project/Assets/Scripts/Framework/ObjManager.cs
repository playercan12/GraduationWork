﻿using Google.Protobuf.Protocol;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjManager : MonoBehaviour
{
    protected bool _updated = false;
    protected bool _updatedAttack = false;
    protected bool _updatedCar = false;
    protected bool _updatedButton = false;

    PositionInfo _positionInfo = new PositionInfo();
    StatInfo _statInfo = new StatInfo();
    AttackInfo _attackInfo = new AttackInfo();
    CarInfo _carInfo = new CarInfo();
    ButtonInfo _buttonInfo = new ButtonInfo();

    public int id;
    public int btn_id;

    public float speed = 0f;

    public float dirX;
    public float dirZ;

    public int MaxHP;
    public float HP;
    public int armor;

    public ButtonInfo buttonInfo
    {
        get
        {
            return _buttonInfo;
        }
        set
        {
            ButtonId = value.ButtonId;
            IsPushButton = value.IsPushButton;
        }
    }

    public int ButtonId
    {
        get
        {
            return _buttonInfo.ButtonId;
        }

        set
        {
            _buttonInfo.ButtonId = value;
        }
    }


    public bool IsPushButton
    {
        get
        {
            return buttonInfo.IsPushButton;
        }
        set
        {
            buttonInfo.IsPushButton = value;
        }
    }


    public CarInfo carInfo
    {
        get { return _carInfo; }
        set
        {
            if (_carInfo.Equals(value))
                return;

            carId_test = value.CarId;
            carplayerId_test = value.CarplayerId;
            CarPos = new Vector3(value.CarX, value.CarY, value.CarZ);
            carDirX = value.DirX;
            carDirZ = value.DirZ;
            carRotX = value.RotX;
            carRotY = value.RotY;
            carRotZ = value.RotZ;
            IsRide = value.IsRide;
        }
    }

    public int carId_test
    {
        get { return carInfo.CarId; }

        set
        {
            carInfo.CarId = value;
            _updatedCar = true;
        }
    }

    public int carplayerId_test
    {
        get { return carInfo.CarplayerId; }

        set
        {
            carInfo.CarplayerId = value;
            _updatedCar = true;
        }
    }

    public Vector3 CarPos
    {
        get
        {
            return new Vector3(carInfo.CarX, carInfo.CarY, carInfo.CarZ);
        }

        set
        {
            if (carInfo.CarX == value.x && carInfo.CarY == value.y && carInfo.CarX == value.z)
                return;

            carInfo.CarX = value.x;
            carInfo.CarY = value.y;
            carInfo.CarZ = value.z;

            _updatedCar = true;
        }
    }

    public float carRotX
    {
        get
        {
            return carInfo.RotX;
        }
        set
        {
            if (carInfo.RotX == value)
                return;

            carInfo.RotX = value;
            _updatedCar = true;
        }
    }

    public float carRotY
    {
        get
        {
            return carInfo.RotY;
        }
        set
        {
            if (carInfo.RotY == value)
                return;

            carInfo.RotY = value;
            _updatedCar = true;
        }
    }

    public float carRotZ
    {
        get
        {
            return carInfo.RotZ;
        }
        set
        {
            if (carInfo.RotZ == value)
                return;

            carInfo.RotZ = value;
            _updatedCar = true;
        }
    }

    public float carDirX
    {
        get
        {
            return carInfo.DirX;
        }
        set
        {
            if (carInfo.DirX == value)
                return;

            carInfo.DirX = value;
            _updatedCar = true;
        }
    }

    public float carDirZ
    {
        get
        {
            return carInfo.DirZ;
        }
        set
        {
            if (carInfo.DirZ == value)
                return;

            carInfo.DirZ = value;
            _updatedCar = true;
        }
    }

    public bool IsRide
    {
        get
        {
            return carInfo.IsRide;
        }
        set
        {
            carInfo.IsRide = value;
            _updatedCar = true;
        }
    }

    public AttackInfo AttackInfo
    {
        get { return _attackInfo; }
        set
        {
            if (_attackInfo.Equals(value))
                return;

            IsShoot = value.IsShoot;
            IsEquiq = value.IsEquiq;
            IsHit = value.IsHit;
            IsReload = value.IsReload;
            IsDead = value.IsDead;
            s_GunType = value.GunType;
        }
    }

    public bool IsReload
    {
        get { return AttackInfo.IsReload; }
        set
        {
            AttackInfo.IsReload = value;
            _updatedAttack = true;
        }
    }

    public bool IsShoot
    {
        get { return AttackInfo.IsShoot; }
        set
        {
            AttackInfo.IsShoot = value;
            _updatedAttack = true;
        }
    }

    public bool IsEquiq
    {
        get { return AttackInfo.IsEquiq; }

        set
        {
            AttackInfo.IsEquiq = value;
            _updatedAttack = true;
        }
    }

    public bool IsHit
    {
        get { return AttackInfo.IsHit; }

        set
        {
            AttackInfo.IsHit = value;
            _updatedAttack = true;
        }
    }

    public bool IsDead
    {
        get { return AttackInfo.IsDead; }

        set
        {
            AttackInfo.IsDead = value;
            _updatedAttack = true;
        }
    }

    public int s_GunType
    {
        get
        {
            return AttackInfo.GunType;
        }
        set
        {
            AttackInfo.GunType = value;
            _updatedAttack = true;
        }
    }


    public PositionInfo PosInfo
    {
        get { return _positionInfo; }
        set
        {
            if (_positionInfo.Equals(value))
                return;

            CellPos = new Vector3(value.PosX, value.PosY, value.PosZ);
            MoveDirX = value.DirX;
            MoveDirZ = value.DirZ;
            Rotate = value.RotY;
            SpinePos = new Vector3(value.SpineX, value.SpineY, value.SpineZ);
            IsJump = value.IsJump;
            carId = value.CarId;
            usingcarId = value.CaruserId;
            s_State = value.State;
            IsClimb = value.IsClimb;
            IsClimbLadder = value.IsClimbLadder;
            Pattern = value.Pattern;
            RandWall = value.Randwall;

            _updated = true;
        }
    }

    public void SyncPos()
    {
        Vector3 destPos = CellPos;
        transform.position = CellPos;
    }

    public Vector3 CellPos
    {
        get
        {
            return new Vector3(PosInfo.PosX, PosInfo.PosY, PosInfo.PosZ);
        }

        set
        {
            if (PosInfo.PosX == value.x && PosInfo.PosY == value.y && PosInfo.PosZ == value.z)
                return;

            PosInfo.PosX = value.x;
            PosInfo.PosY = value.y;
            PosInfo.PosZ = value.z;

            _updated = true;
        }
    }

    public float MoveDirX
    {
        get
        {
            return PosInfo.DirX;
        }
        set
        {
            if (PosInfo.DirX == value)
                return;

            PosInfo.DirX = value;
            _updated = true;
        }
    }

    public float MoveDirZ
    {
        get
        {
            return PosInfo.DirZ;
        }
        set
        {
            if (PosInfo.DirZ == value)
                return;

            PosInfo.DirZ = value;
            _updated = true;
        }
    }

    public float Rotate
    {
        get
        {
            return PosInfo.RotY;
        }
        set
        {
            if (PosInfo.RotY == value)
                return;

            PosInfo.RotY = value;
            _updated = true;
        }
    }

    public Vector3 SpinePos
    {
        get
        {
            return new Vector3(PosInfo.SpineX, PosInfo.SpineY, PosInfo.SpineZ);
        }

        set
        {
            if (PosInfo.SpineX == value.x && PosInfo.SpineY == value.y && PosInfo.SpineZ == value.z)
                return;

            PosInfo.SpineX = value.x;
            PosInfo.SpineY = value.y;
            PosInfo.SpineZ = value.z;

            _updated = true;
        }
    }

    public State s_State
    {
        get { return PosInfo.State; }
        set
        {
            PosInfo.State = value;
            _updated = true;
        }
    }

    public bool IsJump
    {
        get
        {
            return PosInfo.IsJump;
        }
        set
        {
            PosInfo.IsJump = value;
            _updated = true;
        }
    }

    public int carId
    {
        get
        {
            return PosInfo.CarId;
        }
        set
        {
            PosInfo.CarId = value;
            _updated = true;
        }
    }

    public int usingcarId
    {
        get
        {
            return PosInfo.CaruserId;
        }
        set
        {
            PosInfo.CaruserId = value;
            _updated = true;
        }
    }

    public bool IsClimb
    {
        get
        {
            return PosInfo.IsClimb;
        }
        set
        {
            PosInfo.IsClimb = value;
            _updated = true;
        }
    }

    public bool IsClimbLadder
    {
        get
        {
            return PosInfo.IsClimbLadder;
        }
        set
        {
            PosInfo.IsClimbLadder = value;
            _updated = true;
        }
    }

    public Pattern Pattern
    {
        get
        {
            return PosInfo.Pattern;
        }
        set
        {
            PosInfo.Pattern = value;
        }
    }

    public int RandWall
    {
        get
        {
            return PosInfo.Randwall;
        }
        set
        {
            PosInfo.Randwall = value;
        }
    }

    public StatInfo StatInfo
    {
        get { return _statInfo; }
        set
        {
            Level = value.Level;
            s_HP = value.Hp;
            s_MaxHP = value.MaxHp;
            Attack = value.Attack;
            SSpeed = value.Speed;
            s_Exp = value.TotalExp;
            s_Damage = value.Damage;
        }
    }

    public int s_Damage
    {
        get
        {
            return StatInfo.Damage;
        }
        set
        {
            StatInfo.Damage = value;
        }
    }

    public int Level
    {
        get
        {
            return StatInfo.Level;
        }
        set
        {
            StatInfo.Level = value;
        }
    }

    public int s_HP
    {
        get
        {
            return StatInfo.Hp;
        }
        set
        {
            StatInfo.Hp = value;
        }
    }

    public int s_MaxHP
    {
        get
        {
            return StatInfo.MaxHp;
        }
        set
        {
            StatInfo.MaxHp = value;
        }
    }

    public int Attack
    {
        get
        {
            return StatInfo.Attack;
        }
        set
        {
            StatInfo.Attack = value;
        }
    }

    public float SSpeed
    {
        get
        {
            return StatInfo.Speed;
        }
        set
        {
            StatInfo.Speed = value;
        }
    }

    public int s_Exp
    {
        get
        {
            return StatInfo.TotalExp;
        }
        set
        {
            StatInfo.TotalExp = value;
        }
    }

    protected virtual void Move() 
    {
      
    }

    protected virtual void UpdateAnim()
    {
      
    }
   
    //피격 함수
    public virtual void hit(float _damage, float penetration)
    {
        if (armor - penetration >= 70)
        {
            _damage = _damage * 0.1f;
        }
        else if (armor - penetration >= 50)
        {

            _damage = _damage * 0.3f;
        }
        else if (armor - penetration >= 30)
        {
            _damage = _damage * 0.5f;
        }
        else if (armor - penetration >= 10)
        {
            _damage = _damage * 0.7f;
        }
        else
        {
            _damage = _damage * 1f;
        }

        HP -= _damage;
        s_HP -= (int)_damage; // Server
    }

    protected virtual void Dead()
    {
      
    }
}
