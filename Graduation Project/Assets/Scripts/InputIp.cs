﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Google.Protobuf.Protocol;

public class InputIp : MonoBehaviour
{
    private static InputIp instance = null;

    public static InputIp Instance
    {
        get
        {
            if (instance == null)
            {
                return null;
            }

            return instance;
        }
    }

    public Button lobbyButton;

    public InputField ipInput;
    public InputField idInput;

    public String ip; // 처음에 입력하는 ip
    public String id; // 처음에 입력하는 id

    public TextMeshProUGUI[] playerIds;
    public string id0; // 아이디 동기화
    public string id1;
    public string id2;
    public string id3;

    public bool IsEnter = false; //버튼 눌렷는지 동기화 
    public float timer;

    public int isStart = 0;

    public int startInGame;

    public GameObject GameStart;

    //받아와야할 것 string 배열, 스타트 버튼 눌렷지 bool값, 인원수  
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
            instance = this;

        for (int i = 0; i < 4; i++)
        {
            playerIds[i].text = " ";
        }

        
        startInGame = 0;
        GameObject.Find("GameStart").GetComponent<Button>().interactable = false;   
        lobbyButton.interactable = false;
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (idInput.text != "" && ipInput.text != "")
        {
            lobbyButton.interactable = true;
        }
        else
        {
            lobbyButton.interactable = false;

        }
        
        if (IsEnter && timer >= 0.1f)
        {
            onEnterReRobbyBtnClick();
            timer = 0.0f;
        }

        if (isStart == 1)
        {
            C_EnterGame enterGamePacket = new C_EnterGame();
            enterGamePacket.IsStarted = true;
            enterGamePacket.MyId = id;
            Manager.Network.Send(enterGamePacket);
            SceneManager.LoadScene(1);
        }
    }

    public void onEnterBtnClick() //아이디 입력하고 서버로 보내는 함수
    {
        ip = ipInput.text;
        id = idInput.text;

        IsEnter = true;

        GameObject.Find("EnterLobby").GetComponent<Button>().interactable = false;
        GameObject.Find("GameStart").GetComponent<Button>().interactable = true;
    }

    public void onEnterReRobbyBtnClick()
    {
        C_EnterRobby enterRobbyPacket = new C_EnterRobby();
        enterRobbyPacket.EnterOK = startInGame;
        Manager.Network.Send(enterRobbyPacket);
    }

    public void onConnectBtnClick() //서버에 들어온 인원이 4명이면 게임을 시작할 수 있게 해주는 버튼
    {
        startInGame = 1;
    }
}