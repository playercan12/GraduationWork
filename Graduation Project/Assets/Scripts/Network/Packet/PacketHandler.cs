﻿using Google.Protobuf;
using Google.Protobuf.Protocol;
using ServerCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class PacketHandler
{

    public static void S_EnterGameHandler(PacketSession session, IMessage packet)
    {
        S_EnterGame enterGamePacket = packet as S_EnterGame;
        Manager.Object.Add(enterGamePacket.Player, myPlayer: true);
    }


    public static void S_LeaveGameHandler(PacketSession session, IMessage packet)
    {
        S_LeaveGame leaveGameHandler = packet as S_LeaveGame;
        Manager.Object.RemoveMyPlayer();
    }

    public static void S_SpawnHandler(PacketSession session, IMessage packet)
    {
        S_Spawn spawnPacket = packet as S_Spawn;
        foreach (ObjectInfo obj in spawnPacket.Objects)
        {
            Manager.Object.Add(obj, myPlayer: false);
        }
    }

    public static void S_DespawnHandler(PacketSession session, IMessage packet)
    {
        S_Despawn despawnPacket = packet as S_Despawn;
        foreach (int id in despawnPacket.ObjectIds)
        {
            Manager.Object.Remove(id);
        }
    }

    public static void S_MoveHandler(PacketSession session, IMessage packet)
    {
        S_Move movePacket = packet as S_Move;
        //ServerSession serverSession = session as ServerSession;

        GameObject go = Manager.Object.FindById(movePacket.ObjectId);
        if (go == null)
            return;

        ObjManager cc = go.GetComponent<ObjManager>();
        if (cc == null)
            return;

        cc.PosInfo = movePacket.PosInfo;
    }

    public static void S_AttackStateHandler(PacketSession session, IMessage packet)
    {
        S_AttackState attackPacket = packet as S_AttackState;

        GameObject go = Manager.Object.FindById(attackPacket.ObjectId);
        if (go == null)
            return;

        ObjManager cc = go.GetComponent<ObjManager>();
        if (cc == null)
            return;

        cc.AttackInfo = attackPacket.AttackInfo;
    }


    public static void S_ChangeStatHandler(PacketSession session, IMessage packet)
    {
        S_ChangeStat changePacket = packet as S_ChangeStat;

        if (changePacket.ObjectId == 5097508)
        {
            GameObject go1 = GameObject.FindWithTag("PlaneCore");

            if (go1 == null)
                return;

            ObjManager cc1 = go1.GetComponent<ObjManager>();
            if (cc1 == null)
                return;

            cc1.StatInfo = changePacket.StatInfo;
        }

        GameObject go = Manager.Object.FindById(changePacket.ObjectId);
        if (go == null)
            return;

        ObjManager cc = go.GetComponent<ObjManager>();
        if (cc == null)
            return;

        cc.id = changePacket.ObjectId;
        cc.StatInfo = changePacket.StatInfo;
    }

    public static void S_CarHandler(PacketSession session, IMessage packet)
    {
        S_Car carPacket = packet as S_Car;

        GameObject go = Manager.Object.FindById(carPacket.CarInfo.CarId);
        if (go == null)
            return;

        ObjManager cc = go.GetComponent<ObjManager>();
        if (cc == null)
            return;

        cc.carInfo = carPacket.CarInfo;
    }

    public static void S_ButtonHandler(PacketSession session, IMessage packet)
    {
        S_Button buttonPacket = packet as S_Button;

        GameObject go = Manager.Object.FindById(buttonPacket.ObjectId);
        if (go == null)
            return;

        ObjManager cc = go.GetComponent<ObjManager>();
        if (cc == null)
            return;

        cc.buttonInfo = buttonPacket.ButtonInfo;
    }

    public static void S_ConnectedHandler(PacketSession session, IMessage packet)
    {
        Debug.Log("S_Connected");

        // 싱글톤 ipinput의 ID를 가져오자
        InputIp ip = InputIp.Instance;

        C_Login loginPacket = new C_Login();
        loginPacket.UniqueId = ip.id;
        Manager.Network.Send(loginPacket);
    }

    public static void S_LoginHandler(PacketSession session, IMessage packet)
    {
        S_Login loginPacket = (S_Login)packet;
        Debug.Log($"Login ID ({loginPacket})");

        InputIp ip = InputIp.Instance;

        ip.isStart = loginPacket.LoginOK;
        ip.playerIds[0].text = loginPacket.One;
        ip.playerIds[1].text = loginPacket.Two;
        ip.playerIds[2].text = loginPacket.Three;
        ip.playerIds[3].text = loginPacket.Four;
    }
}

