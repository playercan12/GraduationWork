using Google.Protobuf.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager
{
	public Player MyPlayer { get; set; }
	Dictionary<int, GameObject> _objects = new Dictionary<int, GameObject>();
	
	private int door_count = 0;

	public static GameObjectType GetObjectTypeById(int id)
	{
		int type = (id >> 24) & 0x7F;
		return (GameObjectType)type;
	}

	public void Add(ObjectInfo info, bool myPlayer = false)
	{
		GameObjectType objectType = GetObjectTypeById(info.ObjectId);

		if (objectType == GameObjectType.Player)
		{
			if (myPlayer)
			{
				GameObject go = Manager.Resource.Instantiate("Player");
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				MyPlayer = go.GetComponent<Player>();
				MyPlayer.id = info.ObjectId;
				MyPlayer.PosInfo = info.PosInfo;
				MyPlayer.StatInfo = info.StatInfo;

				MyPlayer.SyncPos();
			}

			else
			{
				GameObject go = Manager.Resource.Instantiate("OtherPlayer");
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				OhterPlayer pc = go.GetComponent<OhterPlayer>();
				pc.id = info.ObjectId;
				pc.PosInfo = info.PosInfo;
				pc.StatInfo = info.StatInfo;

				pc.SyncPos();
			}

		}

		else if (objectType == GameObjectType.Button)
		{
			GameObject go = Manager.Resource.Instantiate("InteractManager", new Vector3(info.PosInfo.PosX, info.PosInfo.PosY, info.PosInfo.PosZ));
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			InteractManager interactManager = go.GetComponent<InteractManager>();
			interactManager.id = info.ObjectId;
			interactManager.buttonInfo = info.ButtonInfo;
		}

		else if (objectType == GameObjectType.Doormanager)
		{
			GameObject go = Manager.Resource.Instantiate("Dungeons/SliceManager");
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			SliceManager slicemanager = go.GetComponent<SliceManager>();
			slicemanager.id = info.ObjectId;
		}

		else if (objectType == GameObjectType.Bossone)
		{
			GameObject go = Manager.Resource.Instantiate("Monster/Boss1/Boss1");
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			BossMonster boss = go.GetComponent<BossMonster>();
			boss.id = info.ObjectId;
			boss.PosInfo = info.PosInfo;
			boss.StatInfo = info.StatInfo;
		}

		else if (objectType == GameObjectType.Bosstwo)
		{
			GameObject go = Manager.Resource.Instantiate("Monster/Boss2/Boss2");
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			BossMonster2 boss2 = go.GetComponent<BossMonster2>();
			boss2.id = info.ObjectId;
			boss2.PosInfo = info.PosInfo;
			boss2.StatInfo = info.StatInfo;
		}

		else if (objectType == GameObjectType.Bossthree)
		{
			GameObject go = Manager.Resource.Instantiate("Monster/Boss3/Boss3");
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			BossMonster3 boss3 = go.GetComponent<BossMonster3>();
			boss3.id = info.ObjectId;
			boss3.PosInfo = info.PosInfo;
			boss3.StatInfo = info.StatInfo;
		}

		else if (objectType == GameObjectType.Buggy)
		{
			GameObject go = Manager.Resource.Instantiate("Buggy/Buggy", new Vector3(info.PosInfo.PosX, info.PosInfo.PosY, info.PosInfo.PosZ));
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			CarController car = go.GetComponent<CarController>();
			car.id = info.ObjectId;
			car.carInfo = info.CarInfo;
		}

		else if (objectType == GameObjectType.Monsterspawner)
		{
			GameObject go = Manager.Resource.Instantiate("Monster/s_MonsterSpawner");
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			SMonsterSpawner spawner = go.GetComponent<SMonsterSpawner>();
			spawner.id = info.ObjectId;
		}

		else if (objectType == GameObjectType.Door)
		{
			door_count++;

			if (door_count >= 1 && door_count <= 35)
			{
				GameObject go = Manager.Resource.Instantiate("Dungeons/Dungeon3/FirstWall", new Vector3(info.PosInfo.PosX, info.PosInfo.PosY, info.PosInfo.PosZ), GameObject.Find("FirstWallAnim").transform);
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				Door door = go.GetComponent<Door>();
				door.id = info.ObjectId;
				door.StatInfo = info.StatInfo;
			}

			else if (door_count >= 36 && door_count <= 70)
			{
				GameObject go = Manager.Resource.Instantiate("Dungeons/Dungeon3/SecondWall", new Vector3(info.PosInfo.PosX, info.PosInfo.PosY, info.PosInfo.PosZ), GameObject.Find("SecondWallAnim").transform);
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				Door door = go.GetComponent<Door>();
				door.id = info.ObjectId;
				door.StatInfo = info.StatInfo;
			}


			else if (door_count == 71)
			{
				GameObject go = Manager.Resource.Instantiate("Dungeons/Dungeon1/Cave Hallway Door");
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				Door door = go.GetComponent<Door>();
				door.id = info.ObjectId;
				door.StatInfo = info.StatInfo;
			}

			else if (door_count == 72)
			{
				GameObject go = Manager.Resource.Instantiate("Dungeons/Dungeon1/Cave Hallway Door 1");
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				Door door = go.GetComponent<Door>();
				door.id = info.ObjectId;
				door.StatInfo = info.StatInfo;
			}

			else if (door_count == 73)
			{
				GameObject go = Manager.Resource.Instantiate("Dungeons/Dungeon1/Cave Hallway Door 2");
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				Door door = go.GetComponent<Door>();
				door.id = info.ObjectId;
				door.StatInfo = info.StatInfo;
			}

			else if (door_count == 74)
			{
				GameObject go = Manager.Resource.Instantiate("Dungeons/Dungeon1/Boss Cave Wood Door");
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				Door door = go.GetComponent<Door>();
				door.id = info.ObjectId;
				door.StatInfo = info.StatInfo;
			}

			

			else if (door_count == 75)
			{
				GameObject go = Manager.Resource.Instantiate("Dungeons/Dungeon2/TriggerFloor1");
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				Door door = go.GetComponent<Door>();
				door.id = info.ObjectId;
				door.StatInfo = info.StatInfo;
			}

			else if (door_count == 76)
			{
				GameObject go = Manager.Resource.Instantiate("Dungeons/Dungeon2/TriggerFloor2");
				go.name = info.Name;
				_objects.Add(info.ObjectId, go);

				Door door = go.GetComponent<Door>();
				door.id = info.ObjectId;
				door.StatInfo = info.StatInfo;
			}
		}

		else if (objectType == GameObjectType.Cmonster)
		{
            float yPos = 0;
            if (info.PosInfo.PosY > 0)            
                yPos = info.PosInfo.PosY;            
            else            
                yPos = CheckYpos(info.PosInfo.PosX, info.PosInfo.PosZ);

			GameObject go = Manager.Resource.Instantiate("Monster/ClosedMonster/PatrolClosedMonster", new Vector3(info.PosInfo.PosX, yPos, info.PosInfo.PosZ));
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			PatrolClosedMonster mc = go.GetComponent<PatrolClosedMonster>();
			mc.id = info.ObjectId;
			mc.PosInfo = info.PosInfo;
			mc.StatInfo = info.StatInfo;

			GameObject goParent = Manager.Resource.Instantiate("Monster/MonsterParent/MonsterParent");

			goParent.transform.position = go.transform.position;
			go.transform.parent = goParent.transform;
			go.SetActive(false);
		}

		else if (objectType == GameObjectType.Navcmonster)
		{
			float yPos = 0;
			if (info.PosInfo.PosY > 0)			
				yPos = info.PosInfo.PosY;			
			else			
				yPos = CheckYpos(info.PosInfo.PosX, info.PosInfo.PosZ);			

			GameObject go = Manager.Resource.Instantiate("Monster/ClosedMonster/ClosedMonster", new Vector3(info.PosInfo.PosX, yPos, info.PosInfo.PosZ));
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			ClosedMonster mc = go.GetComponent<ClosedMonster>();
			mc.id = info.ObjectId;
			mc.PosInfo = info.PosInfo;
			mc.StatInfo = info.StatInfo;

			GameObject goParent = Manager.Resource.Instantiate("Monster/MonsterParent/MonsterParent");

			goParent.transform.position = go.transform.position;
			go.transform.parent = goParent.transform;
			go.SetActive(false);
		}

		else if (objectType == GameObjectType.Rmonster)
		{
			float yPos = 0;
			if (info.PosInfo.PosY > 0)
				yPos = info.PosInfo.PosY;
			else
				yPos = CheckYpos(info.PosInfo.PosX, info.PosInfo.PosZ);

			GameObject go = Manager.Resource.Instantiate("Monster/RangedMonster/PatrolRangedMonster", new Vector3(info.PosInfo.PosX, yPos, info.PosInfo.PosZ));
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			PatrolRangedMonster mc = go.GetComponent<PatrolRangedMonster>();
			mc.id = info.ObjectId;
			mc.PosInfo = info.PosInfo;
			mc.StatInfo = info.StatInfo;

			GameObject goParent = Manager.Resource.Instantiate("Monster/MonsterParent/MonsterParent");

			goParent.transform.position = go.transform.position;
			go.transform.parent = goParent.transform;
			go.SetActive(false);
		}

		else if (objectType == GameObjectType.Navrmonster)
		{
			float yPos = 0;
			if (info.PosInfo.PosY > 0)
				yPos = info.PosInfo.PosY;
			else
				yPos = CheckYpos(info.PosInfo.PosX, info.PosInfo.PosZ);

			GameObject go = Manager.Resource.Instantiate("Monster/RangedMonster/RangedMonster", new Vector3(info.PosInfo.PosX, yPos, info.PosInfo.PosZ));
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			RangedMonster mc = go.GetComponent<RangedMonster>();
			mc.id = info.ObjectId;
			mc.PosInfo = info.PosInfo;
			mc.StatInfo = info.StatInfo;

			GameObject goParent = Manager.Resource.Instantiate("Monster/MonsterParent/MonsterParent");

			goParent.transform.position = go.transform.position;
			go.transform.parent = goParent.transform;
			go.SetActive(false);
		}

		else if (objectType == GameObjectType.Dmonster)
		{
			float yPos = 0;
			if (info.PosInfo.PosY > 0)
				yPos = info.PosInfo.PosY;
			else
				yPos = CheckYpos(info.PosInfo.PosX, info.PosInfo.PosZ) + 10.0f;

			GameObject go = Manager.Resource.Instantiate("Monster/DroneMonster/PatrolDroneMonster", new Vector3(info.PosInfo.PosX, yPos, info.PosInfo.PosZ));
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			PatrolDroneMonster mc = go.GetComponent<PatrolDroneMonster>();

			mc.id = info.ObjectId;
			mc.PosInfo = info.PosInfo;
			mc.StatInfo = info.StatInfo;

			mc.PosInfo.PosY = yPos;     // ����� y�� ����� �޾ƾ߸� ������ ����� ��

			GameObject goParent = Manager.Resource.Instantiate("Monster/MonsterParent/MonsterParent");

			goParent.transform.position = go.transform.position;
			go.transform.parent = goParent.transform;
			go.SetActive(false);
		}

		else if (objectType == GameObjectType.Navdmonster)
		{
			float yPos = 0;
			if (info.PosInfo.PosY > 0)
				yPos = info.PosInfo.PosY;
			else
				yPos = CheckYpos(info.PosInfo.PosX, info.PosInfo.PosZ) + 10.0f;

			GameObject go = Manager.Resource.Instantiate("Monster/DroneMonster/DroneMonster", new Vector3(info.PosInfo.PosX, yPos, info.PosInfo.PosZ));
			go.name = info.Name;
			_objects.Add(info.ObjectId, go);

			DroneMonster dm = go.GetComponent<DroneMonster>();
			dm.id = info.ObjectId;
			dm.PosInfo = info.PosInfo;
			dm.StatInfo = info.StatInfo;

			dm.PosInfo.PosY = yPos;

			GameObject goParent = Manager.Resource.Instantiate("Monster/MonsterParent/MonsterParent");

			goParent.transform.position = go.transform.position;
			go.transform.parent = goParent.transform;
			go.SetActive(false);
		}
	}

	public void Remove(int id)
	{
		GameObject go = FindById(id);
		if (go == null)
			return;

		_objects.Remove(id);
		Manager.Resource.Destroy(go);
	}

	public void RemoveMyPlayer()
	{
		if (MyPlayer == null)
			return;

		Remove(MyPlayer.id);
		MyPlayer = null;
	}

	public GameObject FindById(int id)
	{
		GameObject go = null;
		_objects.TryGetValue(id, out go);
		return go;
	}

    public GameObject Find(Vector3 cellPos)
    {
        foreach (GameObject obj in _objects.Values)
        {
            ObjManager cc = obj.GetComponent<ObjManager>();
            if (cc == null)
                continue;

            if (cc.CellPos == cellPos)
                return obj;
        }

        return null;
    }

    public GameObject Find(Func<GameObject, bool> condition)
	{
		foreach (GameObject obj in _objects.Values)
		{
			if (condition.Invoke(obj))
				return obj;
		}

		return null;
	}

	public void Clear()
	{
		foreach (GameObject obj in _objects.Values)
			Manager.Resource.Destroy(obj);
		_objects.Clear();
	}



    public float CheckYpos(float pos_x, float pos_z)
    {
        float terrainHeight;
		TerrainDataStore tds = TerrainDataStore.Instance;
		Terrain td = tds.terrains[0];


        if (pos_x < 1500 && pos_z < 1500) //terrain 0 0
        {
            td = tds.terrains[0];
        }
        if (pos_x < 1500 && (pos_z > 1500 && pos_z < 3000)) //terrain 0 1
        {
            td = tds.terrains[1];
        }
        if (pos_x < 1500 && (pos_z > 3000 && pos_z < 4500)) //terrain 0 2
        {
            td = tds.terrains[2];
        }
        if (pos_x < 1500 && pos_z > 4500) //terrain 0 3
        {
            td = tds.terrains[3];
        }
        if ((pos_x > 1500 && pos_x < 3000) && pos_z < 1500) //terrain 1 0
        {
            td = tds.terrains[4];
        }
        if ((pos_x > 1500 && pos_x < 3000) && (pos_z > 1500 && pos_z < 3000)) //terrain 1 1
        {
            td = tds.terrains[5];
        }
        if ((pos_x > 1500 && pos_x < 3000) && (pos_z > 3000 && pos_z < 4500)) //terrain 1 2
        {
            td = tds.terrains[6];
        }
        if ((pos_x > 1500 && pos_x < 3000) && pos_z > 4500) //terrain 1 3
        {
            td = tds.terrains[7];
        }
        if ((pos_x > 3000 && pos_x < 4500) && pos_z < 1500) //terrain 2 0
        {
            td = tds.terrains[8];
        }
        if ((pos_x > 3000 && pos_x < 4500) && (pos_z > 1500 && pos_z < 3000)) //terrain 2 1
        {
            td = tds.terrains[9];
        }
        if ((pos_x > 3000 && pos_x < 4500) && (pos_z > 3000 && pos_z < 4500)) //terrain 2 2
        {
            td = tds.terrains[10];
        }
        if ((pos_x > 3000 && pos_x < 4500) && pos_z > 4500) //terrain 2 3
        {
            td = tds.terrains[11];
        }

        if (pos_x > 4500 && pos_z < 1500) //terrain 3 0
        {
            td = tds.terrains[12];
        }
        if (pos_x > 4500 && (pos_z > 1500 && pos_z < 3000)) //terrain 3 1
        {
            td = tds.terrains[13];
        }
        if (pos_x > 4500 && (pos_z > 3000 && pos_z < 4500)) //terrain 3 2
        {
            td = tds.terrains[14];
        }
        if (pos_x > 4500 && pos_z > 4500) //terrain 3 3
        {
            td = tds.terrains[15];
        }


        terrainHeight = td.SampleHeight(new Vector3((int)pos_x, 0, (int)pos_z));

        return terrainHeight;
    }
}
