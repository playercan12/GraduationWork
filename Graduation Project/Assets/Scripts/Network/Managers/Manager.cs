using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    static Manager s_instance; // 유일성이 보장된다
    static Manager Instance { get { Init(); return s_instance; } } // 유일한 매니저를 갖고온다

    NetworkManager _network = new NetworkManager();
    public static NetworkManager Network { get { return Instance._network; } }

    ObjectManager _obj = new ObjectManager();
    public static ObjectManager Object { get { return Instance._obj; } }

    ResourceManager _resource = new ResourceManager();
    public static ResourceManager Resource { get { return Instance._resource; } }

    PoolManager _pool = new PoolManager();
    public static PoolManager Pool { get { return Instance._pool; } }


    void Start()
    {
        Init();        
    }

    void Update() 
    {
        _network.Update();
    }

    static void Init()
    {
        if (s_instance == null)
        {
            GameObject go = GameObject.Find("Manager");
            if (go == null)
            {
                go = new GameObject { name = "Manager" };
                go.AddComponent<Manager>();
            }

            DontDestroyOnLoad(go);
            s_instance = go.GetComponent<Manager>();

            //s_instance._network.Init();

            s_instance._pool.Init();
        }
    }

    public void NetworkInit()
    {
        s_instance._network.Init();
    }

    public static void Clear()
    {
        Pool.Clear();
    }

}
