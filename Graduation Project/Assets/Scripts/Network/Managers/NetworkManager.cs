using ServerCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using Google.Protobuf;

public class NetworkManager
{
	ServerSession _session = new ServerSession();

	InputIp input = InputIp.Instance;

	public void Send(IMessage packet)
	{
		_session.Send(packet);
	}

	public void Init()
	{
        // DNS(Domain Name System)

        //string host = Dns.GetHostName();
        //IPHostEntry ipHost = Dns.GetHostEntry(host);
        //IPAddress ipAddr = ipHost.AddressList[0];

        //IPEndPoint endPoint = new IPEndPoint(ipAddr, 7777);
        //Connector connector = new Connector();

        //connector.Connect(endPoint,
        //    () => { return _session; },
        //    1);

        // --- 추후 원격 접속을 위함  --- //
        string host = Dns.GetHostName();
        IPHostEntry ipHost = Dns.GetHostEntry(host);
        IPAddress ipAddr = ipHost.AddressList[0];

        IPAddress ServerIp = IPAddress.Parse(input.ip);
        IPEndPoint ep = new IPEndPoint(ServerIp, 7777);
        Connector connector = new Connector();

        connector.Connect(ep,
            () => { return _session; },
            1);
    }

	public void Update()
	{
		// 패킷큐를 여기서 꺼냄
		List<PacketMessage> list = PacketQueue.Instance.PopAll();
		foreach (PacketMessage packet in list)
		{
			Action<PacketSession, IMessage> handler = PacketManager.Instance.GetPacketHandler(packet.Id);
			if (handler != null)
				handler.Invoke(_session, packet.Message);
		}
	}

}
