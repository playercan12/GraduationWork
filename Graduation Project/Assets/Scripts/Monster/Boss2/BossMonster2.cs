﻿using Google.Protobuf.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BossMonster2 : MonsterManager
{
    public GameObject boss2_Wheel;
    public GameObject boss2_Door;
    public GameObject boss2_Head;

    public GameObject enemyPrefab;

    // 몬스터 소환 위치
    public Transform spawnPoint;

    // 레일의 가운데
    public Transform centerPoint;

    // 패턴4 타켓은 OperationField
    public GameObject OperationField;

    public float boss2_speed;
    public bool isOperate;
    public int phase = 0;
    public int randomPattern;
    public float dead_timer = 0;
    public bool isSpawn;
    public bool isRotate;

    // 공격 텀
    public float attackTime;
    public float currentAttackTime;

    // 전깃 줄 공격
    public GameObject electricWire;
    public GameObject[] electricWirePoints;

    // 장판 공격
    public GameObject boss2_ShootPoint;
    public GameObject boss2_BulletTargetPoint;
    public GameObject boss2_floorAttackBullet;

    // 총알 공격
    public GameObject boss2_bullet;
    public Collider[] TargetsColls;
    public GameObject Target;

    public Material myMat;
    // 드론 소환
    public List<MonsterManager> spawnList = new List<MonsterManager>();

    //사망시 애니메이션
    public Animation dieAnim;
    public Animation doomDoorAnim;
    private Vector3 boss3_StartPos;
    public GameObject DeadPoint;

    // 패턴2 플레이어 리스트
    List<GameObject> playerlist = new List<GameObject>();

    public bool IsStartPattern;
    public int OperateCount;

    public AudioSource boss2Audio;
    
    public AudioClip operSound;
    public AudioClip deadSound;

    // Start is called before the first frame update
    void Start()
    {
        boss3_StartPos = this.transform.position;
        isOperate = false;
        isDead = false;
        isRotate = true;
        IsStartPattern = false;

        this.MaxHP = 300;
        this.HP = MaxHP;
        this.armor = 100;
        // 몬스터 공격 시간
        currentAttackTime = attackTime;
        doomDoorAnim = GameObject.Find("Doom Door").GetComponent<Animation>();
        myMat.SetColor("Color_561C20F3", Color.black * 0.0f);

    }

    void Awake()
    {
        OperationField = GameObject.FindWithTag("CollideBoss2Field");
    }

    // Update is called once per frame
    void Update()
    {
        // CheckSpawnMonsterDead();

        if (isOperate && OperateCount == 1)
        {
            C_ChangeStat Change_packet = new C_ChangeStat();
            Change_packet.ObjectId = id;
            StatInfo.Attack = 2;
            Change_packet.StatInfo = StatInfo;
            Manager.Network.Send(Change_packet);
            Debug.Log("Send packet " + Change_packet);
            isOperate = false;
        }

        if (!isDead)
        {
            if (!boss2Audio.isPlaying)
            {
                boss2Audio.clip = operSound;
                boss2Audio.loop = true;
                boss2Audio.Play();
            }
            if (IsStartPattern)
                CalcPhase();

            switch (Pattern)
            {
                case Pattern.One:
                    IsStartPattern = true;
                    break;

                case Pattern.Two:
                    phaseSecondPattern();
                    Pattern = Pattern.No;
                    break;

                case Pattern.Three:
                    phaseThirdPattern();
                    Pattern = Pattern.No;
                    break;

                case Pattern.Four:
                    phaseFourthPattern();
                    Pattern = Pattern.No;
                    break;

                default:
                    return;
            }
        }
        //hp가 0% 이하일 때 죽이자.
        if (isDead)
        {
            if (!boss2Audio.isPlaying)
            {
                boss2Audio.loop = false;
                boss2Audio.clip = deadSound;
                boss2Audio.Play();
            }
            
            if (isRotate)
            {
                transform.RotateAround(centerPoint.position, Vector3.up, boss2_speed * Time.deltaTime);
            }
        }        
    }

    //private void phaseFirstPattern()
    //{
    //    C_ChangeStat Change_packet = new C_ChangeStat();
    //    Change_packet.ObjectId = 4045289;
    //    Manager.Network.Send(Change_packet);
    //}

    private void phaseSecondPattern()
    {
        DetectTarget();

        if (playerlist.Count > 0)
        {
            GameObject _Target = playerlist[0];

            for (int i = 0; i < playerlist.Count; ++i)
            {
                float dist1 = Vector3.Distance(gameObject.transform.position, playerlist[i].transform.position);
                float dist2 = Vector3.Distance(gameObject.transform.position, _Target.transform.position);

                if (dist1 < dist2)
                    _Target = playerlist[i];
            }

            Boss2Bullet boss2Bullet = Instantiate(boss2_bullet, boss2_ShootPoint.transform.position, Quaternion.identity)
                .GetComponent<Boss2Bullet>();

            boss2Bullet.target = _Target.transform.position;
        }

    }

    private void phaseThirdPattern()
    {
        for (int i = 0; i < electricWirePoints.Length; i++)
        {
            Boss2Laser laser = Instantiate(electricWire, electricWirePoints[i].transform.position, Quaternion.Euler(0.0f, 0.0f, 90.0f)).GetComponent<Boss2Laser>();
            laser.target = centerPoint.position;
        }
    }

    private void phaseFourthPattern()
    {
        Boss2FloorAttackBullet boss2FloorAttackBullet = Instantiate(boss2_floorAttackBullet, boss2_ShootPoint.transform.position, Quaternion.identity)
            .GetComponent<Boss2FloorAttackBullet>();

        boss2FloorAttackBullet.target = OperationField.transform.position;
    }

    private void DetectTarget()
    {
        TargetsColls = Physics.OverlapSphere(transform.position, 100.0f);

        // 여기서 범위 안에 들어온 플레이어를 찾아서 Targets[] 에 append
        for (int i = 0; i < TargetsColls.Length; i++)
        {
            if (TargetsColls[i].tag == "Player")
            {
                playerlist.Add(TargetsColls[i].gameObject);
            }
        }
    }

    private void CalcPhase()
    {
        float hpPer = ((float)((float)this.s_HP / (float)this.s_MaxHP) * 100.0f);

        Debug.Log("hp Per" + hpPer);

        if (hpPer <= 100.0f && hpPer >= 80.0f)
        {
            myMat.SetColor("Color_561C20F3", Color.green * 50000.0f);
            transform.RotateAround(centerPoint.position, Vector3.up, boss2_speed * Time.deltaTime);
        }

        if (hpPer < 80.0f && hpPer >= 50.0f)
        {
            myMat.SetColor("Color_561C20F3", Color.yellow * 50000.0f);
            transform.RotateAround(centerPoint.position, Vector3.up, -boss2_speed * Time.deltaTime);
        }
        if (hpPer < 50.0f && hpPer >= 30.0f)
        {
            myMat.SetColor("Color_561C20F3", Color.magenta * 50000.0f);
            transform.RotateAround(centerPoint.position, Vector3.up, boss2_speed * Time.deltaTime);
        }

        if (hpPer < 30.0f && hpPer > 0.0f)
        {
            myMat.SetColor("Color_561C20F3", Color.red * 50000.0f);
            transform.RotateAround(centerPoint.position, Vector3.up, -boss2_speed * 1.3f * Time.deltaTime);
        }

        // hp가 0이하일 때 ----> 죽자.
        if (s_HP <= 0.0f && !isDead)
        {
            if (boss2Audio.isPlaying)
            {
                boss2Audio.Stop();
            }
            
            myMat.SetColor("Color_561C20F3", Color.black * 50000.0f);

            Instantiate(DeadPoint, DeadPoint.transform.position, Quaternion.identity);

            isDead = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isDead)
        {
            if (other.CompareTag("Boss2DeadPoint"))
            {
                isRotate = false;
                dieAnim.Play();
                doomDoorAnim.Play();
            }
        }
    }


    private void CheckSpawnMonsterDead()
    {
        for (int i = spawnList.Count - 1; i >= 0; i--)
        {
            if (spawnList[i].isDead)
            {
                Debug.Log("dead");
                spawnList.Remove(spawnList[i]);
            }
        }
    }
}
