﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.VFX;

public class Boss3FireThrow : MonoBehaviour
{
    public Transform pos;
    public float persistTime;
    public BoxCollider boxCollider;

    public AudioSource audio;
    
    private void Update()
    {
        if (!audio.isPlaying)
        {
            audio.loop = true;
            audio.Play();
        }
        
        if (pos != null)
        {
            transform.position = pos.position;
            transform.eulerAngles = new Vector3(pos.rotation.eulerAngles.x + 25.0f, pos.rotation.eulerAngles.y + 90.0f, pos.rotation.eulerAngles.z);

            persistTime -= Time.deltaTime;
            if (persistTime <= 2.8f)
            {
                boxCollider.enabled = true;
            }
            if (persistTime <= 0.0f)
            {
                Destroy(this.gameObject);
            }
        }
    }

    //private void OnParticleCollision(GameObject other)
    //{
    //    if (other.GetComponent<Collider>().CompareTag("Player"))
    //    {
    //        other.gameObject.GetComponent<Player>().hit(5);
    //    }
    //}

}