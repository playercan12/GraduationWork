﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushTrigger : MonoBehaviour
{
    public float pushForce = 10.0f;
    public Animation boss3_moving_Attack;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            boss3_moving_Attack.Play();

            other.GetComponent<AnimationObj>().playerRb.AddForce(transform.forward*pushForce, ForceMode.Impulse);

            //if (other.GetComponent<Player>() != null)
            //{
            //    other.GetComponent<Player>().hit(20);
            //}

        }
    }
}
