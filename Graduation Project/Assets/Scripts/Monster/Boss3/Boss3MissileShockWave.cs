﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss3MissileShockWave : MonoBehaviour
{
    public float radius;

    public Collider[] colls;

    private bool isBomb;
    public float persistTime;

    public GameObject explosionEffect;

    private void Start()
    {
        Instantiate(explosionEffect, transform.position, Quaternion.identity);
        
        isBomb = true;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (isBomb)
        {
            persistTime -= Time.deltaTime;
            
            if (persistTime <= 0.0f)
            {
                isBomb = false;
            }
        }

        if (!isBomb)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.CompareTag("Player"))
        //{
        //    other.gameObject.GetComponent<Player>().hit(30);
        //}
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, radius);
    }
}
