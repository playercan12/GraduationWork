﻿using Google.Protobuf.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BossMonster3 : MonsterManager
{
    public Transform[] turnPoint;
    
    public GameObject[] missilePos;
    public GameObject boss3_missile;
    public int missileCount;

    public GameObject boss3_line_renderer;

    public GameObject boss3_firethrow;
    public GameObject boss3_firethrowPos;

    public Collider[] TargetsColls;

    public GameObject Target;
    public Vector3 MoveToTargetPos;

    public float attackTime;
    private float currentAttackTime;

    public float fireThrowTime;
    public float currentFireThrowTime;

    public float playerTraceTime;
    public float currentPlayerTraceTime;

    // 보스 상태변수
    public bool isOperate;
    public bool isMoving;
    public bool isRotating;

    // 애니메이션 변수
    public bool isFactories;
    public bool isFirstWalls;
    public bool isSecondWalls;

    public bool isAnimating;
    public float delayTime;
    private float currentDelayTime;


    public int pattern;
    public float boss3_velocity;

    public int turnCount = 0;

    public int phase = 0;

    public List<GameObject> PlayerList = new List<GameObject>();

    public Animation factoriesAnim;

    public Animation boss3_firstWallsAnim;
    public Animation boss3_secondWallsAnim;

    public Animation boss3_leftLegAnim;
    public Animation boss3_rightLegAnim;

    public Animation boss3_leftArmAnim;
    public Animation boss3_rightArmAnim;

    public List<MonsterManager> spawnList = new List<MonsterManager>();

    private bool UnisFactories;

    public bool IsPatternStart;

    public GameObject leftLineRendererPos;
    public GameObject rightLineRendererPos;

    public Material myMat;
    
    
    public GameObject boss3_Body;
    public GameObject boss3_leftArm;
    public GameObject boss3_rightArm;
    public GameObject boss3_leftLeg;
    public GameObject boss3_rightLeg;
    public GameObject boss3_Head;

    public float deadTimer;

    public AudioSource boss3Audio;

    public AudioClip operSound;
    public AudioClip deadSound;

    void Start()
    {
        isOperate = false;
        isMoving = true;
        isRotating = false;

        isFactories = false;
        isFirstWalls = false;
        isSecondWalls = false;
        isAnimating = false;
        UnisFactories = false;


        currentAttackTime = attackTime;
        //currentFireThrowTime = fireThrowTime;

        currentDelayTime = delayTime;

        //currentPlayerTraceTime = playerTraceTime;

        this.MaxHP = 1000;
        HP = MaxHP;

        missileCount = missilePos.Length;
        myMat.SetColor("Color_561C20F3", Color.black * 0.0f);

        factoriesAnim = GameObject.Find("Factories").GetComponent<Animation>();
        boss3_firstWallsAnim = GameObject.Find("FirstWallAnim").GetComponent<Animation>();
        boss3_secondWallsAnim = GameObject.Find("SecondWallAnim").GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        //TargetsColls = Physics.OverlapSphere(transform.position, 50.0f);

        //if (!isOperate)
        //{
        //    // 여기서 범위 안에 들어온 플레이어를 찾아서 Targets[] 에 append
        //    for (int i = 0; i < TargetsColls.Length; i++)
        //    {
        //        if (TargetsColls[i].tag == "Player")
        //        {
        //            isOperate = true;
        //        }
        //    }
        //}


        if (!isDead)
        {
            float hpPer = ((float)((float)this.s_HP / (float)this.s_MaxHP) * 100.0f);

            if (!boss3Audio.isPlaying)
            {
                boss3Audio.clip = operSound;
                boss3Audio.loop = true;
                boss3Audio.Play();
            }
                
            CalcPhase();

            if (hpPer <= 99.0f)
            {
                if (!isFactories)
                {
                    factoriesAnim.Play("Factories");
                    isFactories = true;
                }

                myMat.SetColor("Color_561C20F3", Color.green * 50000.0f);

                currentFireThrowTime -= Time.deltaTime;
                if (currentFireThrowTime <= 0.0f)
                {
                    FireThrowPattern();
                    currentFireThrowTime = fireThrowTime;
                }
            }



            if (hpPer < 50.0f && !UnisFactories)
            {
                factoriesAnim.Play("UnIsFactories");
                UnisFactories = true;
            }

            //if (turnCount == 3 && !isFirstWalls)
            //{
            //    boss3_firstWallsAnim.Play();
            //    isFirstWalls = true;
            //}


            if (turnCount == 7 && !isSecondWalls)
            {
                boss3_secondWallsAnim.Play();
                isSecondWalls = true;
            }

            if (isAnimating)
            {
                currentDelayTime -= Time.deltaTime;
                if (currentDelayTime <= 0.0f)
                {
                    /// 수정
                    Boss3LineRenderer boss3leftLineRenderer = Instantiate(boss3_line_renderer, leftLineRendererPos.transform.position, Quaternion.identity)
                                .GetComponent<Boss3LineRenderer>();

                    Boss3LineRenderer boss3rightLineRenderer = Instantiate(boss3_line_renderer, rightLineRendererPos.transform.position, Quaternion.identity)
                        .GetComponent<Boss3LineRenderer>();

                    currentDelayTime = delayTime;
                    isAnimating = false;
                }
            }

            if (hpPer < 70.0f && hpPer > 50.0f)
            {
                myMat.SetColor("Color_561C20F3", Color.yellow * 50000.0f);

                Debug.Log("페이즈2 시작 움직임 ");
                TraceMoving();
            }

            if (hpPer < 50.0f)
            {
                myMat.SetColor("Color_561C20F3", Color.red * 50000.0f);

                Debug.Log("페이즈3 시작 움직임 ");
                DetectTarget();
                PlayerTraceMoving();
                currentPlayerTraceTime -= Time.deltaTime;

                if (currentPlayerTraceTime <= 0.0f)
                {
                    MoveToTargetPos = Target.transform.position;
                    isRotating = true;
                    currentPlayerTraceTime = playerTraceTime;
                }

                // 최종발표 때문에 강제로 넣어놨음.
                if (!isFirstWalls)
                {
                    boss3_firstWallsAnim.Play();
                    isFirstWalls = true;
                }
            }

            switch (Pattern)
            {
                case Pattern.One:
                    FirstPhase();
                    Pattern = Pattern.No;
                    break;

                case Pattern.Two:
                    SecondPhase();
                    Pattern = Pattern.No;
                    break;

                case Pattern.Three:
                    ThirdPhase();
                    Pattern = Pattern.No;
                    break;

                default:
                    break;
            }

            if (hpPer <= 0.0f)
            {
                if (boss3Audio.isPlaying)
                {
                    boss3Audio.Stop();
                }
                    
                DeadFunction(boss3_leftArm);
                DeadFunction(boss3_rightArm);
                DeadFunction(boss3_leftLeg);
                DeadFunction(boss3_rightLeg);
                DeadFunction(boss3_Body);
                DeadFunction(boss3_Head);

                isDead = true;

            }
        }


        if (isDead)
        {
            if (!boss3Audio.isPlaying)
            {
                boss3Audio.loop = false;
                boss3Audio.clip = deadSound;
                boss3Audio.Play();
            }
                
            deadTimer += Time.deltaTime;
            if (deadTimer >= 5.0f)
            {
                Destroy(this.gameObject);
            }
        }
        
    }

    private void DeadFunction(GameObject go)
    {
        go.transform.parent = null;
        go.AddComponent<Rigidbody>();
        go.AddComponent<MeshDestroy>();
    }

    private void FirstPhase()
    {
        if (turnCount % 2 != 1)
        {
            if (RandWall == 1)
            {
                MissilePattern();
            }

            if (RandWall == 2)
            {
                LaserPattern();
            }
        }

    }

    private void SecondPhase()
    {

        if (turnCount % 2 != 1)
        {
            if (RandWall == 1)
            {
                MissilePattern();
            }

            if (RandWall == 2)
            {
                LaserPattern();
            }
        }
    }

    private void ThirdPhase()
    {
        //SummonPattern();
        MissilePattern();
        LaserPattern();
    }

    private void PlayerTraceMoving()
    {
        if (isRotating)
        {
            Vector3 turnDirection = new Vector3(MoveToTargetPos.x, transform.position.y, MoveToTargetPos.z) -
                                    transform.position;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, turnDirection, 2.0f * Time.deltaTime, 0f);

            transform.rotation = Quaternion.LookRotation(newDirection);

            float angle = Vector3.Angle(turnDirection, transform.forward);
            if (angle <= 0.0f)
            {
                isMoving = true;
                isRotating = false;
            }
        }

        if (isMoving)
        {
            this.transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(MoveToTargetPos.x, transform.position.y, MoveToTargetPos.z),
                boss3_velocity * Time.deltaTime);

            if (transform.position == new Vector3(MoveToTargetPos.x, transform.position.y, MoveToTargetPos.z))
            {
                MoveToTargetPos = Target.transform.position;
                Vector3 turnDirection = new Vector3(MoveToTargetPos.x, transform.position.y, MoveToTargetPos.z) -
                                        transform.position;
                Vector3 newDirection = Vector3.RotateTowards(transform.forward, turnDirection, 2.0f * Time.deltaTime, 0f);

                transform.rotation = Quaternion.LookRotation(newDirection);

                isMoving = false;
            }
        }
    }

    private void TraceMoving()
    {
        if (isMoving)
        {
            transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(turnPoint[turnCount].transform.position.x, transform.position.y, turnPoint[turnCount].transform.position.z),
                boss3_velocity * Time.deltaTime);

            if (transform.position == new Vector3(turnPoint[turnCount].transform.position.x, transform.position.y, turnPoint[turnCount].transform.position.z))
            {
                if (turnCount < 10)
                {
                    turnCount++;
                }

                if (turnCount != 10)
                {
                    isRotating = true;
                    isMoving = false;
                }
            }
        }

        if (isRotating)
        {
            Vector3 turnDirection = new Vector3(turnPoint[turnCount].transform.position.x, transform.position.y,
                 turnPoint[turnCount].transform.position.z) - transform.position;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, turnDirection, 2.0f * Time.deltaTime, 0f);

            Vector3 targetDirection = transform.position - new Vector3(turnPoint[turnCount].transform.position.x,
                transform.position.y,
                turnPoint[turnCount].transform.position.z);

            turnPoint[turnCount].rotation = Quaternion.LookRotation(targetDirection);

            transform.rotation = Quaternion.LookRotation(newDirection);

            float angle = Quaternion.Angle(transform.rotation,
                turnPoint[turnCount].transform.rotation);
            if (angle >= 179.0f)
            {
                isMoving = true;
                isRotating = false;
            }
        }
    }

    //private void SummonPattern()
    //{
    //    if (spawnList.Count <= 10)
    //    {

    //        ClosedMonster closedMonster = Instantiate(_ClosedMonster, ClosedMonsterSpawnPos.transform.position,
    //            ClosedMonsterSpawnPos.transform.rotation).GetComponent<ClosedMonster>();
    //        spawnList.Add(closedMonster);

    //        RangedMonster rangedMonster = Instantiate(_RangedMonster, RangedMonsterSpawnPos.transform.position,
    //            RangedMonsterSpawnPos.transform.rotation).GetComponent<RangedMonster>();
    //        spawnList.Add(rangedMonster);

    //    }
    //}

    //private void CheckSpawnMonsterDead()
    //{

    //    for(int i = spawnList.Count-1; i>=0; i--)
    //    {
    //        if (spawnList[i].isDead)
    //        {
    //            Debug.Log("dead");
    //            spawnList.Remove(spawnList[i]);

    //        }
    //    }
    //}

    private void MissilePattern()
    {
        DetectTarget();
        RaycastHit hit;
        if (Physics.Raycast(
            new Vector3(Target.transform.position.x, Target.transform.position.y, Target.transform.position.z),
            Vector3.down, out hit, 5.0f))
        {
            if (hit.collider != null)
            {
                Boss3Missile missile = Instantiate(boss3_missile, missilePos[missileCount - 1].transform.position, Quaternion.identity)
                    .GetComponent<Boss3Missile>();
                if (missileCount == 4 || missileCount == 3)
                {
                    boss3_leftArmAnim.Play();
                }

                if (missileCount == 2 || missileCount == 1)
                {
                    boss3_rightArmAnim.Play();
                }
                missile.Target = hit.point;
                missileCount--;
                if (missileCount == 0)
                {
                    missileCount = missilePos.Length;
                }
            }
        }
    }

    private void DetectTarget()
    {
        TargetsColls = Physics.OverlapSphere(transform.position, 200.0f);

        for (int i = 0; i < TargetsColls.Length; i++)
        {
            if (TargetsColls[i].tag == "Player")
            {
                Target = TargetsColls[i].gameObject;
            }
        }
    }

    private void LaserPattern()
    {
        boss3_leftLegAnim.Play();
        boss3_rightLegAnim.Play();

        isAnimating = true;
    }

    private void FireThrowPattern()
    {
        Boss3FireThrow fireThrow =
            Instantiate(boss3_firethrow, boss3_firethrowPos.transform.position, Quaternion.identity)
                .GetComponent<Boss3FireThrow>();
        fireThrow.pos = boss3_firethrowPos.transform;
        fireThrow.transform.forward = transform.forward;
    }

    private void CalcPhase()
    {
        float hpPer = ((float)((float)this.HP / (float)this.MaxHP) * 100.0f);

        if (hpPer <= 99.0f && hpPer >= 80.0f)
        {
            myMat.SetColor("Color_561C20F3", Color.black * 0.0f);

            //phase = 1;
        }

        if (hpPer < 80.0f && hpPer > 0.0f && turnCount < 10)
        {

            //phase = 2;
        }

        if (transform.position == new Vector3(turnPoint[turnPoint.Length - 1].transform.position.x, transform.position.y, turnPoint[turnPoint.Length - 1].transform.position.z))
        {
            isMoving = false;
            //phase = 3;
        }
        // hp가 0이하일 때 ----> 죽자.
        if (hpPer <= 0.0f)
        {
            isDead = true;
        }
    }

}
