﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss3LineRenderer : MonoBehaviour
{
    public int segments;
    public float xradius;
    public float yradius;
    public int gap;
    LineRenderer line;
    public float velocity;
    public float persistTime = 5.0f;

    public AudioSource audio;

    void Start()
    {

        // -90도로 꺽기
        transform.eulerAngles = new Vector3(transform.eulerAngles.x - 90.0f, transform.eulerAngles.y, transform.eulerAngles.z);


        line = gameObject.GetComponent<LineRenderer>();

        line.useWorldSpace = false;

        line.SetVertexCount(segments + 1 - (segments / gap));

        CreatePoints();
    }

    private void Update()
    {
        if (!audio.isPlaying)
        {
            audio.loop = true;
            audio.Play();
        }
        
        
        this.transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * velocity,
            transform.localScale.y + Time.deltaTime * velocity, transform.localScale.z);


        persistTime -= Time.deltaTime;
        if (persistTime <= 0.0f)
        {
            Destroy(this.gameObject);
        }
    }

    void CreatePoints()
    {
        float x;
        float y;
        float z = 0f;

        float angle = 0f;

        for (int i = 0; i < (segments + 1) - (segments / gap); i++)
        {
            x = Mathf.Cos(Mathf.Deg2Rad * angle) * xradius;
            y = Mathf.Sin(Mathf.Deg2Rad * angle) * yradius;

            line.SetPosition(i, new Vector3(x, y, z));

            angle += (360f / segments);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
    }
}
