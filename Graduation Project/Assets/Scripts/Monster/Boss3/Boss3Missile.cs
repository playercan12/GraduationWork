﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss3Missile : MonoBehaviour
{
    public Vector3 Target;
    
    // 미사일 쏘는 부분
    public float missileVel;
    private Vector3 newDir;
    public GameObject shockWave;
    public GameObject targetPointEffect;

    public GameObject tPE;
    
    private void Start()
    {
        tPE = Instantiate(targetPointEffect, new Vector3(Target.x,Target.y+0.1f,Target.z), Quaternion.LookRotation(transform.up));

        Vector3 targetDirection = Target - transform.position;
        transform.rotation = Quaternion.LookRotation(targetDirection);
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position,Target,missileVel * Time.deltaTime);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    Instantiate(shockWave, transform.position, Quaternion.identity);
    //    Destroy(tPE);
    //    Destroy(this.gameObject);

    //}

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.transform.name);

        Instantiate(shockWave, transform.position, Quaternion.identity);
        Destroy(tPE);
        Destroy(this.gameObject);
    }

}
