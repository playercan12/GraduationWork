﻿using Google.Protobuf.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PatrolDroneMonster : MonsterManager
{
    //보류
    public MeshCollider meshCollider;

    public GameObject Missile;
    public GameObject missilePos;
    private Vector3 missleStartPos;

    public GameObject[] droneWing;
    public int wingRotationSpeed;

    public float droneVelocity;
    public float maximumHeight;

    public Collider[] colls;

    private Vector3 monsterStartPos;

    private bool alreadyAttacked = false;
    [SerializeField] private float attackTerm;
    // 상태값
    private bool isAttack = false;
    private float timer = 0.0f;

    public Transform target;
    public bool targetOn;

    // 패트롤
    public float range = 50.0f;

    public float patrolTime = 3.0f;
    private float currentPatrolTime;
    private float minX, maxX, minZ, maxZ;

    private Vector3 moveSpot;

    private bool isPatrol;

    // 맞았을 때 타이머
    private float currentAgroTime;
    private float agroTime = 5.0f;
    public bool agroOn = false;

    public bool patrolOn = false;

    private float repeateAttack = 3.5f;

    public AudioSource patrolDroneMonsterAudioSource;
    
    public AudioClip flySound;
    public AudioClip deadSound;

    private void Start()
    {
        maximumHeight = Rotate;
    }

    private void Awake()
    {
        meshCollider = GetComponent<MeshCollider>();

        // 플레이어 타겟 잡는 곳
        targetOn = false;

        this.MaxHP = 250;
        this.HP = this.MaxHP;
        this.armor = 30;

        //GetPatrolRange();
        moveSpot = GetNewPosition();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            if (!patrolDroneMonsterAudioSource.isPlaying)
            {
                patrolDroneMonsterAudioSource.clip = flySound;
                patrolDroneMonsterAudioSource.loop = true;
                patrolDroneMonsterAudioSource.Play();
            }
            
            // 날개 네개를 돌리는것이다
            for (int i = 0; i < droneWing.Length; i++)
            {
                droneWing[i].GetComponent<Transform>().Rotate(new Vector3(0, Time.deltaTime * wingRotationSpeed, 0), Space.World);
            }

            RaycastHit hit;
            if (Physics.Raycast(new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Vector3.down, out hit, 100.0f))
            {
                if (hit.collider != null)
                {
                    transform.position = Vector3.MoveTowards(transform.position,
                        new Vector3(transform.position.x, hit.point.y + (float)maximumHeight, transform.position.z),
                        droneVelocity * Time.deltaTime);
                }
            }

            if (!targetOn)
            {
                WatchYourStep();
                GetToStepping();
            }

            if (s_HP <= 0)
            {
                if (patrolDroneMonsterAudioSource.isPlaying)
                {
                    patrolDroneMonsterAudioSource.Stop();
                }

                Dead();
            }

            PosInfo.SpineY -= Time.deltaTime;

            if (PosInfo.SpineY <= 0)
            {
                Fire();
                PosInfo.SpineY = repeateAttack;
            }
        }


        if (isDead)
        {
            if (!patrolDroneMonsterAudioSource.isPlaying)
            {
                patrolDroneMonsterAudioSource.loop = false;
                patrolDroneMonsterAudioSource.clip = deadSound;
                patrolDroneMonsterAudioSource.Play();
            }
            
            rigid.useGravity = true;
            rigid.AddForce(Vector3.down * 50.0f, ForceMode.Acceleration);

            timer += Time.deltaTime;
            if (timer >= 5.0f)
            {
                Destroy(this.gameObject.transform.parent.gameObject);
            }
        }
    }
    
    Vector3 GetNewPosition()
    {
        float randomX = PosInfo.SpineX;
        float randomZ = PosInfo.SpineZ;

        Vector3 newPosition = new Vector3(randomX, transform.position.y, randomZ);
        return newPosition;
    }

    private void WatchYourStep()
    {
        Vector3 targetDirection = new Vector3(moveSpot.x, transform.position.y, moveSpot.z) - transform.position;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 0.3f, 0f);
        newDirection = new Vector3(newDirection.x, 90.0f, newDirection.z);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    private void GetToStepping()
    {
        transform.position =
            Vector3.MoveTowards(transform.position, moveSpot,
                3.0f * Time.deltaTime);

        moveSpot = GetNewPosition();
        currentPatrolTime = patrolTime;
    }


    private void AttackTarget()
    {
        if (!alreadyAttacked)
        {
            Fire();
        }
    }

    private void Fire()
    {
        Manager.Resource.Instantiate("Monster/DroneMonster/Monster3 Missile", missilePos.transform.position);

        //alreadyAttacked = true;
        //Invoke(nameof(ResetAttack), attackTerm);
    }

    private void ResetAttack()
    {
        alreadyAttacked = false;
    }
}
