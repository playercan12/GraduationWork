﻿using Google.Protobuf.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.VFX;

public class PatrolRangedMonster : MonsterManager
{
    public MeshCollider meshCollider;
    public GameObject FirePos;

    public GameObject HeadPart;
    //public bool isHit = false;

    public float xPos;
    public float yPos;
    public float zPos;


    private float timer = 0.0f;
    private bool targetOn = false;


    public GameObject projectile;
    private Vector3 bulletStartPos;



    // 패트롤
    public float range = 10.0f;
    private bool isPatrol;
    private float minX, maxX, minZ, maxZ;
    private Vector3 moveSpot; // 내가 처음에 가야할 위치

    private float patrolTime = 3.0f;
    private float currentPatrolTime = 0.0f;

    private float repeateAttack = 2.5f;

    public AudioSource patrolRangedMonsterAudioSource;

    public AudioClip walkSound;
    public AudioClip deadSound;

    private void Start()
    {
        transform.position = CellPos;
    }

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        meshCollider = GetComponent<MeshCollider>();
        nav = GetComponent<NavMeshAgent>();

        bulletStartPos = FirePos.transform.position;

        nav.enabled = true;
        // 플레이어 타겟 잡는 곳
        targetOn = false;

        moveSpot = GetNewPosition();

        this.MaxHP = 300;
        this.HP = this.MaxHP;
        this.armor = 30;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(PosInfo.SpineY);

        if (!isDead)
        {
            if (!patrolRangedMonsterAudioSource.isPlaying)
            {
                patrolRangedMonsterAudioSource.clip = walkSound;
                patrolRangedMonsterAudioSource.loop = true;
                patrolRangedMonsterAudioSource.Play();
            }
            
            if (!targetOn)
            {
                WatchYourStep();
                GetToStepping();
            }

            PosInfo.SpineY -= Time.deltaTime;

            if (PosInfo.SpineY <= 0) 
            {
                Fire();
                PosInfo.SpineY = repeateAttack;
            }

            if (s_HP <= 0)
            {
                if (patrolRangedMonsterAudioSource.isPlaying)
                {
                    patrolRangedMonsterAudioSource.Stop();
                }

                
                HeadPart.transform.parent = null;
                HeadPart.AddComponent<Rigidbody>();

                Dead();
            }

        }

        if (isDead)
        {
            if (!patrolRangedMonsterAudioSource.isPlaying)
            {
                patrolRangedMonsterAudioSource.loop = false;
                patrolRangedMonsterAudioSource.clip = deadSound;
                patrolRangedMonsterAudioSource.Play();
            }
            
            timer += Time.deltaTime;
            if (timer >= 2.0)
            {
                Destroy(HeadPart);
                Destroy(this.gameObject.transform.parent.gameObject);
            }
        }
    }

    private void Fire()
    {
        Rigidbody rb = Manager.Resource.Instantiate("Monster/RangedMonster/MonsterBullet", FirePos.transform.position).GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * 10f, ForceMode.Impulse);
        rb.AddForce(transform.up * 2f, ForceMode.Impulse);
    }

    Vector3 GetNewPosition()
    {
        float randomX = PosInfo.SpineX;
        float randomZ = PosInfo.SpineZ;

        Vector3 newPosition = new Vector3(randomX, transform.position.y, randomZ);
        return newPosition;
    }

    private void WatchYourStep()
    {
        Vector3 targetDirection = moveSpot - transform.position;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 0.3f, 0f);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    private void GetToStepping()
    {
        nav.SetDestination(moveSpot);   // 내가 가야할 위치

        moveSpot = GetNewPosition();
        currentPatrolTime = patrolTime;
    }
}