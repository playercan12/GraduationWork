﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterBullet : MonoBehaviour
{
    public int damage = 10;
    public AudioSource audio;

    private void Start()
    {
        if (!audio.isPlaying)
        {
            audio.Play();
            audio.loop = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Manager.Resource.Destroy(this.gameObject);
        }

        if (other.CompareTag("Terrain"))
        {
            Manager.Resource.Destroy(this.gameObject);
        }

        if (other.CompareTag("Cave"))
        {
            Manager.Resource.Destroy(this.gameObject);
        }

        if (other.CompareTag("Dungeon2FirstFloor"))
        {
            Manager.Resource.Destroy(this.gameObject);
        }
    }
}