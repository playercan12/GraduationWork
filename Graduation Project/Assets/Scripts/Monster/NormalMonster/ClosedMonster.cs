﻿using Google.Protobuf.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using Random = System.Random;

public class ClosedMonster : MonsterManager
{
    public Animator anim;

    public GameObject livedparts;
    public GameObject deadparts;
    public MeshCollider meshCollider;

    private Vector3 monsterStartPos;

    // 상태값
    private bool isAttack = false;

    private float timer = 0.0f;

    public float attackTerm;
    private float currentAttackTerm;
    private float motionDelay = 1.0f;
    public Collider[] colls;
    public Transform target;
    public bool targetOn;

    public BoxCollider attackCol;

    // 패트롤
    public float range = 10.0f;

    // 맞았을 때 타이머
    private float currentAgroTime;
    private float agroTime = 5.0f;
    public bool agroOn = false;


    private float repeateAttack = 3.0f;
    
    public AudioSource ClosedMonsterAudioSource;

    public AudioClip walkSound;
    public AudioClip attackSound;
    public AudioClip deadSound;

    private void Start()
    {
        transform.position = CellPos;
    }

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        meshCollider = GetComponent<MeshCollider>();
        nav = GetComponent<NavMeshAgent>();

        monsterStartPos = transform.position;


        nav.enabled = true;

        // 플레이어 타겟 잡는 곳
        targetOn = false;
        currentAttackTerm = attackTerm;

        // 패트롤
        //isPatrol = true;

        //this.MaxHP = 400;
        //this.HP = MaxHP;
        //this.armor = 50;

        //GetPatrolRange();
        //moveSpot = GetNewPosition();
        //currentPatrolTime = patrolTime;

        currentAgroTime = agroTime;
        //bm = GetComponent<BossMonster>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isHit)
        {
            //StopTrace();
            Rigidity();
            agroOn = true;
        }
        if (!isDead)
        {
            if (!ClosedMonsterAudioSource.isPlaying)
            {
                ClosedMonsterAudioSource.clip = walkSound;
                ClosedMonsterAudioSource.loop = true;
                ClosedMonsterAudioSource.Play();
            }
            
            colls = Physics.OverlapSphere(transform.position, 20.0f);

            for (int i = 0; i < colls.Length; i++)
            {
                if (colls[i].tag == "Player")
                {
                    target = colls[i].transform;
                    targetOn = true;
                    break;
                }
            }

            if (targetOn)
            {
                TraceTarget();
            }

            PosInfo.SpineY -= Time.deltaTime;

            if (PosInfo.SpineY <= 0)
            {
                AttackTarget();
                PosInfo.SpineY = repeateAttack;
            }

            if (s_HP <= 0)
            {
                if (ClosedMonsterAudioSource.isPlaying)
                {
                    ClosedMonsterAudioSource.Stop();
                }
 
                livedparts.SetActive(false);
                deadparts.SetActive(true);

                Dead();
            }
        }

        if (isDead)
        {
            if (!ClosedMonsterAudioSource.isPlaying)
            {
                ClosedMonsterAudioSource.loop = false;
                ClosedMonsterAudioSource.clip = deadSound;
                ClosedMonsterAudioSource.Play();
            }

            timer += Time.deltaTime;
            if (timer >= 2.0f)
            {
                Destroy(this.gameObject.transform.parent.gameObject);
            }
        }
    }

    private void TraceTarget()
    {
        // 플레이어와의 거리가 20보다 가까울 때 
        nav.Resume();
        transform.LookAt(target);
        anim.SetBool("Walking", true);
        nav.SetDestination(target.position);
    }

    private void StopTrace()
    {
        nav.SetDestination(monsterStartPos);
        transform.LookAt(monsterStartPos);
        if (Vector3.Distance(monsterStartPos, transform.position) <= 1.0f)
        {
            anim.SetBool("Walking", false);
            targetOn = false;
        }
    }

    private void AttackTarget()
    {
        ClosedMonsterAudioSource.clip = attackSound;
        nav.Stop();
        anim.SetTrigger("Attack");
        attackCol.gameObject.SetActive(true);
    }

    public void Rigidity()
    {
        nav.Stop();
        anim.SetTrigger("Hit");
        isHit = false;
    }

}