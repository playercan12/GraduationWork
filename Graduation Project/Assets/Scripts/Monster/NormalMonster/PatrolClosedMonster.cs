﻿using Google.Protobuf.Protocol;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolClosedMonster : MonsterManager
{
    public MeshCollider meshCollider;

    public Animator anim;
    public GameObject livedparts;
    public GameObject deadparts;

    //public bool isHit = false;

    public float xPos;
    public float yPos;
    public float zPos;

    public int damage = 80;

    private float timer = 0.0f;
    private bool targetOn = false;

    // 패트롤
    public float range = 10.0f;
    private Vector3 moveSpot; // 내가 처음에 가야할 위치

    private float patrolTime = 3.0f;
    private float currentPatrolTime = 0.0f;

    public AudioSource patrolClosedMonsterAudioSource;

    public AudioClip walkSound;
    public AudioClip attackSound;
    public AudioClip deadSound;
    

    void Start()
    {
        transform.position = CellPos;
    }

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        meshCollider = GetComponent<MeshCollider>();
        nav = GetComponent<NavMeshAgent>();

        nav.enabled = true;

        moveSpot = GetNewPosition();

        //transform.position = CellPos;
    }

    void Update()
    {
        if (!isDead)
        {
            if (!patrolClosedMonsterAudioSource.isPlaying)
            {
                patrolClosedMonsterAudioSource.clip = walkSound;
                patrolClosedMonsterAudioSource.loop = true;
                patrolClosedMonsterAudioSource.Play();
            }
            
            if (!targetOn)
            {
                WatchYourStep();
                GetToStepping();
            }

            if (s_HP <= 0)
            {
                if (patrolClosedMonsterAudioSource.isPlaying)
                {
                    patrolClosedMonsterAudioSource.Stop();
                }
                
                livedparts.SetActive(false);
                deadparts.SetActive(true);

                Dead();
            }
        }

        if (isDead)
        {
            if (!patrolClosedMonsterAudioSource.isPlaying)
            {
                patrolClosedMonsterAudioSource.loop = false;
                patrolClosedMonsterAudioSource.clip = deadSound;
                patrolClosedMonsterAudioSource.Play();
            }

            timer += Time.deltaTime;
            if (timer >= 2.0f)
            {
                Destroy(this.gameObject.transform.parent.gameObject);
            }
        }
    }

    Vector3 GetNewPosition()
    {
        float randomX = PosInfo.SpineX;
        float randomZ = PosInfo.SpineZ;

        Vector3 newPosition = new Vector3(randomX, transform.position.y, randomZ);
        return newPosition;
    }

    private void WatchYourStep()
    {
        Vector3 targetDirection = moveSpot - transform.position;
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 0.3f, 0f);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    private void GetToStepping()
    {
        nav.SetDestination(moveSpot);   // 내가 가야할 위치
        anim.SetBool("Walking", true);

        moveSpot = GetNewPosition();
        currentPatrolTime = patrolTime;
    }
}