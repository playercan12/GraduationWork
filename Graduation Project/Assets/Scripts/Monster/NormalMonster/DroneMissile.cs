﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMissile : MonoBehaviour
{
    public GameObject shockWave;

    private float Destroytime = 8.0f;

    private void Update()
    {
        Destroytime -= Time.deltaTime;
        if (Destroytime <= 0.0f)
        {
            Manager.Resource.Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("DroneMonster"))
        {
            Manager.Resource.Instantiate("Monster/DroneMonster/Monster3 MissileShockWave", transform.position);

            Manager.Resource.Destroy(this.gameObject);
        }
    }
}
