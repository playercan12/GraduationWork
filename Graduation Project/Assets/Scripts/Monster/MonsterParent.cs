﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterParent : MonoBehaviour
{
    private int playerCount;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerCount++;

            if (!transform.GetChild(0).gameObject.activeSelf)
            {
                transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerCount--;

            if (transform.GetChild(0).gameObject.activeSelf && playerCount <= 0)
            {
                transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }
}
