﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEffect : MonoBehaviour
{
    private float existTime;
    // Start is called before the first frame update
    void Start()
    {
        existTime = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        existTime -= Time.deltaTime;

        if (existTime <= 0.0f)
        {
            Destroy(this.gameObject);
        }
    }
}
