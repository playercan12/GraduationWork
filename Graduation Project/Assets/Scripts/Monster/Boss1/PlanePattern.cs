﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlanePattern : ObjManager
{
    public int damage = 30;
    public GameObject[] planes;
    public GameObject[] corePoints;
    public GameObject core;

    public Vector3 corePosition;
    public int coreRandPos;

    public Vector3 target;

    BossMonster boss1;

    public AudioSource audio;

    // Update is called once per frame
    void Start()
    {
        // Server
        boss1 = GameObject.FindWithTag("Boss1").GetComponent<BossMonster>();
        coreRandPos = boss1.getRandWall();
        corePosition = corePoints[coreRandPos].transform.position;
        Instantiate(core, corePosition, Quaternion.identity).transform.parent = corePoints[coreRandPos].transform.parent.transform;

        //coreRandPos = Random.Range(0, corePoints.Length);
        //corePosition = corePoints[coreRandPos].transform.position;
        //Instantiate(core, corePosition, Quaternion.identity).transform.parent = corePoints[coreRandPos].transform.parent.transform;
    }

    void Update()
    {
        if (!audio.isPlaying)
        {
            audio.Play();
            audio.loop = true;
        }
        
        transform.position = Vector3.MoveTowards(transform.position, target, 5.0f * Time.deltaTime);

        if (transform.position == target)
        {
            Destroy(this.gameObject);
        }

    }
}
