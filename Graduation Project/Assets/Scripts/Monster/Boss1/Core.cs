﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Core : ObjManager
{
    BossMonster boss1;
    int coreCount;

    private Transform _parent;
    // Start is called before the first frame update
    void Start()
    {
        _parent = transform.parent;
        transform.parent = null;
        transform.localScale = new Vector3(1f, 4.5f, 1f);
        transform.parent = _parent.parent;
        transform.localRotation = Quaternion.Euler(-90, 0, 0);
        MaxHP = 90;
        HP = MaxHP;

        // Server
        boss1 = GameObject.FindWithTag("Boss1").GetComponent<BossMonster>();
        coreCount = boss1.getCoreCount();
        id = 5097508 + coreCount;
        s_HP = 90;
        s_MaxHP = 90;
    }

    // Update is called once per frame
    void Update()
    {
        if (s_HP <= 0)
        {
            //Destroy(this.transform.parent.gameObject);
            Destroy(_parent.gameObject);
            Destroy(this.gameObject);
        }
    }
}
