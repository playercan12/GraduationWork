﻿using Google.Protobuf.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using Object = System.Object;
using Random = UnityEngine.Random;

public class BossMonster : MonsterManager
{
    public GameObject first_face;
    public GameObject second_face;
    public GameObject third_face;
    public GameObject fourth_face;

    public GameObject second_leftArm;
    public GameObject second_rightArm;
    public GameObject third_leftArm;
    public GameObject third_rightArm;

    public GameObject leftLeg;
    public GameObject rightLeg;

    // 보스 총알
    public GameObject bossBullet;

    // 전깃줄
    public GameObject electricWire;

    // 면 벽
    public GameObject planesWall;

    // 몬스터 소환 위치
    public Transform leftArmMonsterSpawnPoint;
    public Transform rightArmMonsterSpawnPoint;

    // 총알 발사 위치
    public Transform leftArmShootPoint;
    public Transform rightArmShootPoint;

    // 전깃줄 , 벽 공격 발사 위치
    public Transform electricWirePoint;
    public Transform planeWallPoint;


    // 소환할 몬스터 프리팹
    public GameObject[] enemyPrefabs;
    public GameObject portalPrefab;


    // 플레이어
    public GameObject Target;
    public Collider[] TargetsColls;

    // 랜덤으로 Targets[]에서 뽑아서 최종 타겟이 될거임.
    private Transform leftArmTarget;
    private Transform rightArmTarget;

    // 레이저 공격 타겟
    public Transform laserTarget;
    public Transform wallTarget;

    // 공격 텀
    public float attackTime;
    public float currentAttackTime;

    // 패턴2 플레이어 리스트
    List<GameObject> playerlist = new List<GameObject>();


    public Vector3 offset;


    public List<MonsterManager> spawnList = new List<MonsterManager>();
    // 보스 상태
    public bool isOperate;
    public int phase = 0;

    // 보스 랜덤 패턴
    public int randomPattern;

    public Material _headMat;
    public float headRot;
    public float eyeIntensity = 0;

    public float dead_timer = 0;

    public AudioSource boss1Audio;

    public AudioClip operSound;

    public AudioClip deadSound;
    //     블럭이 위에서부터 하나씩 떼어짐 - 단계별로
    //     떼질때마다 아래칸에 눈이 생김 - 색은 변하던 말던 
    //
    //     첫번째칸은 아직 미정
    //     두번째팔에서는 어그로 정하고 (제일 가까운 놈) 플레이어 위치 계산 후 그 위치로 에너지볼 발사. 
    //     세번째팔에서는 원거리랑 근거리 몬스터를 계속 소환 ( 최대 개수 정해놓고 그만큼 계속 소환 ) 텀 10초.
    //     마지막은 발만 있고 얘는 전깃줄을 쏘는데 이걸 플레이어가 점프로 피해야함.
    //
    //     체력 100 - 80퍼 사이는 몬스터 소환만   -   눈이 색 없음
    //     체력 80퍼 이하로는 투사체 공격  -   이때 깨니까 . 초록색
    //     체력 50퍼 이하로는 모든 패턴 다 나옴.
    //     체력 30퍼이하 되면 광폭화로 바뀜 모델만 있으면 플레인만 프리팹으로 교체해서 얼굴 보여줌  - 빨강
    //
    //     광폭화 됬을때 면으로된 공격을 하는데 가운데에는 코어가 박혀있음. 이걸 부셔서 일렬로 패턴을 피하는 방식임.


    // 생각해봐야 할 부분
    // 몬스터 최대 개수만큼 생성해야되는데 이건 어떻게 할까 ?
    // 보스몬스터 파츠별로 콜라이더와 리지드 바디를 넣어야되나 ?

    public static int coreCount = 0;

    public int getRandWall()
    {
        return RandWall;
    }

    public int getCoreCount()
    {
        return coreCount;
    }

    // Update is called once per frame
    void Start()
    {
        isOperate = false; // 플레이어를 찾아내면 isOperate
        isDead = false; // 처음이니까 안죽어있을거고

        this.MaxHP = 300;
        this.HP = MaxHP;
        this.armor = 100;
        // 몬스터 공격 시간
        currentAttackTime = attackTime;
        eyeIntensity = 0;
        _headMat.SetColor("Color_561C20F3", Color.white * eyeIntensity);

    }

    void Update()
    {
        if (!isDead)
        {
            if (!boss1Audio.isPlaying)
            {
                boss1Audio.clip = operSound;
                boss1Audio.loop = true;
                boss1Audio.Play();
            }
                
            CalcPhase();

            switch (Pattern)
            {
                case Pattern.One:
                    phaseFirstPattern();
                    Pattern = Pattern.No;
                    break;

                case Pattern.Two:
                    phaseSecondPattern();
                    Pattern = Pattern.No;
                    break;

                case Pattern.Three:
                    phaseThirdPattern();
                    Pattern = Pattern.No;
                    break;

                case Pattern.Four:
                    phaseFourthPattern();
                    Pattern = Pattern.No;
                    break;

                default:
                    return;
            }

            if (isDead)
            {
                if (!boss1Audio.isPlaying)
                {
                    boss1Audio.loop = false;
                    boss1Audio.clip = deadSound;
                    boss1Audio.Play();
                }
                    
                dead_timer += Time.deltaTime;
                if (dead_timer >= 3.0f)
                {
                    Destroy(this.gameObject);
                }
            }
        }        
    }

    private void CheckSpawnMonsterDead()
    {

        for (int i = spawnList.Count - 1; i >= 0; i--)
        {
            if (spawnList[i].isDead)
            {
                Debug.Log("dead");
                spawnList.Remove(spawnList[i]);

            }
        }
    }

    private void phaseFirstPattern()
    {
        Instantiate(portalPrefab, new Vector3(1407f, 225.979f, 4914.01f), Quaternion.identity);
        Instantiate(portalPrefab, new Vector3(1410f, 225.979f, 4904f), Quaternion.identity);
    }

    private void phaseSecondPattern()
    {
        DetectTarget();

        if (playerlist.Count > 0)
        {
            GameObject _Target = playerlist[0];

            for (int i = 0; i < playerlist.Count; ++i)
            {
                float dist1 = Vector3.Distance(gameObject.transform.position, playerlist[i].transform.position);
                float dist2 = Vector3.Distance(gameObject.transform.position, _Target.transform.position);

                if (dist1 > dist2)
                    _Target = playerlist[i];
            }

            Transform tPos = _Target.transform;


            BossMonsterBullet leftBullet = Instantiate(bossBullet, leftArmShootPoint.transform.position, leftArmShootPoint.transform.rotation).GetComponent<BossMonsterBullet>();
            BossMonsterBullet rightBullet = Instantiate(bossBullet, rightArmShootPoint.transform.position, rightArmShootPoint.transform.rotation).GetComponent<BossMonsterBullet>();

            leftBullet.SetTarget(tPos.position);
            rightBullet.SetTarget(tPos.position);
        }
    }

    private void phaseThirdPattern()
    {
        Laser laser = Instantiate(electricWire, electricWirePoint.transform.position, electricWirePoint.transform.rotation).GetComponent<Laser>();

        laser.target = laserTarget.position;
    }

    private void phaseFourthPattern()
    {
        PlanePattern pp = Instantiate(planesWall, planeWallPoint.transform.position, planeWallPoint.rotation).GetComponent<PlanePattern>();

        pp.target = wallTarget.position;
    }

    private void DetectTarget()
    {
        TargetsColls = Physics.OverlapSphere(transform.position, 50.0f);

        // 여기서 범위 안에 들어온 플레이어를 찾아서 Targets[] 에 append
        for (int i = 0; i < TargetsColls.Length; i++)
        {
            if (TargetsColls[i].tag == "Player")
            {
                playerlist.Add(TargetsColls[i].gameObject);
            }
        }
    }

    private void CalcPhase()
    {
        float hpPer = ((float)((float)this.s_HP / (float)this.s_MaxHP) * 100.0f);

        if (hpPer < 80.0f && hpPer >= 50.0f)
        {
            eyeIntensity = 50000;
            _headMat.SetColor("Color_561C20F3", Color.white * eyeIntensity);
        }
        // hp가 30% 이상 50% 미만일 때 = 3페이즈    ---->  third_face 분리 후 제거  투사체 공격하는 패턴
        if (hpPer < 50.0f && hpPer >= 30.0f)
        {
            if (headRot < 180)
            {
                first_face.transform.Rotate(new Vector3(0, 10, 0), Space.World);
                headRot += 10;
            }
        }
        // hp가 0% 초과 30% 미만일 때 = 4페이즈(광폭화)  ----> 0프로 이하되면 죽음   2,3 패턴 둘다
        if (hpPer < 30.0f && hpPer > 0.0f)
        {
            first_face.transform.Rotate(new Vector3(0, 30, 0), Space.World);
            //phase = 4;
        }
        // hp가 0이하일 때 ----> 죽자.
        if (hpPer <= 0.0f)
        {
            if (boss1Audio.isPlaying)
            {
                boss1Audio.Stop();
            }
            
            isDead = true;

            this.GetComponent<BoxCollider>().enabled = false;

            DestroyParts(first_face, 2.0f);
            DestroyParts(second_face, 2.0f);
            DestroyParts(third_face, 2.0f);

            if (fourth_face.GetComponent<BoxCollider>() == null)
            {
                fourth_face.AddComponent<BoxCollider>();
            }
            else
            {
                fourth_face.GetComponent<BoxCollider>().size = fourth_face.transform.localScale;
            }
        }
    }

    private void DestroyParts(GameObject parts, float time)
    {
        parts.transform.parent = this.transform;
        if (parts.GetComponent<Rigidbody>() == null)
        {
            parts.AddComponent<Rigidbody>();
            Rigidbody rb = parts.GetComponent<Rigidbody>();
            rb.GetComponent<Rigidbody>().AddForce(parts.transform.forward * -50.0f, ForceMode.Impulse);
            rb.GetComponent<Rigidbody>().AddForce(parts.transform.up * 50.0f, ForceMode.Impulse);
        }
        else
        {
            Rigidbody rb = parts.GetComponent<Rigidbody>();
            rb.GetComponent<Rigidbody>().AddForce(parts.transform.forward * -50.0f, ForceMode.Impulse);
            rb.GetComponent<Rigidbody>().AddForce(parts.transform.up * 50.0f, ForceMode.Impulse);
        }

        if (parts.GetComponent<BoxCollider>() == null)
        {
            parts.AddComponent<BoxCollider>();
        }
        else
        {
            parts.GetComponent<BoxCollider>().size = parts.transform.localScale;
        }
    }
}
