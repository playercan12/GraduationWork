﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class OhterPlayer : NoneControlObj
{
   [Tooltip("스피드 조정")]
   public float runSpeed;
    public float crouchSpeed;
    
    
    public float applySpeed;//걷기와 뛰기 함수를 두개만들지말고 이동하는것에 속도를 적용한다.

    [Tooltip("점프 조정")]
    [SerializeField]
    public float jumpForce = 5;
    
    [Tooltip("플레이어 상태 변수")]
    [HideInInspector]public bool isRun;
    [HideInInspector]public bool isGround = true;
    [HideInInspector]public bool isCrouch = false;
    [HideInInspector]public bool isNearCar = false;
    [HideInInspector] public AnimState state = AnimState.Idle;

    public bool isJump;
    
    
    [Tooltip("얼마나 앉을건지")]
    private float _crouchPosY;
    private float _originPosY;
    private float _applyCouchPosY;

   
    
    private CapsuleCollider _capsuleCollider;

    private RaycastHit _hitInfo;
    
    
  
    
    private IKGunGrab _gunGrab;

    //클라이밍 관련 스크립트
    public OtherPlayerClimb climb;
    public OtherPlayerClimbLadder climbL;

    
    [Tooltip("목 움직이기")]
    //private Transform targetTransform; //서버에서 현재 목위치를 전달 받아야함
    private Transform _playerSpineTransform;
    private Vector3 _spineDir = new Vector3(0,0,0);


    private bool _isGunEject = false; //총 꺼내는 애니메이션 반복을 막기 위한 변수

    public int prevWeaponType;
    
    //총 드는거는 ikGunGrab 스크립트의 isgrab 이 총 드는 스크립트인데  true가 되면 총을 잡음 이걸 이용해서 플레이어에서 isgrab이 true될 때 넘기고 서버에서  그걸 넘겨 받으면 될 것 같음 총 여러개 되었을 때는 enum으로 해서 바꿔주면 될거같음
    
    
    [SerializeField] private Vector3 _spineOffset = new Vector3(0, 0, 0);

    
    //사운드 관련
    public AudioSource playerAudioSource;
    public AudioSource moveAudioSource;

    public AudioClip walkSound;
    public AudioClip runSound;
    public AudioClip deadSound;
    public bool isdeadSoundPlayed;
    [HideInInspector] public enum AnimState //상태는 오브젝트마다 다를거같아서 따로 관리하는게 좋을것 같음
    {
        Idle,
        Walk,
        Run,
        Jump
    }

    private void Awake()
    {
        playerRb = gameObject.GetComponent<Rigidbody>();
        _capsuleCollider = gameObject.GetComponent<CapsuleCollider>();
        anim = gameObject.GetComponent<Animator>();
        _gunGrab = GetComponent<IKGunGrab>(); // isgrab 받아오기 위해서 받아옴
        weaponGunAnim = currentWeapon[currentWeaponType].GetComponent<Animation>();//애니메이션 변수 가져옴
        prevWeaponType = currentWeaponType;
    }

    // Start is called before the first frame update
    void Start()
    {
        speed = 3.0f * 2.27f;

        destPos = transform.position;//위치 조정을 여기서 해주면될듯함
        
        applySpeed = speed;
        runSpeed = speed * 3;
        
        
        if (anim)
        {
            _playerSpineTransform = anim.GetBoneTransform(HumanBodyBones.Spine); //spine bone transform받아오기
        }

       
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead && s_HP == 100)
        {
            Resurrection();
            isDead = false;

            IsEquiq = true;
        }

        if (s_HP <= 0)
        {
            Debug.Log("Player Dead");
            if (!isdeadSoundPlayed)
            {
                PlayerSFX(deadSound);
                isdeadSoundPlayed = true;
            }

            anim.SetBool("Dead", true);
            _capsuleCollider.direction = 2;
            isDead = true;

            _gunGrab.isGrabed = false;

            currentWeapon[s_GunType].SetActive(false);

            IsEquiq = false;
        }


        if (usingcarId <= 0)//차를 타고있지 않은 상태
        {
            Move();
            RotateObj();
            AnimUpdate();

            if (prevWeaponType != s_GunType)
            {
                currentWeapon[prevWeaponType].SetActive(false);
                myGun = currentWeapon[s_GunType].GetComponent<Gun>();
                weaponGunAnim = currentWeapon[s_GunType].GetComponent<Animation>();//애니메이션 변수 가져옴
                prevWeaponType = s_GunType;
                _isGunEject = false;
            }

            //if (IsJump)
            //    anim.SetTrigger("Jump");
            //else if (!IsJump)
            //    anim.ResetTrigger("Jump");

            if (IsEquiq)//총 꺼낼때 애니메이션 및 처리
            {
                currentWeapon[s_GunType].SetActive(true);
                _gunGrab.isGrabed = true;
                if (!_isGunEject)
                {
                    weaponGunAnim = currentWeapon[s_GunType].GetComponent<Animation>();//애니메이션 변수 가져옴

                    weaponGunAnim.Play("GunEject");
                    _isGunEject = true;
                }
            }
            else if (!IsEquiq)
            {
                _gunGrab.isGrabed = false;
                currentWeapon[s_GunType].SetActive(false);
                _isGunEject = false;
            }

            if (_gunGrab.isGrabed) //무기를 쥐고 있을 때 shoot 상태변수를 받아와서 이게 true이면 쏘는 애니메이션을 보여주도록하자
            {
                myGun.isShoot = IsShoot;
            }

            if (IsReload)
            {
                myGun.Reload();
                IsReload = false;
            }
        }

        else
        {
            Debug.Log("Other " + usingcarId);
            CarController.carList[usingcarId].setOtherCarControll(this);
        }
    }

    private void Jump()
    {
        //anim.SetTrigger("Jump");
        //isJump = false;
    }

    
    public void PlayerSFX(AudioClip ac)
    {
        playerAudioSource.clip = ac;
        playerAudioSource.PlayOneShot(playerAudioSource.clip);
    }
    private void AnimUpdate()
    {
        anim.SetFloat("MoveDirX", MoveDirX);
        anim.SetFloat("MoveDirZ", MoveDirZ);
        
        if (MoveDirX > 0 || MoveDirZ > 0)
        {
            if(MoveDirX>0.5||MoveDirZ>0.5)
                moveAudioSource.clip = runSound;
            else
            {
                moveAudioSource.clip = walkSound;

            }
            if (!moveAudioSource.isPlaying)
            {
                moveAudioSource.Play();
            }
        }
        else
        {
            if (moveAudioSource.isPlaying)
            {
                moveAudioSource.Stop();
            }
        }
    }

    private void RotateObj()
    {
        transform.rotation = Quaternion.Euler(0, Rotate, 0);
    }

    private void LateUpdate()
    {
        OperationBonRotate();
    }


    private void OperationBonRotate()
    {
        _spineDir = SpinePos;
        _playerSpineTransform.LookAt(_spineDir);
        _playerSpineTransform.rotation = _playerSpineTransform.rotation * Quaternion.Euler(_spineOffset); //상체 움직임 보정
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Lever"))
        {
            ClimbWall cw = other.GetComponentInChildren<ClimbWall>();
            ClimbLadderWall cl = other.GetComponentInChildren<ClimbLadderWall>();

            if (cw != null)
            {
                climb.targetTransforms = cw.BrickTransfroms;
            }

            if (cl != null)
            {
                climbL.targetLeftFootTransforms = cl.targetLeftFootTransforms;
                climbL.targetLeftHandTransforms = cl.targetLeftHandTransforms;
                climbL.targetRightFootTransforms = cl.targetRightFootTransforms;
                climbL.targetRightHandTransforms = cl.targetRightHandTransforms;
            }
        }
    }

    void Resurrection()
    {
        anim.SetBool("Dead", false);
        isdeadSoundPlayed = false;
        _capsuleCollider.direction = 1;

        currentWeapon[s_GunType].SetActive(false);
    }
}
