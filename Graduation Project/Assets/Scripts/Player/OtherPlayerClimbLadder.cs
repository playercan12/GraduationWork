﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherPlayerClimbLadder : MonoBehaviour
{
     private OhterPlayer _player;
    [SerializeField]
    private bool isUpMove =false;

    public IKCCD leftHand;

    public IKCCD rightHand;

    public IKCCD leftFoot;
    public IKCCD rightFoot;

    
    public Transform[] targetLeftHandTransforms;
    public Transform[] targetLeftFootTransforms;
    public Transform[] targetRightHandTransforms;
    public Transform[] targetRightFootTransforms;

    // Start is called before the first frame update
    void Start()
    {
        _player = gameObject.GetComponent<OhterPlayer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (_player.isClimbingLadder)
        if (_player.IsClimbLadder)
        {
            _player.playerRb.useGravity = false;
            if (targetLeftHandTransforms.Length != 0)
            {

                if (!leftHand.isIKOn)
                {
                    int index = 0;
                    float temp = (leftHand.transform.position - targetLeftHandTransforms[0].position).sqrMagnitude;
                    for (int i = 0; i < targetLeftHandTransforms.Length - 1; i++)
                    {
                        float dist = (leftHand.transform.position - targetLeftHandTransforms[i].position).sqrMagnitude;

                        if (temp > dist)
                        {
                            index = i;
                            temp = dist;
                        }
                    }


                    {
                        leftHand.SetTarget(targetLeftHandTransforms[index].position,
                            targetLeftHandTransforms[index].rotation);
                    }
                }
            }

            if (targetRightHandTransforms.Length != 0)
            {

                if (!rightHand.isIKOn)
                {
                    int index = 0;
                    float temp = (rightHand.transform.position - targetRightHandTransforms[0].position).sqrMagnitude;
                    for (int i = 0; i < targetRightHandTransforms.Length - 1; i++)
                    {
                        float dist = (rightHand.transform.position - targetRightHandTransforms[i].position)
                            .sqrMagnitude;

                        if (temp > dist)
                        {
                            index = i;
                            temp = dist;
                        }
                    }

                    {
                        rightHand.SetTarget(targetRightHandTransforms[index].position,
                            targetRightHandTransforms[index].rotation);
                    }

                }
            }

            if (targetLeftFootTransforms.Length != 0)
            {

                
                {
                    int index = 0;
                    float temp = (leftFoot.transform.position - targetLeftFootTransforms[0].position).sqrMagnitude;
                    for (int i = 0; i < targetLeftFootTransforms.Length - 1; i++)
                    {
                        float dist = (leftFoot.transform.position - targetLeftFootTransforms[i].position).sqrMagnitude;

                        if (temp > dist)
                        {
                            index = i;
                            temp = dist;
                        }
                    }

                    if (leftFoot.targetPos != Vector3.zero)
                    {

                        Vector3 targetPos = Vector3.Lerp(leftFoot.targetPos, targetLeftFootTransforms[index].position,
                            Time.deltaTime * 5);
                        leftFoot.SetTarget(targetPos, targetLeftFootTransforms[index].rotation);

                    }
                    else
                    {
                        leftFoot.SetTarget(targetLeftFootTransforms[index].position,
                            targetLeftFootTransforms[index].rotation);
                    }
                }
            }

            if (targetRightFootTransforms.Length != 0)
            {
                
                {
                    int index = 0;
                    float temp = (rightFoot.transform.position - targetRightFootTransforms[0].position).sqrMagnitude;
                    for (int i = 0; i < targetRightFootTransforms.Length - 1; i++)
                    {
                        float dist = (rightFoot.transform.position - targetRightFootTransforms[i].position)
                            .sqrMagnitude;

                        if (temp > dist)
                        {
                            index = i;
                            temp = dist;
                        }
                    }

                    if (rightFoot.targetPos != Vector3.zero)
                    {
                        Vector3 targetPos = Vector3.Lerp(rightFoot.targetPos, targetRightFootTransforms[index].position,
                            Time.deltaTime * 5);
                        rightFoot.SetTarget(targetPos, targetRightFootTransforms[index].rotation);
                    }
                    else
                    {
                        rightFoot.SetTarget(targetRightFootTransforms[index].position,
                            targetRightFootTransforms[index].rotation);
                    }
                }
            }
        }
        //else
        //{
        //    _player.playerRb.useGravity = true;
        //}
    }
    
    
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Lever"))
        {
            
            _player.isClimbingLadder = false;
            _player.playerRb.useGravity = true;
            targetLeftFootTransforms = new Transform[0];
            targetRightHandTransforms = new Transform[0];
            targetRightFootTransforms = new Transform[0];
            targetLeftHandTransforms = new Transform[0];

            leftHand.targetPos = new Vector3();
            rightHand.targetPos = new Vector3();
            leftFoot.targetPos = new Vector3();
            rightFoot.targetPos = new Vector3();

           
            
        }
        
    }

   
}
