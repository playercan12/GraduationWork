﻿using Google.Protobuf.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TMPro;
//using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;

public class Player : AnimationObj
{
    private static Player instance = null;
    
    [Tooltip("스피드 조정")]
    
    public float runSpeed;
    public float crouchSpeed;
    
    
    public float applySpeed;//걷기와 뛰기 함수를 두개만들지말고 이동하는것에 속도를 적용한다.

    [Tooltip("점프 조정")]
    [SerializeField]
    public float jumpForce;
    
    [Tooltip("플레이어 상태 변수")]
    [HideInInspector]public bool isRun;
    public bool isGround = true;
    [HideInInspector]public bool isCrouch = false;
    [HideInInspector]public bool isInteract = false;
    [HideInInspector] public bool isJump = false;
    [HideInInspector] public bool onInteractKey = false;

    [Tooltip("상호작용 버튼 쿨다운")]
    [SerializeField]
    private float _interactCoolDown = 0.0f;

    public bool isOnLift = false;
    
    // [Tooltip("얼마나 앉을건지")]
    // private float _crouchPosY;
    // private float _originPosY;
    // private float _applyCouchPosY;

    public float sensitivity = 15;
    

    private CapsuleCollider _capsuleCollider;
    private BoxCollider _boxCollider;
    public SphereCollider camCol;
    
    
    private RaycastHit _hitInfo;
    
    
    
    

    private IKGunGrab _gunGrab;
    public Climb climb;
    public ClimbLadder climbL;
    
    [Tooltip("목 움직이기")]
    private Transform _cameraTansform;
    private Transform _playerSpineTransform;
    private Vector3 _spineDir = new Vector3(0,0,0);

    [SerializeField]
    private bool isUpMove = false;


    public IKCCD leftHandIKCCD;
    public Camera myCam;
    public Camera SubCam;
    
    [SerializeField] private Vector3 _neckOffset = new Vector3(0, 0, 0);

    public GameObject CenterUI;

    private float invincibilityTime = 0.0f;
    public Image hitImage;

    //부활을 위한 게이지 변수
    public float resurrectionGage = 0.0f;

    public Slider ResurrectionBar;

    private Vector3 oldPos;
    [HideInInspector]public Vector3 vel;
    private enum PlayerState
    {
        Idle,
        Walk,
        Run,
        Jump
    }

    [Tooltip("키입력 변수")] 
    [HideInInspector] public bool w_keyPress = false;
    [HideInInspector] public bool a_keyPress = false;
    [HideInInspector] public bool s_keyPress = false;
    [HideInInspector] public bool d_keyPress = false;
    private StateMachine _stateMachine;
    
    private Dictionary<PlayerState, IState> _stateDic = new Dictionary<PlayerState,IState>(); // 상태를 보관할 딕션어리

    public Transform rayCastPoint;


    public TextMeshProUGUI hpText;
    public TextMeshProUGUI interactText;

    public Transform cheatPos1;
    public Transform cheatPos2;
    public Transform cheatPos3;

    
    
    private UIManager _uiManager;

    public bool isJumping = false;
    public bool enableJump = false; // 상호작용 트리거에 들어왔을때 점프가 되지않게 하기 위한 변수

    private InteractManager _IManager;

    public AudioSource myAudioSource;
    public AudioSource moveAudioSource;
    
    public AudioClip walkSound;
    public AudioClip runSound;
    public AudioClip hitSound;
    public AudioClip deadSound;
    
    public bool boss2Safety = false;
    private void Awake()
    {
        
        this.id = 1;//차 타는거 테스트
        if (instance == null)
        {
            instance = this;
            
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
        anim = gameObject.GetComponent<Animator>(); 
        playerRb = gameObject.GetComponent<Rigidbody>();
        _capsuleCollider = gameObject.GetComponent<CapsuleCollider>();
        _boxCollider = gameObject.GetComponent<BoxCollider>();
        _boxCollider.enabled = false;

        _gunGrab = GetComponentInChildren<IKGunGrab>();
        myCam.gameObject.GetComponent<CameraMove>().enabled = true; //갑자기 생긴 버그 때문에 고치기위해서 카메라무브 스크립트를 게임 시작하면 켜줌
        weaponGunAnim = currentWeapon[currentWeaponType].GetComponent<Animation>();
        myGun.bulletText.text = "Bullet  " +myGun. bulletCount.ToString() + " / " + myGun.maxBulletCount.ToString();
        isPlayer = true;
        cheatPos1 = GameObject.FindWithTag("deongunTransform").transform.GetChild(0).GetComponent<Transform>();
        cheatPos2 = GameObject.FindWithTag("deongunTransform").transform.GetChild(1).GetComponent<Transform>();
        cheatPos3 = GameObject.FindWithTag("deongunTransform").transform.GetChild(2).GetComponent<Transform>();

    }

    public static Player Instance
    {
        get
        {
            if (instance == null)
            {
                return null;
            }

            return instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        oldPos = transform.position; //속도를 구하기위해 위치받기
        
        //상태를 생성함 -> 새로운 상태가 생기면 추가하면됨
        IState idle = new StateIdle();
        IState walk = new StateWalk();
        IState run = new StateRun();
        IState jump = new StateJump();

        //상태를 딕션에 넣음
        
        _stateDic.Add(PlayerState.Idle,idle);
        _stateDic.Add(PlayerState.Walk, walk);
        _stateDic.Add(PlayerState.Run, run);
        _stateDic.Add(PlayerState.Jump, jump);
        
        _stateMachine = new StateMachine(idle);

        speed = 3.0f;
        
        MaxHP = 100;
        HP = MaxHP;
        applySpeed = speed;
        runSpeed = speed * 3;
        enableJump= true;
        if (anim)
        {
            _playerSpineTransform = anim.GetBoneTransform(HumanBodyBones.Spine); //spine bone transform받아오기
        }

        _cameraTansform = GetComponentInChildren<Camera>().transform;
        _uiManager = UIManager.instance;
        _IManager = GameObject.Find("InteractManager").GetComponent<InteractManager>();

    }

    // Update is called once per frame
    void Update()
    {
        if (usingcarId <= 0 && !isClimbing && !isClimbingLadder)
        {

            KeyboardInput();
            if (!isDead)
                Interact();
        }

        else if (isClimbing || isClimbingLadder)
        {
            Climbing();
        }

        if (_interactCoolDown > 0.0f)
        {
            _interactCoolDown -= Time.deltaTime;
            if (transform.rotation.z != 0)
            {
                Debug.Log("Get up!!");

                transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
                
            }
        }

        hpText.text = "HP  " + ((s_HP).ToString() + " / " + (s_MaxHP).ToString()); 
        Debug.DrawRay(transform.position, Vector3.down*(_capsuleCollider.bounds.extents.y/5.0f), Color.blue);

        if (invincibilityTime > 0)
        {
            invincibilityTime -= Time.deltaTime;
            var tempColor = hitImage.color;
            tempColor.a = invincibilityTime;
            hitImage.color = tempColor;
        }

        if (s_HP <=0 && Attack == 1000)
        {
            if (!isDead)
            {
                Dead();
            }
        }

        SendMovePacket();
        SendAttackPacket();
    }

    protected override void Dead()
    {
        PlayerSFX(deadSound);
        anim.SetBool("Dead", true);
        _capsuleCollider.direction = 2;//콜라이더 방향
        _boxCollider.enabled = true;
        camCol.enabled = false;
        isDead = true;
        _gunGrab.isGrabed = false;

        currentWeapon[currentWeaponType].SetActive(false);

        myGun.bulletText.gameObject.SetActive(false);
    }

    public void PlayerSFX(AudioClip ac)
    {
        myAudioSource.clip = ac;
        myAudioSource.PlayOneShot(myAudioSource.clip);
    }
    void Resurrection()
    {
        anim.SetBool("Dead", false);
        invincibilityTime = 1.0f;

        _capsuleCollider.direction = 1;
        _boxCollider.enabled = false;

        camCol.enabled = true;
        isDead = false;


        currentWeapon[currentWeaponType].SetActive(false);

        myGun.bulletText.gameObject.SetActive(true);

        {
            s_Damage -= 100;

            C_ChangeStat Change_packet = new C_ChangeStat();
            Change_packet.ObjectId = id;
            Change_packet.StatInfo = StatInfo;
            Manager.Network.Send(Change_packet);
        }

        // 죽었을 때 Attack == 1000을 받는데 부활하면 다시 1로 바꿔준다
        Attack = 1;
    }

    void SendMovePacket()
    {
        if (_updated)
        {
            C_Move movePacket = new C_Move();
            movePacket.PosInfo = PosInfo;
            Manager.Network.Send(movePacket);
            _updated = false;
        }
    }

    void SendAttackPacket()
    {
        if (_updatedAttack)
        {
            C_AttackState attackPacket = new C_AttackState();
            attackPacket.AttackInfo = AttackInfo;
            Manager.Network.Send(attackPacket);
            _updatedAttack = false;
        }
    }

    private void FixedUpdate()//물리적인 충돌을 계산하기위해서 움직임등을 모두 fixedupdate에 넣음 이 update는 매 프레임마다 불림
    {
        if (usingcarId <= 0 && !isDead)
        {
            IsGround();

            if (!isClimbing && !isClimbingLadder)
            {
                _stateMachine.ExecuteUpdate();
                CharacterRotation();
                Move();
            }

            SendMovePacket();
        }

    }

    private void LateUpdate()
    {
        if (!isDead&& !isClimbing&&!isClimbing)
        {
            OperationBonRotate();
        }

        SendMovePacket();
    }

    private void OperationBonRotate()
    {
        _spineDir = _cameraTansform.position + _cameraTansform.forward * 50;
        SpinePos = _spineDir; // Server

        _playerSpineTransform.LookAt(_spineDir);
        _playerSpineTransform.rotation = _playerSpineTransform.rotation * Quaternion.Euler(_neckOffset); //상체 움직임 보정
    }

    void KeyboardInput()//키입력처리
    {
        _stateMachine.SetState(_stateDic[PlayerState.Idle]);
        
        w_keyPress = false;
        a_keyPress = false;
        s_keyPress = false;
        d_keyPress = false;
    
        
        w_keyPress = Input.GetKey(KeyCode.W);
        a_keyPress = Input.GetKey(KeyCode.A);
        s_keyPress = Input.GetKey(KeyCode.S);
        d_keyPress = Input.GetKey(KeyCode.D);


        
        if (!isDead)
        {
            if (w_keyPress || a_keyPress || s_keyPress || d_keyPress)
            {
                if (!isJumping&&enableJump)
                    _stateMachine.SetState(_stateDic[PlayerState.Walk]);

                if (Input.GetKey(KeyCode.LeftShift))
                {
                    if (!isJumping&&enableJump)
                    {
                        _stateMachine.SetState(_stateDic[PlayerState.Run]);
                    }

                }
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _stateMachine.SetState(_stateDic[PlayerState.Jump]);
                IsJump = true;
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                IsJump = false;
            }

            if (Input.GetKeyDown(KeyCode.F)&&_interactCoolDown<=0)
            {
                onInteractKey = true;
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
               ChangeWeapon(0);//총기 넘버 0 AR

            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
               ChangeWeapon(1); //총기 넘버 1 SMG
            }

            if (Input.GetKeyDown(KeyCode.Alpha4))
            {           
                isEquipWeapon = false; //이 변수를 보내자
                IsEquiq = isEquipWeapon; //Server

                _gunGrab.isGrabed = false;

                currentWeapon[currentWeaponType].SetActive(false);

                myGun.bulletText.gameObject.SetActive(false);
                
                _uiManager.centerPoint.SetActive(false);
            }

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                applySpeed = speed / 2.0f;
            }

            if (Input.GetKeyUp(KeyCode.LeftControl))
            {
                applySpeed = speed;
            }

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                isShoot = true;//여기서 총쏘고 있다는 것을 샌드하자
                myGun.isShoot = isShoot;
                IsShoot = isShoot; // Server
            }

            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                isShoot = false;//여기서 총쏘고 있다는 것을 샌드하자
                myGun.isShoot = isShoot;
                IsShoot = isShoot; // Server
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                if (_gunGrab.isGrabed)
                {
                    myGun.Reload();
                    IsReload = true;
                }
            }

            if (Input.GetKeyUp(KeyCode.R))
            {
                IsReload = false;
            }
            

            if (Input.GetKeyDown(KeyCode.O))
            {
                HP = MaxHP;
            }

           
        }


        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
        {
            dirZ = 0;
        }

        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            dirX = 0;
        }
        
        if (w_keyPress && s_keyPress)
        {
            dirZ = 0;
        }
       
        if (a_keyPress && d_keyPress)
        {
            dirX = 0;
        }

        MoveDirX = dirX;
        MoveDirZ = dirZ;
        CellPos = transform.position;
    }

    public void ChangeWeapon(int weaponType)
    {
        if (weaponType == s_GunType && IsEquiq == true)
            return;

        isEquipWeapon = true;
        IsEquiq = isEquipWeapon; //Server

        currentWeapon[currentWeaponType].SetActive(false);//현재 장비하고있는총을 숨김
        currentWeaponType = weaponType;

        s_GunType = currentWeaponType; // Server

        myGun = currentWeapon[currentWeaponType].GetComponent<Gun>();
        weaponGunAnim = currentWeapon[currentWeaponType].GetComponent<Animation>();
        myGun.bulletText.gameObject.SetActive(true);//ui setActive
        currentWeapon[currentWeaponType].SetActive(true);//입력된 총의 타입을 활성화
        _gunGrab.isGrabed = true;
        weaponGunAnim.Play("GunEject");
        myGun.currentFireRate = 1.0f;
        _uiManager.centerPoint.SetActive(true);
    }

    protected override void Move()
    {

        Vector3 _moveHorizontal = transform.right * dirX;
        Vector3 _moveVertical = transform.forward * dirZ;

        Vector3 _velocity = (_moveHorizontal + _moveVertical).normalized *applySpeed;

        vel = Vector3.zero;
        //playerRb.velocity = _velocity;
        Vector3 currentPos = transform.position;
        //playerRb.MovePosition(transform.position + _velocity * Time.deltaTime);
        playerRb.position += _velocity * Time.deltaTime;

        Vector3 distance = currentPos - oldPos;
        vel = distance / Time.deltaTime;

        vel.x = Mathf.Clamp(vel.x, -7.0f, 7.0f);
        vel.z = Mathf.Clamp(vel.z, -7.0f, 7.0f);
        
        
        
        oldPos = currentPos;

        if (dirX > 0 || dirZ > 0)
        {
            if(applySpeed<runSpeed)
                moveAudioSource.clip = walkSound;
            else
            {
                moveAudioSource.clip = runSound;

            }
            if (!moveAudioSource.isPlaying)
            {
                moveAudioSource.Play();
            }
        }
        else
        {
            if (moveAudioSource.isPlaying)
            {
                moveAudioSource.Stop();
            }
        }
        
        
    }

  
    public void hit(int damage)
    {
        if (invincibilityTime <= 0 &&!isDead)
        {
            //HP -= damage;
            //Debug.Log(transform.name + ":" + HP);
            invincibilityTime = 1.0f;
            PlayerSFX(hitSound);
            {
                s_Damage = damage;
                C_ChangeStat Change_packet = new C_ChangeStat();
                Change_packet.ObjectId = id;
                Change_packet.StatInfo = StatInfo;
                Manager.Network.Send(Change_packet);
            }
        }
    }
    
    
    private void CharacterRotation()
    {
        float _yRotation = Input.GetAxisRaw("Mouse X");
        Vector3 _charcterRotationY = new Vector3(0f, _yRotation, 0f) * sensitivity;
        playerRb.MoveRotation(playerRb.rotation*Quaternion.Euler(_charcterRotationY));

        Rotate = transform.rotation.eulerAngles.y; // Server
    }

    private void IsGround()
    {
        
        isGround = Physics.Raycast(transform.position, Vector3.down, _capsuleCollider.bounds.extents.y/5.0f);
        if ( vel.y>0.0f )
        {
            isJumping = true;
        }
        else
        {
            isJumping = false;
        }
        if(isGround)
        {
            isJumping = false;
        }
        isJump = true;
        if (isGround && !isJump)
        {
            anim.ResetTrigger("Jump");
        }
        
    }
  

  

    private void Interact()
    {
        int layerMask = (-1) - (1 << LayerMask.NameToLayer("PlayerCamera"));
        isInteract = Physics.Raycast(myCam.transform.position,  myCam.transform.forward,out _hitInfo,_capsuleCollider.bounds.extents.z + 1f,layerMask);
        Debug.DrawRay(myCam.transform.position,  myCam.transform.forward,Color.blue);
        
        if (isInteract)
        {
            CarController car = _hitInfo.transform.GetComponent<CarController>();
            InteractiveButton ib = _hitInfo.transform.GetComponent<InteractiveButton>();
            InteractiveDoubleButton idb = _hitInfo.transform.GetComponent<InteractiveDoubleButton>();
            InteractiveLiftButton ilb = _hitInfo.transform.GetComponent<InteractiveLiftButton>();
            InteractiveNonAnimButton inab = _hitInfo.transform.GetComponent<InteractiveNonAnimButton>();
            ClimbWall cw = _hitInfo.transform.GetComponent<ClimbWall>();
            ClimbLadderWall cl = _hitInfo.transform.GetComponent<ClimbLadderWall>();

            if (car != null && usingcarId <= 0)
            {
                if (onInteractKey)
                {
                    if (car.carplayerId_test <= 0)
                    {
                        carId_test = car.id;
                        carplayerId_test = this.id;

                        usingcarId = car.id;

                        Debug.Log("Player ID : " + carplayerId_test + " Car ID :  " + carId_test);

                        interactText.text = " ";

                        CenterUI.SetActive(false);
                        _capsuleCollider.enabled = false;
                        playerRb.isKinematic = true;
                        car.setCarControll(this);
                        onInteractKey = false;
                    }
                }
                else
                {
                    if (car.carplayerId_test <= 0)
                        interactText.text = "Interact Key 'F'";
                    else
                        interactText.text = " ";
                }
            }

            if (ib != null)
            {
                if (onInteractKey)
                {
                    _IManager.SetButton(ib.buttonId);
                    _interactCoolDown = 1.0f;
                    onInteractKey = false;
                }

                else
                {
                    interactText.text = "Interact Key 'F'";
                    leftHandIKCCD.targetPos = ib.transform.position;
                }
            }

            if (idb != null )
            {
                if (onInteractKey)
                {
                    _IManager.SetButton(idb.buttonId);
                    _interactCoolDown = 1.0f;
                    onInteractKey = false;
                }
                else
                {
                    interactText.text = "Interact Key 'F'";
                    //Debug.Log(_hitInfo.transform.name);
                }
            }

            if (ilb != null )
            {
                if (onInteractKey)
                {
                    _IManager.SetButton(ilb.buttonId);
                    _interactCoolDown = 1.0f;
                    onInteractKey = false;
                }
                else
                {
                    interactText.text = "Interact Key 'F'";
                    //Debug.Log(_hitInfo.transform.name);
                }
            }

            if (cw != null)
            {
                if (onInteractKey)
                {
                    interactText.text = " ";

                    transform.rotation = Quaternion.LookRotation(-_hitInfo.normal);
                    isClimbing = true;
                    IsClimb = isClimbing; // Server
                    playerRb.velocity = Vector3.zero;
                    playerRb.useGravity = false;
                    dirX = 0;
                    dirZ = 0;
                    climb.targetTransforms = cw.BrickTransfroms;
                    anim.SetFloat("MoveDirZ", dirZ);
                    anim.SetFloat("MoveDirX", dirX);

                    climb.enabled = true;

                }
                else
                {
                    interactText.text = "Interact Key 'F'";
                    Debug.Log(_hitInfo.transform.name);
                }
            }

            if (cl != null)
            {
                if (onInteractKey)
                {
                    interactText.text = " ";

                    transform.rotation = Quaternion.LookRotation(-_hitInfo.normal);
                    isClimbingLadder = true;
                    IsClimbLadder = isClimbingLadder; // Server
                    playerRb.velocity = Vector3.zero;
                    playerRb.useGravity = false;
                    dirX = 0;
                    dirZ = 0;

                    climbL.targetLeftFootTransforms = cl.targetLeftFootTransforms;
                    climbL.targetLeftHandTransforms = cl.targetLeftHandTransforms;
                    climbL.targetRightFootTransforms = cl.targetRightFootTransforms;
                    climbL.targetRightHandTransforms = cl.targetRightHandTransforms;

                    anim.SetFloat("MoveDirZ", dirZ);
                    anim.SetFloat("MoveDirX", dirX);

                    climbL.enabled = true;

                }
                else
                {
                    interactText.text = "Interact Key 'F'";
                    Debug.Log(_hitInfo.transform.name);
                }
            }

            if (inab != null)
            {
                if (onInteractKey)
                {
                    _IManager.SetButton(inab.buttonId);
                    _interactCoolDown = 1.0f;
                    onInteractKey = false;
                }
                else
                {
                    interactText.text = "Interact Key 'F'";
                    //Debug.Log(_hitInfo.transform.name);
                }
            }
        }
        else
        {
            interactText.text = " ";
        }
        onInteractKey = false;
    }

    void Climbing()
    {

        Vector3 _moveHorizontal = transform.right * Input.GetAxis("Horizontal");
        Vector3 _moveVertical = transform.up * Input.GetAxis("Vertical");

        Vector3 _velocity = (_moveHorizontal + _moveVertical).normalized * speed / 7;

        playerRb.position += _velocity * Time.deltaTime;


        if (isGround && isUpMove && Input.GetKey(KeyCode.S))
        {
            isClimbing = false;
            isClimbingLadder = false;

            IsClimb = isClimbing;
            IsClimbLadder = isClimbingLadder;

            playerRb.useGravity = true;

            isUpMove = false;
        }

        if (Input.GetKey(KeyCode.W))
        {
            isUpMove = true;
        }

        CellPos = transform.position;
    }

    public void TakeoffCar()
    {
        CenterUI.SetActive(true);
        _interactCoolDown = 1.0f;
        _capsuleCollider.enabled = true;
        playerRb.isKinematic = false;
        dirX = 0;
        dirZ = 0;
        myCam.gameObject.SetActive(true);
        //rideCarID = -1; // 차를 내릴때 불리는 함수에서 rideCarID를 디폴트값으로 수정한다.
        usingcarId = -1;

        SendMovePacket();
    }

    private void OnTriggerEnter(Collider other)
    {
        // 근접 몬스터 충돌1
        if (other.CompareTag("CollideCM"))
        {
            hit(10);
        }

        // 근접 몬스터 충돌2
        if (other.CompareTag("CollideCM1"))
        {
            hit(10);
        }

        // 원거리 몬스터 충돌
        if (other.CompareTag("CollideRM"))
        {
            hit(10);
        }

        // 드론 몬스터 미사일 충돌
        if (other.CompareTag("CollideMissile"))
        {
            hit(10);
        }

        // 보스1 몬스터 벽 패턴 충돌
        if (other.CompareTag("CollideBoss1Plane"))
        {
            hit(10);
        }

        // 보스1 몬스터 레이저 패턴 충돌
        if (other.CompareTag("CollideBoss1Laser"))
        {
            hit(10);
        }

        // 보스1 몬스터 총알 충돌
        if (other.CompareTag("CollideBoss1Bullet"))
        {
            hit(10);
        }


        if (other.CompareTag("CollideBoss2Bullet"))
        {
            hit(10);
        }

        if (other.CompareTag("CollideBoss2Laser"))
        {
            hit(10);
        }

        // 보스2 패턴4 공격으로부터 안전한 위치
        if (other.CompareTag("SafetyZone"))
        {
            boss2Safety = true;
        }

        if (other.CompareTag("CollideBoss3Missile"))
        {
            hit(10);
        }

        if (other.CompareTag("Lever"))
        {
            myCam.gameObject.SetActive(false);
            SubCam.gameObject.SetActive(true);
        }

        if (other.CompareTag("CollideBoss3PushTrigger"))
        {
            hit(10);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Trap"))
        {
            hit(10);
        }

        if (other.CompareTag("CollideBoss2FloorAttack"))
        {
            if (!boss2Safety)
            {
                hit(5);
            }
        }

        if (other.CompareTag("Player"))
        {
            if (isDead)
            {
                interactText.text = "Resurrection key 'B'";

                if (Input.GetKey(KeyCode.B))//브활을 위한 키코드
                {
                    ResurrectionBar.gameObject.SetActive(true);

                    resurrectionGage += 1.0f;
                    ResurrectionBar.value = resurrectionGage;
                    interactText.text = "being resurrected...";

                    if (resurrectionGage >= 100)
                    {
                        Resurrection();

                        resurrectionGage = 0;
                        ResurrectionBar.value = resurrectionGage;

                        ResurrectionBar.gameObject.SetActive(false);

                        //isDead = false;
                    }

                }

                if (Input.GetKeyUp(KeyCode.B))
                {
                    interactText.text = " ";
                    resurrectionGage = 0;
                    ResurrectionBar.value = resurrectionGage;

                    ResurrectionBar.gameObject.SetActive(false);


                    Debug.Log("게이지 없어짐 " + resurrectionGage);

                }
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Lever"))
        {
            myCam.gameObject.SetActive(true);


            SubCam.gameObject.SetActive(false);
            isClimbing = false;
            isClimbingLadder = false;
            IsClimb = isClimbing; // Server
            IsClimbLadder = isClimbingLadder; // Server

            playerRb.useGravity = true;
        }

        if (other.CompareTag("SafetyZone"))
        {
            boss2Safety = false;
        }
        if (other.CompareTag("Player"))
        {
            interactText.text = " ";
            resurrectionGage = 0;
            ResurrectionBar.value = resurrectionGage;

            ResurrectionBar.gameObject.SetActive(false);

        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("CollideBoss3Laser"))
        {
            hit(10);
        }
    }

}
