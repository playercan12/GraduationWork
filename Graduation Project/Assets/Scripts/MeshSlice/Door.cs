﻿using Google.Protobuf.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class Door : ObjManager
{
    public GameObject[] monsters;
    public Vector3 slicePos;

    [SerializeField]
    public SliceManager sliceManager;

    public AudioSource audio;

    private void Start()
    {
        //DontDestroyOnLoad(this.gameObject);

        MaxHP = 150;
        HP = MaxHP;
        sliceManager = FindObjectOfType<SliceManager>();
    }

    private void Update()
    {
        if (s_HP <= 30.0f)
        {
            sliceManager.SliceObject = this.gameObject;
            sliceManager.SlicePos = new Vector3((int)StatInfo.TotalExp, (int)StatInfo.Speed, (int)StatInfo.Level);
            sliceManager.insideMaterial = GetComponent<MeshRenderer>().materials.ElementAt(0);
            sliceManager.isSlice = true;

            if (!audio.isPlaying)
            {
                audio.Play();
            }
        }
    }

    //private void OnDestroy()
    //{
    //    for (int i = 0; i < monsters.Length; i++)
    //    {
    //        monsters[i].SetActive(true);
    //    }
    //}
}
