﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheatTeleport : MonoBehaviour
{
    public Transform[] cheatPos;

    public int index;
    // Start is called before the first frame update
    void Start()
    {
       cheatPos = GameObject.Find("CheatPos").GetComponent<CheatPos>().cheatPositions;

    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.F9))
        {
            if (index > 0)
            {
                index--;
                transform.position = cheatPos[index].transform.position;
            }

        }
        
        if (Input.GetKeyDown(KeyCode.F10))
        {
            if (index < cheatPos.Length-1)
            {
                index++;
                transform.position = cheatPos[index].transform.position;
            }

        }
    }

   
}
