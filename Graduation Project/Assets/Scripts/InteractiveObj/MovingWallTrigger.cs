﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWallTrigger : MonoBehaviour
{
    public MovingWall[] movingWalls;

    public bool isOn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isOn)
        {
            if (other.CompareTag("Player"))
            {
                foreach (var mw in movingWalls)
                {
                    mw.isOn = true;
                }

                isOn = true;
            }
        }
    }
}
