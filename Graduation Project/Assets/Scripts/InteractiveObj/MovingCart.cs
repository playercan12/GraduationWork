﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCart : MonoBehaviour
{
    public float speed = 5.0f;

    public bool onPlayer = false;

    public Transform startPos;
    public Transform endPos;

    private Vector3 forwardDirection;
    private Vector3 backDirection;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        forwardDirection = (endPos.position - transform.position).normalized;
        backDirection = (startPos.position - transform.position).normalized;
        if (onPlayer)
        {
            if (transform.position.z <= endPos.position.z)
            {
                transform.position += forwardDirection * speed/1.0f * Time.deltaTime;
            }
        }
        else
        {
            if (transform.position.z >= startPos.position.z)
            {

                transform.position += backDirection * speed/8.0f * Time.deltaTime;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.transform.parent = transform;
            onPlayer = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.transform.parent = null;
            onPlayer = false;

            
        }
    }
}
