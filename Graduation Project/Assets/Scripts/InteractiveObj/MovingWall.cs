﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour
{
    public float animTimer = 0.0f;

    private Animation _anim;

    public bool isOn = false;
    // Start is called before the first frame update
    void Start()
    {
        _anim = gameObject.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {

        if (!_anim.isPlaying && animTimer <0.0f)
        {
            _anim.Play();
        }

        if (animTimer >= 0.0f&&isOn)
        {
            animTimer -= Time.deltaTime;
        }
    }
}
