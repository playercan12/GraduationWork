﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour
{

    public Transform playerTransform;
    public Transform[] targetDir;
    public int index =0;

    private void Start()
    {
        targetDir[0] = GameObject.FindWithTag("deongunTransform").transform.GetChild(0).GetComponent<Transform>();
        targetDir[1] = GameObject.FindWithTag("deongunTransform").transform.GetChild(1).GetComponent<Transform>();
        targetDir[2] = GameObject.FindWithTag("deongunTransform").transform.GetChild(2).GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = (targetDir[index].position - playerTransform.position).normalized;
        Vector3 dirXY = new Vector3(dir.x, 0, dir.z);

       
        float rotAngle = Vector3.SignedAngle(dirXY, playerTransform.forward, Vector3.up);
       
       
        transform.eulerAngles = new Vector3(0, 0, rotAngle);

        if (Vector3.Distance(targetDir[index].position, playerTransform.position) < 3.0f)
        {
            index++;
        }
        
    }
}
