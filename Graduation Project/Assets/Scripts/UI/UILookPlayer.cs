﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UILookPlayer : MonoBehaviour
{

    private Player  _player;
    public OhterPlayer idPlayer;
    public TextMeshProUGUI idText;
    // Start is called before the first frame update
    void Start()
    {
        _player = Player.Instance;
        idText.text = idPlayer.name;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - _player.myCam.transform.position);
    }
}
