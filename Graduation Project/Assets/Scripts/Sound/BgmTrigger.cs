﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgmTrigger : MonoBehaviour
{
    public int bgmIndex; //0숲 //1동굴 //2 탑 //3 최종보스
    private SoundManager _soundManager;
    private void Start()
    {
        _soundManager = SoundManager.Instance;
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {

            if (other.gameObject.GetComponent<Player>() != null)
            {
                Debug.Log("asd");

                if(bgmIndex != _soundManager.GetBgmIndex())
                    _soundManager.PlayBGM(bgmIndex);
            }
        }
    }

  
}
