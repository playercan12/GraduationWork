﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    private static SoundManager _instance= null;

    public static SoundManager Instance 
    {
        get
        {
            if (_instance == null)
            {
                return null;
            }

            return _instance;
        }
       
    }

    private AudioSource _bgmAudio;

    public AudioClip[] gameBgm;
    private int _bgmIndex;
    

    // Start is called before the first frame update
    void Start()
    {
        if (_instance == null)
            _instance = this;
        
        DontDestroyOnLoad(this.gameObject);

        _bgmAudio = gameObject.GetComponent<AudioSource>();
        PlayBGM(0);
    }

    public void PlayBGM(int index)
    {
        _bgmIndex = index;
        _bgmAudio.loop = true;

        _bgmAudio.clip = gameBgm[_bgmIndex];
        _bgmAudio.Play();
    }

    public int GetBgmIndex()
    {
        return _bgmIndex;
    }
    
}
