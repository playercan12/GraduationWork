﻿using Google.Protobuf.Protocol;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.VFX;

public class Gun : ObjManager
{

    private const float FireRate = 0.1f;
    public float damage = 30;
    public float range = 300f;
    public float penetration = 50;
    public Camera fpsCam;

    public Animation gunAnim;

    public VisualEffect muzzleFlash;

    public float currentFireRate = FireRate;

    public int maxBulletCount = 40;
    public int bulletCount = 40;

    public bool isShoot =false;

    public TextMeshProUGUI bulletText;
    public bool isPlayer; //플레이어인지 구분

    public GameObject bulletMark;

    public GameObject markParticle;
    
    UIManager _uiManager;

    private Player _player;
    
    public GameObject gunMag;
    public GameObject cloneMag;
    private CameraMove retroCameraMove;

    private float reloadTime = 0f;
    // Start is called before the first frame update

    public AudioSource gunAudioSource;
    public AudioClip gunReloadSound1;
    public AudioClip gunReloadSound2;

    public AudioClip gunShotSound;
    
    public Material insideMaterial;

    private float loadTime = 0.01f;
    private int generations = 5;
    
    void Start()
    {
        gunAnim = GetComponent<Animation>();
        _uiManager = UIManager.instance;
        _player = Player.Instance;
        retroCameraMove = _player.myCam.GetComponent<CameraMove>();
    }

    // Update is called once per frame
    void Update()
    {
        GunFireRateCalc();

        if (isShoot && reloadTime<=0)
        {
            if (currentFireRate <= 0 &&bulletCount>0)
            {
                Shoot();
            }
        }

        if (isPlayer)
        {
            bulletText.text = "Bullet  " + bulletCount.ToString() + " / " + maxBulletCount.ToString();
        }
    }
    
    private void GunFireRateCalc()
    {
        if (currentFireRate > 0)
        {
            currentFireRate -= Time.deltaTime;
        }

        if (reloadTime > 0f)
        {
            reloadTime -= Time.deltaTime;
        }
    }

    public void PlayReloadSound1()//애니메이션 이벤트로 실행하기 위한 함수
    {
        gunAudioSource.clip = gunReloadSound1;
        gunAudioSource.PlayOneShot(gunAudioSource.clip);
    }
    
    public void PlayReloadSound2()//이하동문
    {
        gunAudioSource.clip = gunReloadSound1;
        gunAudioSource.PlayOneShot(gunAudioSource.clip);
    }
    private void Shoot()
    {
        if (!gunAnim.isPlaying)
        {
            gunAnim.Play("GunShot");
            gunAudioSource.clip = gunShotSound;//사운드 플레이 
            gunAudioSource.PlayOneShot(gunAudioSource.clip);//사운드플레이
        }

        muzzleFlash.SendEvent("OnPlay");

        if (isPlayer)
        {
            RaycastHit hit;
            int layerMask = (-1) - (1 << LayerMask.NameToLayer("PlayerCamera"));  // Everything에서 Player 레이어만 제외하고 충돌 체크함
            if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range,layerMask, QueryTriggerInteraction.Ignore))
            {
                GameObject mark =  Instantiate(bulletMark, hit.point+hit.normal*0.001f,Quaternion.identity,hit.transform);
                GameObject markP =  Instantiate(markParticle, hit.point+hit.normal*0.001f,Quaternion.identity,hit.transform);
                mark.transform.LookAt(hit.point+hit.normal);
                markP.transform.LookAt(hit.point+hit.normal);
                mark.transform.localScale = new Vector3(1/ hit.transform.localScale.x,
                    1 / hit.transform.localScale.x,
                    1 / hit.transform.localScale.x); 
                markP.transform.localScale = new Vector3(1/ hit.transform.localScale.x,
                    1 / hit.transform.localScale.x,
                    1 / hit.transform.localScale.x); 
                Destroy(mark, 5f);
                Destroy(markP, 1f);

                PatrolClosedMonster Pcmonster = hit.transform.GetComponent<PatrolClosedMonster>();
                PatrolRangedMonster Prmonster = hit.transform.GetComponent<PatrolRangedMonster>();
                PatrolDroneMonster Pdmonster = hit.transform.GetComponent<PatrolDroneMonster>();
                ClosedMonster cmonster = hit.transform.GetComponent<ClosedMonster>();
                RangedMonster rmonster = hit.transform.GetComponent<RangedMonster>();
                DroneMonster dmonster = hit.transform.GetComponent<DroneMonster>();

                BossMonster bossMonster1 = hit.transform.GetComponent<BossMonster>();
                BossMonster2 bossMonster2 = hit.transform.GetComponent<BossMonster2>();
                BossMonster3 bossMonster3 = hit.transform.GetComponent<BossMonster3>();
                
                Core planeCore = hit.transform.GetComponent<Core>();
                Door door = hit.transform.GetComponent<Door>();

                if (Pcmonster != null && !Pcmonster.isDead)
                {
                    s_Damage = (int)damage;

                    C_ChangeStat Change_packet = new C_ChangeStat();
                    Change_packet.ObjectId = Pcmonster.id;
                    Change_packet.StatInfo = StatInfo;
                    Manager.Network.Send(Change_packet);
                }

                if (Prmonster != null && !Prmonster.isDead)
                {
                    s_Damage = (int)damage;

                    C_ChangeStat Change_packet = new C_ChangeStat();
                    Change_packet.ObjectId = Prmonster.id;
                    Change_packet.StatInfo = StatInfo;
                    Manager.Network.Send(Change_packet);
                }

                if (Pdmonster != null && !Pdmonster.isDead)
                {
                    s_Damage = (int)damage;

                    C_ChangeStat Change_packet = new C_ChangeStat();
                    Change_packet.ObjectId = Pdmonster.id;
                    Change_packet.StatInfo = StatInfo;
                    Manager.Network.Send(Change_packet);
                }

                if (cmonster != null && !cmonster.isDead)
                {
                    s_Damage = (int)damage;

                    C_ChangeStat Change_packet = new C_ChangeStat();
                    Change_packet.ObjectId = cmonster.id;
                    Change_packet.StatInfo = StatInfo;
                    Manager.Network.Send(Change_packet);
                }

                if (rmonster != null && !rmonster.isDead)
                {
                    s_Damage = (int)damage;

                    C_ChangeStat Change_packet = new C_ChangeStat();
                    Change_packet.ObjectId = rmonster.id;
                    Change_packet.StatInfo = StatInfo;
                    Manager.Network.Send(Change_packet);
                }

                if (dmonster != null && !dmonster.isDead)
                {
                    s_Damage = (int)damage;

                    C_ChangeStat Change_packet = new C_ChangeStat();
                    Change_packet.ObjectId = dmonster.id;
                    Change_packet.StatInfo = StatInfo;
                    Manager.Network.Send(Change_packet);

                    Debug.Log("dmosnter HP : " + dmonster.s_HP);
                }

                if (bossMonster1 != null)
                {
                    s_Damage = (int)damage;

                    C_ChangeStat Change_packet = new C_ChangeStat();
                    Change_packet.ObjectId = bossMonster1.id;
                    Change_packet.StatInfo = StatInfo;
                    Manager.Network.Send(Change_packet);                    
                }

                if (bossMonster2 != null)
                {
                    if (bossMonster2.IsStartPattern)
                    {
                        s_Damage = (int)damage;

                        C_ChangeStat Change_packet = new C_ChangeStat();
                        Change_packet.ObjectId = bossMonster2.id;
                        Change_packet.StatInfo = StatInfo;
                        Manager.Network.Send(Change_packet);
                    }
                }

                if (bossMonster3 != null)
                {
                    s_Damage = (int)damage;

                    C_ChangeStat Change_packet = new C_ChangeStat();
                    Change_packet.ObjectId = bossMonster3.id;
                    Change_packet.StatInfo = StatInfo;
                    Manager.Network.Send(Change_packet);                    
                }

                if (planeCore != null)
                {
                    C_ChangeStat Change_packet = new C_ChangeStat();
                    planeCore.s_HP -= 30;
                    Change_packet.ObjectId = planeCore.id;
                    Change_packet.StatInfo = planeCore.StatInfo;

                    Manager.Network.Send(Change_packet);

                    Debug.Log("Core" + Change_packet);
                }

                if (door != null)
                {
                    //door.slicePos = hit.point;
                    //door.CellPos = door.slicePos;

                    door.StatInfo.TotalExp = (int)hit.point.x;
                    door.StatInfo.Speed = (int)hit.point.y;
                    door.StatInfo.Level = (int)hit.point.z;
                    door.StatInfo.Hp -= 30;

                    C_ChangeStat Change_packet = new C_ChangeStat();

                    Change_packet.ObjectId = door.id;
                    Change_packet.StatInfo = door.StatInfo;

                    Manager.Network.Send(Change_packet);
                }

            }
            retroCameraMove.HorizontalRetro();
            retroCameraMove.VerticalRetro();
            if (_uiManager.crossHairSize < 300.0f)
            {
                _uiManager.crossHairSize += 2400.0f * Time.deltaTime;
            }
            
            bulletCount -= 1;
        }
        currentFireRate = FireRate;        
    }

    //private IEnumerator ShatterObject(GameObject obj,Vector3 hitpoint,int gen)
    //{
    //    yield return new WaitForSeconds(Random.Range(loadTime, loadTime * 2));

    //    GameObject[] pieces = MeshManipulation.MeshCut.Cut(obj, /*obj.GetComponent<Collider>().bounds.center*/ hitpoint, GetAngle(obj,gen), insideMaterial);

    //    foreach (GameObject piece in pieces)
    //    {
    //        piece.AddComponent<Rigidbody>().ResetCenterOfMass();
    //        piece.AddComponent<MeshCollider>().convex = true;

    //        piece.GetComponent<Rigidbody>().AddForce(transform.forward * 1.0f , ForceMode.Impulse);
    //        piece.transform.tag = "Sliceable";
    //        if (gen > 0)
    //            StartCoroutine(ShatterObject(piece, hitpoint, gen - 1));

    //        piece.AddComponent<MeshDestroy>();
    //    }

    //    Destroy(obj);
    //}
    //private Vector3 GetAngle(GameObject obj , int gen)
    //{
    //    Quaternion q = Quaternion.Euler(Random.Range(-40, 40), Random.Range(-40, 40), Random.Range(-40, 40));
    //    Vector3[] faces = {obj.transform.forward, obj.transform.right, obj.transform.up};
    //    return q * faces[gen % 3];
    //}

    private void OnDrawGizmos()
    {
        if (isPlayer)
        {
            Debug.DrawLine(fpsCam.transform.position, fpsCam.transform.position + fpsCam.transform.forward * range);
        }
    }

    public void Reload()
    {
        bulletCount = maxBulletCount;
        reloadTime = 2.0f;
        gunMag.GetComponent<Rigidbody>().isKinematic = false;
        gunMag.transform.parent = null;
        gunAnim.Play("GunReload");
    }

    public void GenerateGunMag()
    {
        var go =  Instantiate(cloneMag, this.transform);
        go.SetActive(true);
        Destroy(gunMag);
        gunMag = go;
    }
}
