﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKGunGrab : MonoBehaviour
{
    public Transform[] leftHandPos;
    public Transform[] RightHandPos;
    
    
    public bool isGrabed = false;
    public bool isPlayer = false;

    private AnimationObj _animationObj;
    private Animator _anim;
    // Start is called before the first frame update
    void Start()
    {
        _animationObj = gameObject.GetComponent<AnimationObj>();
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame

    private void OnAnimatorIK(int layerIndex)
    {

        if (isGrabed)
        {
            _anim.SetIKPosition(AvatarIKGoal.LeftHand,leftHandPos[_animationObj.s_GunType].position);
            _anim.SetIKPositionWeight(AvatarIKGoal.LeftHand,1f);
            
            _anim.SetIKRotation(AvatarIKGoal.LeftHand, leftHandPos[_animationObj.s_GunType].rotation);
            _anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
            
            _anim.SetIKPositionWeight(AvatarIKGoal.RightHand,1f);
            _anim.SetIKPosition(AvatarIKGoal.RightHand,RightHandPos[_animationObj.s_GunType].position);
            
            _anim.SetIKRotation(AvatarIKGoal.RightHand, RightHandPos[_animationObj.s_GunType].rotation);
            _anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);
        }
        
       
    }
    
    // void OnDrawGizmosSelected()
    // {
    //     // Draw a yellow sphere at the transform's position
    //     Gizmos.color = Color.yellow;
    //     Gizmos.DrawSphere(leftHandPos[_player.currentWeaponType].position, 0.01f);
    //     Gizmos.DrawSphere(RightHandPos[_player.currentWeaponType].position, 0.01f);
    //
    // }
}
