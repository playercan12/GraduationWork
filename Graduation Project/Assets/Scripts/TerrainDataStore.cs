﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainDataStore : MonoBehaviour
{
    private static TerrainDataStore instance;
    public Terrain[] terrains;
    // Start is called before the first frame update


    public static TerrainDataStore Instance
    {
        get
        {
            if(instance == null)
            {
                return null;
            }

            return instance;
        }
    }
    void Start()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);

        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
